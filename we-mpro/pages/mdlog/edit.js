
const api = require('../../utils/api.js'); 
const util = require('../../utils/util.js');
const app = getApp();

Page({

  data: {  
    aid: '',
    row:{'title':'', 'body':'', 'auser':'', 'atime':''}
  },
  updValue(e) {
    let row = this.data.row,
      k1 = e.currentTarget.dataset.key;
    row[k1] = e.detail.value
    this.setData({row})
  },
  onLoad: function (options) { 
    let aid = options.aid ? options.aid : ''
    this.setData({
      aid,
      'row.auser': app.globalData.uinfo.mname,
      'row.atime': util.formatTime()
    })
    let title = '创建博文';
    if(aid){
      let param = 'aid='+aid+'&rejson=1'
      api.post(api.root + '/umc/mdlog-edit?'+param).then((res1) => { //console.log(res1)
        let r1 = res1.data,
          erow = {'title':r1.title, 'body':r1.body, 'auser':r1.auser, 'atime':r1.atime}
        this.setData({row:erow})
      }, res2 => {
          reject(res2)
      });
      title = '博文修改';
    }
    wx.setNavigationBarTitle({title:title})
  },
  onShow: function () {},
  onPullDownRefresh: function () {},
  onShareAppMessage: function () {},

  savForm: function(){
      let row = this.data.row, aid = this.data.aid;
      for (const key in row) { console.log(key, row[key])
        if(!row[key]){
          wx.showToast({title: `${key} 不能为空`, icon: 'none'})
          return true;
        }         
      };
      let paid = aid ? "?aid="+aid : ''
      api.post(api.root + '/umc/mdlog-aedo'+paid, row).then((res1) => { console.log(res1)
        wx.showToast({
          title: '保存成功', 
          success:()=>{
            wx.switchTab({
              url: '../login/uhome',
            })
          }
        })
      }, res2 => {
          reject(res2)
      });

  }
})