
const api = require('../../utils/api.js'); 
const util = require('../../utils/util.js');
const app = getApp();

Page({

  data: {
    userThumb: '/asset/icon/avatar132.jpg',
    userName: '... (加载中) ...',
    udata: {},
    loaded: 0
  },

  goEdit: function (e){ 
    let rid = e.currentTarget.dataset.rid; 
    if(!rid){ return; }
    wx.navigateTo({
      url: '../mdlog/edit?aid='+rid,
    })
  },
  goDel: function (e){ 
    let aid = e.currentTarget.dataset.aid;
    api.post(api.root + '/umc/mdlog-del?aid='+aid).then((res1) => { //console.log(res1)
      wx.showToast({
        title: '删除成功', 
        success:()=>{
          this.loadBlog()
        }
      })
    }, res2 => {
        reject(res2)
    });
  },

  onLoad: function (options) { },
  onShow: function () {
    this.init();
  },

  init: function(){
    this.waitLogin()
  },
  waitLogin() { 
    if(!app.globalData.inited){ //console.log('wait')
        setTimeout(()=>{this.waitLogin();},1273)
    }else{ //console.log('inited2', app.globalData)
        this.loadUser() // TODO
    } 
  },
  loadUser(){
    if(!app.globalData.autok){ 
      this.setData({
        userName: '(游客)', loaded: 1
      })
      return; 
    } //console.log(app.)
    api.post(api.root + '/api/uio', {}).then((res1) => { 
        let data = res1.data, 
          mpic = (data.umod && data.umod.mpic) ? data.umod.mpic : '',
          mname = (data.djwt && data.djwt.mname!='-') ? data.djwt.mname : '(微信用户)';
        if(!data.djwt){ return; }
        this.setData({
          userThumb: mpic,
          userName: mname,
          udata:data, loaded: 1
        });
        this.loadBlog()
    }, res2 => {
        reject(res2)
    });
  },
  loadBlog(){
      api.post(api.root + '/umc/mdlog-list', {}).then((res) => { 
          this.setData({
            pager: res.data.pager,
            posts: res.data.posts
          })
      }, res => {
          reject(res)
      });
  },
  getUinfo: function(){
    wx.getUserProfile({
      desc: '使用微信头像昵称', 
      success: (res) => { //console.log(res.userInfo, '<getUserProfile')
        let uinfo = res.userInfo;
        this.setData({
          userThumb:uinfo.avatarUrl,
          userName:uinfo.nickName
        })
      }
    })
  },

  wxLogin: function(){
    util.wxlogin(app, '/uio-wxmpro-reg').then((data)=>{ 
      this.onShow();
      //app.globalData.openid = data.wxtok.openid;
    })
  },

  
})