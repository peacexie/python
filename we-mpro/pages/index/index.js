
const app = getApp()
const api = require('../../utils/api.js'); 

Page({
  data: {
    posts: [], pager: {rcnt:0},
    copyat: '微爬(Weyp)-V3 @ 2022'
  },
  goView: function (e){ 
    let rid = e.currentTarget.dataset.rid; 
    if(!rid){ return; }
    wx.navigateTo({
      url: '../mdlog/view?aid='+rid,
    })
  },
  onLoad() {
    this.waitLogin();
  },
  waitLogin() { 
    if(!app.globalData.inited){ //console.log('wait')
        setTimeout(()=>{this.waitLogin();},1273)
    }else{  
        this.loadBlog() // TODO
    } 
  },
  loadBlog(){
      api.post(api.root + '/umc/mdlog-list', {}).then((res) => { 
          this.setData({
            pager: res.data.pager,
            posts: res.data.posts
          })
      }, res => {
          reject(res)
      });
  }

})
