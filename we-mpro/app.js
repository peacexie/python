
const api = require('./utils/api.js'); 
const util = require('./utils/util.js');

App({
  onLaunch() {
    // logs
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
    // login
    let accInfo = wx.getAccountInfoSync();
    this.globalData.appid = accInfo.miniProgram.appId;
    //api.setHeader({appid:accInfo.miniProgram.appId});
    util.wxlogin(this).then((data)=>{ //console.log('onLaunch:',data)
      // TODO?
    })
  }, 
  globalData: {
    autok: '', // token
    uinfo: {mname:'(游客)'}, // djwt
    wxtok: {}, // 备用, openid, session_key
    appid: '', openid: '',
    inited: 0 
  }
})
