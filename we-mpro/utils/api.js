
let root = 'http://127.0.0.1:8083'; //
//let root = 'http://txjia.com/wepay/xxx';

// # request

// 需要全局提示的api
const rtip = function(key, url, res) {
    let tips = `${key} Error\n"${url}"\n`
    console.error(tips, res);
    let tab = ['/uio-wxmpro-reg','/_xxxx']; 
    for(let i=0;i<tab.length;i++){
        if(url.indexOf(tab[i])>0){
            let info = res.info ? res.info : {}
            let title = (info.errno && info.errmsg) ? info.errmsg : tips
            wx.showToast({title, icon: 'none'});
            return
        }
    }
}
const wxreq = function (url, data, method, hload) { 
    if(!hload){ wx.showLoading()}
    const app = getApp();
    let userHeader = {
        autok:app.globalData.autok,
        appid:app.globalData.appid
    }
    return new Promise(function (resolve, reject) {
        wx.request({
            url, data, method, header:userHeader,
            success: (res) => { 
                //if(res.header.TokenExpired){ token = null; }  // 清除token的标识
                let info = res.info ? res.info : {}
                if(info.errno && info.errmsg){ 
                    rtip('API', url, res) // 错误提示
                    reject(res) 
                }else{ 
                    resolve(res.data)
                }
                if(!hload){ wx.hideLoading()}
            },
            fail: (err) => {
                rtip('Server', url, err) // 错误提示 
                reject(err);
                if(!hload){ wx.hideLoading()} 
            }
        })
    });
};
const get = function (url, params, hload) {
    return wxreq(url, params, 'GET', hload);
};
const post = function (url, params, hload) {
    return wxreq(url, params, 'POST', hload);
};

// # methods

//let uerHeader = { autok: '', appid: ''};
//const setHeader = (header) => { Object.assign(userHeader, header); }

// imgroot，根据apiUrl自动切换正式测试 (浏览记录,评价数据) 
const imgroot = function () {
    let urlTest = 'https://img.txjia-dev.com',
      urlPro = 'https://img.txjia.com',
      urlBase = root.indexOf('.test')>0 ? urlTest : urlPro;
    return urlBase;
}

// # export
module.exports = {
    root, //setHeader, 
    get, post, 
    imgroot
};
