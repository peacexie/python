//const util = require('./util.js');
const api = require('./api.js');

// # 购物车相关

const cskey = 'cart'
//wx.clearStorageSync(cskey);
//wx.setStorageSync(cskey, [])
const getCartKey = function(app, cb) {
    let myCkey = 'c' + app.globalData.cbase.cshopId + cskey; 
    return myCkey
}
// 购物车数据, 未登录从本地存储获取原始数据
const getCartPost = function(app, cb) {
    let myItems = {isUser:1}
    let myCkey = getCartKey(app)
    if(!app.globalData.userInfo || !app.globalData.userToken){
        myItems = {items:wx.getStorageSync(myCkey)}
    }
    return myItems;
};
// 更新购物车, 未登录更新本地存储
const updCartData = function(app, myItems, cb) { //console.log(myItems)
    let myCkey = getCartKey(app)
    if(!app.globalData.userInfo || !app.globalData.userToken){
        wx.setStorageSync(myCkey, myItems)
        cb && cb();
    }else{
        api.post(api.root+'/ucarUpdate',{items:myItems}).then(data=>{
            cb && cb();
        });
    }
};
// 转换购物车, 把未登录的本地存储更新到线上(登录之后使用)
const convUserCart = function(app, cb) { //console.log(myItems)
    let myCkey = getCartKey(app)
    let myItems = wx.getStorageSync(myCkey)
    updCartData(app, myItems, cb)
};

// # area相关

var areaArrays = {}; // 

// 统一调用外部数据利于维护更新(是否需要经常更新???)，
// 便于多项目(目前第七市场小程序也是用这个数据)共用 --- (Peace/2021-10-27)
function loadAreaArrays(cb) { 
    api.get(api.areaApi, {}).then(res => { //console.log(res);
        areaArrays = res;
        cb && cb();  
    });
}

function area_province_list(callback) {
    var rs = [], d = areaArrays;
    for (var code in d) {
        if (code.length == 3) rs.push([code, d[code][0]])
    }
    var result = {"result": rs}
    return callback ? callback(result) : result;
}

function area_city_list(province_id, callback) {
    var rs = [], d = areaArrays, m = province_id - 0
    for (var code in d) {
        if (d[code][1] == m) rs.push([code + "000000", d[code][0]])
    }
    var result = {"result": rs}
    return callback ? callback(result) : result;
}

function area_county_list(city_id, callback) {
    var rs = [], d = areaArrays, m = city_id.toString().slice(0, 6) * 1;
    for (var code in d) {
        if (d[code][1] == m) rs.push([code + "000000", d[code][0]])
    }
    var result = {"result": rs}
    return callback ? callback(result) : result;
}

// # 版本更新

const chkUpdate = function() {
    const updMain = wx.getUpdateManager()
    updMain.onCheckForUpdate((res) => { 
        // 请求完新版本信息的回调
        console.info('新版本检测结果:', res) 
    })
    updMain.onUpdateReady((rdy) => { 
        updMain.applyUpdate() 
        /*
        wx.showModal({
            title: '更新提示',
            content: '新版本已经准备好，是否重启应用？',
            success(res) { 
                if (res.confirm) {
                    // 调用 applyUpdate 应用新版本并重启
                    updMain.applyUpdate() 
                }
            }
        })*/
    })
    updMain.onUpdateFailed((err) => {
        // 新版本下载失败
        console.error('新版本下载失败:', err)
    })
}

// # export

module.exports = {
    getCartPost, updCartData, convUserCart,
    loadAreaArrays, area_province_list, area_city_list, area_county_list, 
    chkUpdate
}

/*

*/
