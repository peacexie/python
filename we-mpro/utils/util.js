const api = require('./api.js');

const formatTime = (date) => {
    date = date ? date : new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    const hour = date.getHours()
    const minute = date.getMinutes()
    const second = date.getSeconds()
    return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')
};
const formatNumber = n => {
    n = n.toString()
    return n[1] ? n : '0' + n
};

const wxlogin = function (app, apath) { 
    apath = '/api' + (apath ? apath : '/uio-wxmpro-login')
    let post = {appid:app.globalData.appid, openid:app.globalData.openid}
    return new Promise((resolve, reject) => {
        wx.login({
            success: res1 => { // 尝试login,向api请求获取用户token
                post.code = res1.code
                api.post(api.root + apath, post).then((res3) => { 
                    let data = res3.data;
                    wxlogcb(app, data)
                    resolve(data);
                }, res => {
                    reject(res)
                });
            },
            fail: res2 => { 
                reject(res2)
            }
        });
    })
}
const wxlogcb = function (app, data, cbOk) { //console.log('wxlogcb:',data)
    app.globalData.autok = data.token ? data.token : '';
    app.globalData.uinfo = data.djwt ? data.djwt : {};
    app.globalData.wxtok = data.wxtok ? data.wxtok : {};
    app.globalData.openid = (data.wxtok && data.wxtok.openid) ? data.wxtok.openid : '';
    app.globalData.inited = 1;
    //api.setHeader({openid:app.globalData.openid});
    cbOk && cbOk(data);
}

module.exports = {
    wxlogin, wxlogcb,
    formatTime
};

/*

const navBackSetData = function (numberOfPrev, data) {
    var pages = getCurrentPages();
    var prevPage = pages[pages.length - numberOfPrev - 1]; //上一个页面
    //直接调用上一个页面的setData()方法，把数据存到上一个页面中去
    if(!prevPage){ wx.showToast({ title: '返回失败', icon: 'none' }); return; }
    prevPage.setData(data);
    wx.navigateBack();
};

const getPosition = function (app, callback) { 
    if (app.globalData.geoInfo.address_component) { 
        callback && callback(app.globalData.geoInfo.address_component.city);
    } else { 
        wx.getLocation({
            type: 'wgs84',
            success: res => {
                api.post(api.root + '/geoInfos', res).then(data => {
                    app.globalData.geoInfo = data.result;
                    callback && callback(app.globalData.geoInfo.address_component.city);
                });
            },
            fail: res => { // 比如,定位没打开...就来到这里了
                console.log(res);
            }
        });
    }
};

*/
