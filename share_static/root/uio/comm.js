
var _ckpre='n2T_', _stpath='/share_static/', _timeFlag, _cnt=0; // 与 config.py中一致
var _nuluser = {mname:'(游客)'}

var tplCfgs = { 
    evaluate: /\{'([\s\S]+?(\}?)+)`\}/g,
    interpolate: /\{`=([\s\S]+?)`\}/g,
    encode: /\{'!([\s\S]+?)`\}/g,
    use: /\{'#([\s\S]+?)`\}/g,
    useParams: /(^|[^\w$])def(?:\.|\[[\'\"])([\w$\.]+)(?:[\'\"]\])?\s*\:\s*([\w$\.]+|\"[^\"]+\"|\'[^\']+\'|\{[^\}]+\})/g,
    define: /\{'##\s*([\w\.$]+)\s*(\:|=)([\s\S]+?)#`\}/g,
    defineParams: /^\s*([\w$]+):([\s\S]+)/,
    conditional: /\{'\?(\?)?\s*([\s\S]*?)\s*`\}/g,
    iterate: /\{'~\s*(?:`\}|([\s\S]+?)\s*\:\s*([\w$]+)\s*(?:\:\s*([\w$]+))?\s*`\})/g,
    varname: "it", strip: !0, append: !0, selfcontained: !1, doNotSkipEncoded: !1
}

function setCookie(name, value, exp, domain){
    var key = _ckpre+name, now = new Date(); 
    now.setTime(now.getTime()+exp*1000); //单位毫秒  
    var sexp = exp ? "; expires="+ now.toGMTString()+";" : ''; 
    var path = '; path=/';
    domain = domain ? '; domain=' + domain : '';
    document.cookie = key+"="+escape(value)+sexp+path+domain;    
}
function getCookie(name){
    var key = _ckpre+name;
    if (document.cookie.length>0){
        c_start=document.cookie.indexOf(key + "=")
        if (c_start>-1){ 
            c_start=c_start + key.length+1 
            c_end=document.cookie.indexOf(";",c_start)
            if (c_end==-1) c_end=document.cookie.length
            return unescape(document.cookie.substring(c_start,c_end))
        } 
    }
    return '';
}
function ckobj(part){
    part = part=='session' ? 'sessionStorage' : 'localStorage';
    return window[part];
}
function ckset(key, val, part){
    if(!part){ return setCookie(key, val, 3600); }
    let store = ckobj(part)
    store.setItem(_ckpre+key, val);
}
function ckget(key, part){
    if(!part){ return getCookie(key); }
    let store = ckobj(part)
    return store.getItem(_ckpre+key);
}

function setPassinp(e){
    $(e).prop('type','password');
}

function vload(type, msg) {
    $('#dtip1').find('.text').html(msg ? msg : '加载中...');
    $('#dtip1').find('.error, .okey, .load').hide();
    $('#dtip1').find('.'+type).show();
    $('#dtip1').show(); 
    //let autok = ckget('autok'); console.log(autok)
}

function setVcode(mod, click){
    var url = "/vcode?mod="+mod+'&_cnt='+_cnt+''
    url += "&_r="+(new Date().getTime());
    $("#"+mod+'_vimg').attr("src", url); //jq方式
    _cnt++;
}

// 设置表单token 
function setFmtok(mod, tkid, tab, setvc){
    vload('load')
    $.ajax({ 
        url: '/api/uio-fmtok?mod='+mod+'&tab='+tab+'&_cnt='+_cnt+'', 
        cache: false,
        data: {}, //$('#fmlogin').serialize(),
        dataType: 'json',
        success:function(res){ 
            $('#'+mod+'_'+tkid).val(res.data.token)
            arr = tab.split(',')
            for(var i=0;i<arr.length;i++){
                $('#'+mod+'_'+arr[i]).attr('name', res.data[arr[i]])
            }
            setvc && setVcode(mod)
            $('#dtip1').hide();
        },
        error:function(res){
            alert('Server Error!');
            $('#dtip1').hide();
        }
    })
    _cnt++;
}

function exitUser(cb){
    vload('load')
    autok = ckget('autok'); 
    $.ajax({ 
        url: '/api/uio-logout?_cnt='+_cnt+'', 
        cache: false, dataType: 'json',
        //headers: {autok:autok}, data: {}, 
        success:function(res){
            ckset('autok', '')
            if(cb){ opSmenu(0,'smenu'); cb(res) }
            else{ location.href = '/root/uio' }
            $('#dtip1').hide();
        },
        error:function(res){
            alert('Server Error!');
            $('#dtip1').hide();
        }
    })
    _cnt++;
}

function getUser(cb){
    vload('load')
    autok = ckget('autok'); 
    $.ajax({ 
        url: '/api/uio?_cnt='+_cnt+'', 
        cache: false, dataType: 'json',
        //headers: {autok:autok}, data: {}, 
        success:function(res){
            let data = res.data ? res.data : {},
                user = data.djwt ? data.djwt : _nuluser,
                stat = data.stat ? data.stat : {},
                info = res.info ? res.info : {};
            if(data.afresh && data.token){ ckset('autok', data.token) }
            $('.cmname').text(user.mname || user.uname);
            cb && cb(user, stat, info, data)
            $('#dtip1').hide();
        },
        error:function(res){
            alert('Server Error!');
            $('#dtip1').hide();
        }
    })
    _cnt++;
}

function showQr(url, msg, size){
    $.getScript(_stpath+"/ui3nd/qrcode.jqm4.js?_v=419a", function(){
        var qrhtm = "<div id='qrElm'></div><p>请扫码</p>";
        tips_show(qrhtm); 
        var data = {text:encodeURI(qr_url), width:"240", height:"240", correctLevel:2}
        setTimeout(function(){$('#qrElm').qrcode(data)}, 123);
    });
    _cnt++;
}

// <b class="qrcode_tip" onMouseOver="qrurl_act(id,1)" onMouseOut="qrurl_act(id,0)">扫码<i class="qrcode_pic"></i></b
function qrurl_set(id,url){ 
    cls = 'qrcode_pic';
    if($('#'+cls+id).html().length>24) return;
    url = url.length>12 ? url : window.location.href;
    url = urlEncode(url);
    img = "<img src='"+_cbase.run.fbase+"?ajax-vimg&mod=qrShow&data="+url+"' width='180' height='180' />";
    $('#'+cls+id).html('扫描网址到手机<br>'+img); // onload='imgShow(this,180,180)'
}
function qrurl_act(id,type,url){
    if(url) qrurl_set(id,url);
    if(type) $('#qrcode_pic'+id).show();
    else $('#qrcode_pic'+id).hide();
}

function qrcargo_act(id,type,url){
    if(url){
         var src = $('#qrcode_pic'+id).attr('src');
         if(!src){
            if(url.indexOf('?')>=0){ //08tools/yssina/1/root/run/mob.php?cargo.2015-97-dad1
                url = _cbase.run.rsite+url; 
                img = _cbase.run.fbase+"?ajax-vimg&mod=qrShow&data="+url; 
                $('#qrcode_pic'+id).find('img').attr('src',img);
            }else{//cargo.2015-97-dad1
                var extp = Math.random().toString(36).substr(2); 
                extp = url+','+extp;
                var url = 'actys=getQrcode&qrmod=send&extp='+extp+'&datatype=js&varname=data';
                $.getScript(_cbase.run.roots+'/plus/api/wechat.php?'+url, function(){ 
                    img = data.url; 
                    $('#qrcode_pic'+id).find('img').attr('src',img);
                });    
            }
         }
    } 
    if(type) $('#qrcode_pic'+id).show();
    else $('#qrcode_pic'+id).hide();
}

/* --- */

function log(msg){
    alert(JSON.stringify(msg));
    console.log(msg);
}
