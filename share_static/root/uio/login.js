
// 发送登录
function sndLogin(){
    vload('load')
    $.ajax({ 
        url: '/api/uio-login?'+_cnt, 
        cache: false, type: "POST", 
        data: $('#fmidpwd').serialize(),
        success:function(res){
            let info = 'info' in res ? res['info'] : {},
                data = 'data' in res ? res['data'] : {},
                msg = info.errno ? info.errno+':'+info.errmsg : '登录成功'
            if(info.errno){
                vload('error', msg)
            }else{
                vload('okey', msg)
            }
            if(!info.errno && data.login && data.token){
                ckset('autok', data.token)
                setVcode('idpwd', 0)
                godir = godef ? godef : '/root/uio-user'
                location.href = godir; //.reload();
            }
            opToast('dtip1', function(){
                $('#dtip1').hide();
            }, 2700);
        },
        error:function(){
            alert('Server Error!');
            $('#dtip1').hide();
        }
    })
    _cnt++;
    return false;
}

// 注册-切换用户类型
function setUgrade(e){
    let ugrade = $(e).find("option:selected").attr("ugrade")
    $('#apply_grade').val(ugrade)
}
// 注册-检查
function chkApply(){
    let uname = $('#apply_uname').val();
    if(uname.length<5){ alert('账号:至少5个字符!'); return false; }
    //let opass = $('#apply_opass').val();
    let upass = $('#apply_upass').val();
    let upchk = $('#apply_upchk').val();
    if(upass.length<6 || upass!=upchk){ alert('密码:至少6个字符，且确认码与密码要一致!'); return false; }
    let umod = $('#apply_umod').val();
    if(umod.length==0){ alert('请选择:类型!'); return false; }
    let mname = $('#apply_mname').val();
    if(mname.length<2){ alert('姓名:至少填两个字!'); return false; }
    return saveApply();
}
// 注册-保存
function saveApply(){
    vload('load')
    $.ajax({ 
        url: '/api/uio-apply?'+_cnt, 
        cache: false, type: "POST", 
        data: $('#fmapply').serialize(),
        success:function(res){
            let info = 'info' in res ? res['info'] : {},
                data = 'data' in res ? res['data'] : {},
                msg = info.errno ? info.errno+'<br>'+info.errmsg : '注册成功'
            if(info.errno){
                vload('error', msg)
            }else{
                vload('okey', msg)
            }
            if(!info.errno && data.login && data.token){
                ckset('autok', data.token)
                setVcode('apply', 0)
                location.href = '/root/uio-user'; //.reload();
            }
            opToast('dtip1', function(){
                $('#dtip1').hide();
            }, 2700);
        },
        error:function(){
            alert('Server Error!');
            $('#dtip1').hide();
        }
    })
    _cnt++;
    return false;
}

function chkTel(isvc){
    let vcode=$('#mobvc_vcode').val(), mtel = $('#mobvc_mtel').val();
    if(!(/^1\d{10}$/.test(mtel))){ 
        alert("手机号码有误，请重填");
        $('#mobvc_btnSms').removeClass('btn-act').addClass('btn-gray');
    }else if(vcode){ 
        $('#mobvc_btnSms').removeClass('btn-gray').addClass('btn-act');
    }
    return false; 
}
function smsCode(act){
    // TODO ... 
}
function sndfMobvc(act){
    let vcode=$('#mobvc_vcode').val(), mcode = $('#mobvc_mcode').val(), mtel = $('#mobvc_mtel').val();
    if(act=='smsg' && $('#mobvc_btnSms').hasClass('btn-gray')){
        if(!vcode || !mtel){ alert('请填写图片码和手机'); }
        return false;
    }
    if(act=='login' && !$('#mobvc_btnSms').hasClass('btn-checked')){
        alert('请填写图片码和手机，并发送手机短信');
        return false;
    }
    vload('load')
    $.ajax({ 
        url: '/api/uio-mobvc?'+_cnt, 
        cache: false, type: "POST", 
        data: $('#fmmobvc').serialize(),
        success:function(res){
            let info = 'info' in res ? res['info'] : {},
                data = 'data' in res ? res['data'] : {},
                msg = info.errno ? info.errno+'<br>'+info.errmsg : '登录成功'
            if(info.errno){
                vload('error', msg)
            }else{
                vload('okey', msg)
            }
            if(!info.errno && data.login && data.token){
                ckset('autok', data.token)
                setVcode('apply', 0)
                location.href = '/root/uio-user'; //.reload();
            }
            opToast('dtip1', function(){
                $('#dtip1').hide();
            }, 2700);
            // if(act!='smsg' && !data.errno){ location.reload(); }
        },
        error:function(){
            alert('Server Error!');
            $('#dtip1').hide();
        }
    })
    _cnt++;
    return false;
}

function checkScan(n){
    let gap = n<12 ? 2300 + n*300 : 6000;
    if(n>600){
        $('.qrpelm').html("<i class='fa fa-qrcode'></i>超时，刷新重新扫码")
        return;
    }
    $.ajax({
        url: checkapi+'-'+_cnt+'-'+n+'-'+gap,
        dataType: 'json',
        success:function(data){
            if(data.uflag=='0'){ // 未登录/32,96
                _timeFlag = setTimeout(function(){
                    checkScan(n+1);
                }, gap);
            }else{ //  if(data.uflag=='login')
                $('#dtip1').find('.text').html(data.errmsg);
                $('#dtip1').find('.error').hide();
                $('#dtip1').find('.okey').show();
                opToast('dtip1', function(){ 
                    location.reload(); 
                }, 2300);
            }
            _cnt++;
        },
        error:function(err){
            alert('服务器错误，请稍后刷新试一试!');
            //$('#modalMsg').html('服务器错误，请稍后试一试!');
        }
    })
}


