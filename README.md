
### 微爬(Wepy) - 文档


### 微爬(Wepy) - F3

**微爬(Wepy) - F3** 是站在巨人的肩上，基于 Python3 + Flask2 开发的一套Web框架。

简约、实用、开源、分享。

微爬(Wepy) 遵守MIT开源协议；沿用[贴心猫[Imcat]](http://txjia.com/imcat/)的架构理念；无缝对接贴心猫[Imcat]数据结构。

微爬(Wepy) 支持命令行、多线程，对API友好。不管是中小型传统web项目，还是多线程爬虫项目：都有出色的表现。


### 文档目录

此链接在项目运行情况下`http://127.0.0.1:8083/demo/read`浏览，在`gitee`下预览的md链接无效。

* [下载安装](./wepy-fx/AppYS/tplate/root/read/chd1.md)
* [MKV 路由](./wepy-fx/AppYS/tplate/root/read/chd2.md)
* [Jinja2 模板](./wepy-fx/AppYS/tplate/root/read/chd3.md)
* [控制器](./wepy-fx/AppYS/tplate/root/read/chd4.md)
* [Db/Model/User 操作](./wepy-fx/AppYS/tplate/root/read/chd5.md)
* [博客/小程序 等 综合演示](./wepy-fx/AppYS/tplate/root/read/chd6.md)
* [命令行/多线程 等 进阶应用](./wepy-fx/AppYS/tplate/root/read/chd7.md)
* [部署发布](./wepy-fx/AppYS/tplate/root/read/chd8.md)

生活是艰难的：甚至需要爬……
微爬(Wepy)，尽量让您轻松愉快的爬知识，爬价值，爬乐趣！

