
﻿﻿

https://gitee.com/miao-zehao/tkinter_study


@[toc](目录)

<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">


> **欢迎关注 [『Python黑科技』 系列](https://blog.csdn.net/u011027547/category_11594117.html)，持续更新中**
> **欢迎关注 [『Python黑科技』 系列](https://blog.csdn.net/u011027547/category_11594117.html)，持续更新中**


## 1. 制作计算器
![在这里插入图片描述](https://img-blog.csdnimg.cn/da14000c5b1b45d49a97cf35498ed70c.gif)
<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

## 2. 制作记事本
![在这里插入图片描述](https://img-blog.csdnimg.cn/eb0ec84e05484f14863ee8723e66230f.gif)
<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">


## 3. 用户的注册和登录
![在这里插入图片描述](https://img-blog.csdnimg.cn/e4cd7d22bf124c0a97d3350f466a806d.gif)
<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">


## 4. “2048”小游戏
![在这里插入图片描述](https://img-blog.csdnimg.cn/0d3290591f9447d38a3a61bdb36b2b69.gif)
<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

## 5. “俄罗斯方块”小游戏
![在这里插入图片描述](https://img-blog.csdnimg.cn/71faa38c895d472a8e5e019be51dd3d3.gif)
<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">


## 6. “贪吃蛇”小游戏
![在这里插入图片描述](https://img-blog.csdnimg.cn/12c76871cb444ad98cac57fd70d0fe25.gif)
<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

## 7. “连连看”小游戏
![在这里插入图片描述](https://img-blog.csdnimg.cn/456abbd3ffd0410bbd4c913bf8ecdfd0.gif)
<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">


## 代码下载
gitee下载
```python
https://gitee.com/miao-zehao/tkinter_study
```

百度网盘
```python
链接: https://pan.baidu.com/s/1WuofbEdCf_bJlQfZ10tQQw?pwd=m4x7 
提取码: m4x7
```

<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

## 总结

大家喜欢的话，给个👍，点个关注！给大家分享更多有趣好玩的Python黑科技！

> **欢迎关注 [『Python黑科技』 系列](https://blog.csdn.net/u011027547/category_11594117.html)，持续更新中**
> **欢迎关注 [『Python黑科技』 系列](https://blog.csdn.net/u011027547/category_11594117.html)，持续更新中**
> [【Python黑科技】tkinter库实战7个小项目合集（保姆级图文+实现代码）](https://blog.csdn.net/u011027547/article/details/122858281)
> [【Python黑科技】tkinter库实战制作一个计算器（保姆级图文+实现代码）](https://blog.csdn.net/u011027547/article/details/122847493)
> [【Python黑科技】tkinter库实战制作一个记事本（保姆级图文+实现代码）](https://blog.csdn.net/u011027547/article/details/122849668)
> [【Python黑科技】tkinter库实战用户的注册和登录（保姆级图文+实现代码）](https://blog.csdn.net/u011027547/article/details/122849819)
> [【Python黑科技】tkinter库实战“2048”小游戏（保姆级图文+实现代码）](https://blog.csdn.net/u011027547/article/details/122850153)
> [【Python黑科技】tkinter库实战“俄罗斯方块”小游戏（保姆级图文+实现代码）](https://blog.csdn.net/u011027547/article/details/122850439)
> [【Python黑科技】tkinter库实战“贪吃蛇”小游戏（保姆级图文+实现代码）](https://blog.csdn.net/u011027547/article/details/122850274)
> [【Python黑科技】tkinter库实战“连连看”小游戏（保姆级图文+实现代码）](https://blog.csdn.net/u011027547/article/details/122850937)



<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

