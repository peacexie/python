
rem ============== 开始区 ============== ========
@echo off 
:_START

cls
echo.
echo ++++++++++++++++++++++++++++++++++++++++++++
echo [       === AppYS Run at Cli Mode ===      ]
echo [    1. `r` for RESTART                    ]
echo [    2. `c` for CMD, `x` for EXIT          ]
echo [    3. `m` set MODE=`api` or def `tpl`    ]
echo [    4. `part/mod-act` for Run MKV         ]
echo [    e.g: `api/test_api-hello?pa=vb`       ]
echo ++++++++++++++++++++++++++++++++++++++++++++


rem ============== 输入区 ============== ========
:_INPUT

echo.
echo.
if "%MODE%"=="api" (
    set VMODE=API
) else (
    set VMODE=TPL
)
set /p mkv="view %VMODE%-MODE : [part/]mkv = "

if "%mkv%"=="r" goto _START
if "%mkv%"=="c" goto _CMD
if "%mkv%"=="m" goto _SETM
if "%mkv%"=="x" goto _EXIT
goto _DEEL


rem ============== 处理区 ============== ========
:_DEEL

python "./cli-entry.py" "%mkv%" "%MODE%"
goto _INPUT


rem set MODE = tpl
rem ============== 设置区 ============== ========
:_SETM

set /p MODE="set view-mode :       MODE = "
goto _START


rem ============== 结束区 ============== ========
:_CMD

echo.
echo +++++++++++++++++++++++++++++++++++
echo [ END !!! Peace[XieYS] 2022-01-16 ] 
echo +++++++++++++++++++++++++++++++++++
echo. pause & cmd 


rem ============== EXIT ============== ========
:_EXIT
exit


rem ============== 临时区 ============== ========

rem flask run -p 8083
