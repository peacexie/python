

from setuptools import find_packages, setup

setup(
    name='AppYS',
    version='1.0.0',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask',
    ],
    summary='Wepy3',
    homePage='txjia.com',
    author='Peace',
    license='MIT',
    description='Wepy3',
    platform='Linux,Windows',
)
