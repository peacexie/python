
import sys
from AppYS.core import mproc

# 自定义设置 - start

# mkv路由表
tab0 = [
    'bj', 'sh', 'gz', ['sz'], ['tj'], ['cq'],
    ['dongguan'], ['chenzhou'], ['hezhou'],
    ['city1','api/test_api-status'],
    'api/test_api-hello',
    'api/test_api-clib',
    'api/test_api-status',
    'demo/home'
]
tab3 = [
    'beijing', ['shenzhen'],
    ['dg','api/test_api-status'],
    'api/test_api-hello',
    'a~/b_c', # 错误
    'root/xxx',
]
tabAll = {'0':tab0, '3':tab3}

# mkv路由前缀
pre = 'api/test_api-mpsub-'

# 自定义设置 - end

defT = '0' # 默认mkv路由表
defN = 4 # 默认进程个数, 若8核cpu,设置4-6个分pools

if __name__=='__main__':

    argv = sys.argv #
    utb = argv[1]      if (len(argv)>1 and argv[1] in tabAll)       else defT
    uno = int(argv[2]) if (len(argv)>2 and isinstance(argv[2],str)) else defN
    #print(argv, uno, utb)

    mp = mproc.Pools(tabAll[utb], pre);
    mp.run(uno).vres()

'''
    命令demo: `>mul-proc.py {tab_no=0} {precc_cnt=4}` eg: `>mul-proc.py 0 4`
    # 鸡肋...？ 大批量采集容易封ip, 出错调试也易出问题
'''
