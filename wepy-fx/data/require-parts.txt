
pip freeze > requirements.txt

-----------------

Flask==2.0.2

PyMySQL==1.0.2
Markdown==3.3.6
requests==2.26.0

pyquery==1.4.3

click==8.0.3
pytest==6.2.5

-----------------

asgiref==3.4.1
atomicwrites==1.4.0
attrs==21.2.0
certifi==2021.10.8
charset-normalizer==2.0.8

colorama==0.4.4
cssselect==1.1.0
Django==3.2.9

idna==3.3
importlib-metadata==4.8.2
iniconfig==1.1.1
itsdangerous==2.0.1
Jinja2==3.0.3
lxml==4.6.4

MarkupSafe==2.0.1
packaging==21.3
pluggy==1.0.0
py==1.11.0

pyparsing==3.0.6

pytz==2021.3

sqlparse==0.4.2
toml==0.10.2
urllib3==1.26.7
Werkzeug==2.0.2
zipp==3.6.0
