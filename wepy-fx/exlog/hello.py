
from flask import Flask, abort, url_for, render_template, request, make_response, jsonify
from markupsafe import escape


app = Flask(__name__)



@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
    keywd = request.args.get('keywd', '')

    uanme = request.cookies.get('uanme', '') 
    resp = make_response(render_template('hello.htm', name=name, keywd=keywd, uanme=uanme))
    resp.set_cookie('uanme', uanme)
    return resp

    #return render_template('hello.htm', name=name, keywd=keywd)

@app.route('/umc/')
def umc():
    abort(401)
    render_template('hello.htm')    


@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"

# ---------------------

@app.route('/user/<username>')
def show_user_profile(username):
    # show the user profile for that user
    return f'User {escape(username)}'

@app.route('/post/<int:post_id>')
def show_post(post_id):
    # show the post with the given id, the id is an integer
    return f'Post {post_id}'

@app.route('/path/<path:subpath>')
def show_subpath(subpath):
    # show the subpath after /path/
    return f'Subpath {escape(subpath)}'

# ---------------------

@app.route('/')
def index():
    return 'index'

@app.route('/login')
def login():
    return 'login'

@app.route('/user/<username>')
def profile(username):
    return f'{username}\'s profile'

with app.test_request_context():
    print(url_for('hello_world'))
    #print(url_for('show_subpath'))
    print(url_for('login', next='/'))
    print(url_for('profile', username='John Doe'))


"""
with app.test_request_context('/hello', method='POST'):
    # now you can do something with the request until the
    # end of the with block, such as basic assertions:
    assert request.path == '/hello'
    assert request.method == 'POST'
"""

# ---------------------

@app.errorhandler(404)
def error(error):
    return render_template('error.htm', code=404), 404

@app.route("/ume")
def me_api():
    user = {'uname':'Peace', 'theme':'def', 'image':'logo.jpg'}
    #return jsonify([user.to_json()])
    return {
        "uname": user['uname'],
        "theme": user['theme'],
        #"image": url_for("user_image", filename=user.image),
    }
