#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from PIL import Image, ImageDraw, ImageFont, ImageFilter

import random

# 随机字母:
def rndChar():
    return chr(random.randint(65, 90))

# 随机颜色1:
def rndColor():
    return (random.randint(128, 255), random.randint(128, 255), random.randint(128, 255))

# 随机颜色2:
def rndColor2():
    return (random.randint(32, 127), random.randint(32, 127), random.randint(32, 127))

# 240 x 60:
width = 60 * 4
height = 60
image = Image.new('RGB', (width, height), (255, 255, 255))
# 创建Font对象:
fofp = '../../share_static/ui3nd/fonts/win-char.ttf' # 'C:/Windows/Fonts/Arial.ttf'
fofp = '../../share_static/ui3nd/fonts/fontawesome-webfont.ttf'
font = ImageFont.truetype(fofp, 36)
# 创建Draw对象:
draw = ImageDraw.Draw(image)
# 填充每个像素:
for x in range(width):
    for y in range(height):
        draw.point((x, y), fill=rndColor())
# 输出文字:
for t in range(4):
    draw.text((60 * t + 10, 10), rndChar(), font=font, fill=rndColor2())
# 模糊:
#image = image.filter(ImageFilter.BLUR)
image.save('mod_vcode.jpg', 'jpeg')

"""

Flask - 图形验证码
https://blog.csdn.net/qq_33962481/article/details/114409873
https://www.cnblogs.com/zhenzi0322/p/15413188.html

 --- --- 

 biact

import io
from flask import Flask, make_response, jsonify
from utils import VerifyCode

app = Flask(__name__)

@app.route('/')
def captcha():
    image, code = VerifyCode.create_validate_code()  # code是验证码
    imgio = io.BytesIO()
    image.save(imgio, 'png')
    imgio_str = imgio.getvalue()
    response = make_response(imgio_str)
    response.content_type = 'image/png'
    return response

"""