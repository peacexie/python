import sqlite3

#import click
from flask import current_app, g
from .. import config


def init_app(app):
    #print('1:::', __name__)
    app.teardown_appcontext(close_db)
    #app.cli.add_command(init_db_command)

"""
def init_db():
    db = get_db()

    with current_app.open_resource('../instance/schema.sql') as f:
        db.executescript(f.read().decode('utf8'))

@click.command('init-db')
@with_appcontext
def init_db_command():
    # Clear the existing data and create new tables.
    init_db()
    click.echo('Initialized the database.')
"""

def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect(
            config.dbcfgs['blog']['path'], #current_app.config['SQDBLOG'],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.db.row_factory = sqlite3.Row

    #print('3:::', __name__)
    return g.db


def close_db(e=None):
    db = g.pop('db', None)
    #print('2:::', __name__)
    if db is not None:
        db.close()

