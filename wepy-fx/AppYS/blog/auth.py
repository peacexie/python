
import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash

#from .db import get_db
from ..core import dbop

bp = Blueprint('auth', __name__, url_prefix='/blog_auth')


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view


@bp.route('/reg', methods=('GET', 'POST'))
@login_required
def reg():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = dbop.Db('blog')
        error = None

        if not username:
            error = 'Username is required.'
        elif not password:
            error = 'Password is required.'

        if error is None:
            try:
                db.exe("INSERT INTO {user} (username, password) VALUES (?, ?)",
                    (username, generate_password_hash(password)))
            except db.IntegrityError:
                error = f"User {username} is already registered."
            else:
                return redirect(url_for("auth.login"))

        flash(error)

    return render_template('blog/reg.htm')

@bp.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = dbop.Db('blog')
        error = None
        user = db.get('SELECT * FROM {user} WHERE username = ?', (username,) ,1)

        if user is None:
            error = 'Incorrect username.'
        elif not check_password_hash(user['password'], password):
            error = 'Incorrect password.'

        if error is None:
            session.clear()
            session['user_id'] = user['id']
            return redirect(url_for('blog.index'))

        flash(error)

    return render_template('blog/login.htm')


@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        db = dbop.Db('blog')
        g.user = db.get('SELECT * FROM {user} WHERE id = ?', (user_id,), 1)


@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('blog.index'))
