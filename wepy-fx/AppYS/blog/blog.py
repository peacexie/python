
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, make_response
)
from werkzeug.exceptions import abort

from .auth import login_required
#from .db import get_db
from ..core import scut
from ..core import dbop

bp = Blueprint('blog', __name__, url_prefix='/blog_demo')
#bp.add_url_rule('/', endpoint='index')

@bp.route('/rss.xml')
def rss():

    field = "SELECT p.id, title, body, created, user_id, username"
    sfrom = "FROM {post} p JOIN {user} u ON p.user_id = u.id"

    db = dbop.Db('blog')
    posts = db.get(f"{field} {sfrom} ORDER BY created DESC LIMIT 96")

    info = {'title':'AppYS.Sqlite.博客'}

    tpxml = render_template('blog/rss.xml', info=info, posts=posts)
    response = make_response(tpxml)
    response.headers['Content-Type'] = 'application/xml'
    return response

@bp.route('/errmsg')
def errmsg():

    name1 = "Peace";
    age1 = 88
    res = f"Hello,{name1}, your age is {age1}"
    print(res)

    #abort(404, f"Post id {id} doesn't exist.")
    abort(403, 'Test:测试`Error`信息')
    return {}
    #a = 8/0
    # page(1), limit(10), keywd=''

@bp.route('/')
def index():

    db = dbop.Db('blog')
    
    page  = request.args.get('page',1)
    limit = request.args.get('limit',5) # 2,5
    offset = (int(page)-1) * int(limit)
    
    keywd = request.args.get('keywd','')
    field = "SELECT p.id, title, body, created, user_id, username"
    sfrom = "FROM {post} p JOIN {user} u ON p.user_id = u.id"
    where = "WHERE title LIKE '%"+keywd+"%'" if keywd else ''
    filters = "&keywd="+keywd if keywd else ''
    slimit = 'LIMIT '+str(offset)+','+str(limit)

    pager = db.pager(f"{sfrom} {where}", page, limit)
    pager['keywd'] = keywd #; print(ctemp,pager)

    sql = f"{field} {sfrom} {where} ORDER BY created DESC {slimit}"
    posts = db.get(sql, ())
    return render_template('blog/index.htm', posts=posts, pager=pager, filters=filters)

@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        #flash('error')
        if error is not None:
            flash(error)
        else:
            db = dbop.Db('blog')
            sql = "INSERT INTO {post} (title, body, user_id) VALUES (?, ?, ?)"
            db.exe(sql, (title, body, g.user['id']))
            return redirect(url_for('blog.index'))

    return render_template('blog/create.htm')

def get_post(id, check_author=True):

    sql = 'SELECT p.id,title,body,created,user_id,username '
    sql += ' FROM {post} p JOIN {user} u ON p.user_id = u.id WHERE p.id=?'

    db = dbop.Db('blog')
    post = db.get(sql, (id,), 1)

    if post is None:
        abort(404, f"Post id {id} doesn't exist.")

    if check_author and post['user_id'] != g.user['id']:
        abort(403)

    return post

@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    post = get_post(id)

    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = dbop.Db('blog')
            sql = "UPDATE {post} SET title = ?, body = ? WHERE id = ?"
            db.exe(sql, (title, body, id))
            return redirect(url_for('blog.index'))

    return render_template('blog/update.htm', post=post)

@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    get_post(id)
    db = dbop.Db('blog')
    db.exe('DELETE FROM {post} WHERE id = ?', (id,))
    return redirect(url_for('blog.index'))

