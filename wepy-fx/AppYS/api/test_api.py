
import time, random, json
from flask import Flask, g, current_app, request
from ..core import clib, parse, mkvop, scut, xbug, urlpy

# -----------------------------

def helloAct(mkv=''):

    name = "微爬(Wepy)"
    age = 88
    PI2 = 3.14159265/2
    E20 = 20*2.71828182
    res = f"{name.upper()}, id={age:04d}, pi/2={PI2:.3f}, 20*e={E20:.3f}"
    #print(res)

    return res

def mpsubAct(mkvs):
    rnd1 = random.random() * 500
    time.sleep(rnd1/1000)
    res = f"{mkvs['mkv']} run@{rnd1}(sec)"
    return {'view':mkvs['view'], 'sleep':f"{rnd1:.3f}(ms)"}

def clibAct(mkvs):

    res = {}

    tel = '13537432147'; ct = clib.cutStr(tel, 3,4,'***')
    eml = 'xpigoen@163.com'; ce = clib.cutStr(eml, 3,3,'***@***')

    res['tel'] = tel; res['ct'] = ct;
    res['eml'] = eml; res['ce'] = ce;
    return res

def statusAct(mkvs=''):

    data = {'axx':'.recode.'}
    data = 'success' #
    return data

def retplAct(mkvs=''):
    
    mode = 'json'

    if mode=='json':
        host = 'https://api.weixin.qq.com'
        url = f"{host}/sns/jscode2session?appid=appid&secret=secret&js_code=code&grant_type=authorization_code"
        res = urlpy.get(url, {}, 'get', 'json')
    if mode=='page':
        url = 'http://txjia.com/'
        #res = get_data(url)
        res = urlpy.get(url)

    data = {
        #'res': res, 
        'html': res, 
        '_newtp': 'demo/test_tpl/rdata',
    }
    return data

def get_data(url=''):
    import urllib.request
    resp = urllib.request.urlopen(url) #
    redata = resp.read().decode('utf8')
    return redata

def logerAct(mkvs):

    current_app.logger.debug('A value for debugging')
    current_app.logger.warning('A warning occurred (%d apples)', 6688)
    current_app.logger.error('An error occurred')

    root_path = current_app.root_path
    instance_path = current_app.instance_path
    current_app.logger.debug(root_path+'\n'+instance_path)
    current_app.logger.debug(current_app)

    data = {'root_path':root_path}

    return data

#@app.route("/get-data")
#async def get_data():
#    #data = await async_db_query(...)

# -----------------------------

# 提示信息: OK
def tipokAct(mkvs):
    data = {'tipok':'tipokDetail'}
    data['_info'] = {'_retip':1, 'errmsg':'操作成功!', '_data':data}
    return data

# 提示信息: Bad
def errorAct(mkvs):
    data = {'tibad':'tip(bad)Detail'}
    data['_info'] = {'_retip':0, 'errno':'test:BadMessage', 'errmsg':'It is a TEST BAD Message!', '_data':data}
    return data

