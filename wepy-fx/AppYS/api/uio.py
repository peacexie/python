
import time, datetime, json #, jwt
from flask import Flask, g, current_app, request, session
from ..core import parse, mkvop, dbop, dmop, umop, scut, xbug, urlpy, clib
from ..extra import weixin

# uio
"""
    js_code -> user+token -> update-token
    token+act -> user+token -> update-token
"""

# ============================================ 

# login, reg
def wxmproAct(mkvs):
    data = {}; bind = {}
    post = request.get_json()
    if not post or not 'code' in post:
        data['_info'] = {'errno':'Null-POST', 'errmsg':'Null POST-DATA.'}
        return data

    if 'openid' in post and post['openid']:
        openid = post['openid']
        wxtok = {'openid':openid}
    else:
        wx = weixin.mpro(post['appid'])
        wxtok = wx.code2openid(post['code'])
        openid = wxtok['openid']

    user = umop.Ubase() 
    uinfo = user.get(openid, post['appid'], 'wxmpro')
    if 'uacc' in uinfo: 
        del uinfo['uacc']['upass']
    elif mkvs['view']=='reg':
        user.mod = 'person'
        bind = user.bind(openid, post['appid'], 'wxmpro')
        uinfo = user.get(openid, post['appid'], 'wxmpro')
    
    if 'uacc' in uinfo: 
        data = umop.fmtAuser(uinfo)
        data['auclear'] = umop.clearAutok(data['djwt'])

    data['wxtok'] = wxtok
    data['bind'] = bind
    return data

# ============================================ 

def smscodeAct(mkvs):
    pass # TODO ... 

def mobvcAct(mkvs):
    data = {} # TODO ... 
    ref = clib.chkref()
    pex = scut.gvget('run.pex')

    post = clib.upost() #;print('post', post)
    tab = 'mtel,mcode,vcode'
    fmtok = clib.tkval(post, 'fmtok', tab)
    vtb = fmtok['vtb']; vtb['vcode']=vtb['vcode'].upper()

    scode = session['vcode_mobvc'].upper() if 'vcode_mobvc' in session else ''
    if len(scode)<3 or scode!=vtb['vcode']: #(not 'vcode' in vtb) or 
        data['_info'] = {'errno':'Error-Vcode', 'errmsg':'认证码错误'}
        return data
    if len(vtb['uname'])<3 or len(vtb['upass'])<5: # not 'uname' in vtb or not 'upass' in vtb or 
        data['_info'] = {'errno':'Error-Login(1)', 'errmsg':'账号密码错误'}
        return data

    user = umop.Ubase()
    uinfo = user.login(vtb['uname'], vtb['upass'])
    if not uinfo['login']:
        data['_info'] = {'errno':'Error-Login(2)', 'errmsg':'账号密码错误'}
        return data

    data = umop.fmtAuser(uinfo)
    #time.sleep(0.2)
    data['vtb'] = vtb
    return data

# ============================================ 

# 默认首页方法, 不返回数据或不改变模板可不要
def _homeAct(mkvs):
    data = {}
    utok = umop.heartBeat()

    if utok and 'djwt' in utok and 'uname' in utok['djwt']:
        user = umop.Ubase() 
        uinfo = user.get(utok['djwt']['uname'])
        del uinfo['uacc']['upass']
        data = {**uinfo, **utok}

    #umop.clearAutok(data['djwt']) # Test
    return data

# apply
def applyAct(mkvs):
    data = {}
    ref = clib.chkref()
    pex = scut.gvget('run.pex')

    post = clib.upost() #;print('post', post)
    tab = 'uname,upass,upchk,vcode'
    fmtok = clib.tkval(post, 'fmtok', tab)
    vtb = fmtok['vtb']; vtb['vcode']=vtb['vcode'].upper()
    umod = post['umod'] if 'umod' in post else 'person'
    mname = post['mname'] if 'mname' in post else '-'
    grade = post['grade'] if 'grade' in post else '-'

    scode = session['vcode_apply'].upper() if 'vcode_apply' in session else ''
    if not umod:
        data['_info'] = {'errno':'Error-Apply(umod)', 'errmsg':'请选择用户类型'}
        return data
    if len(scode)<3 or scode!=vtb['vcode']: # (not 'vcode' in vtb) or 
        data['_info'] = {'errno':'Error-Apply(vcode)', 'errmsg':'认证码错误'}
        return data
    if len(vtb['uname'])<3 or len(vtb['upass'])<5: #not 'uname' in vtb or not 'upass' in vtb 
        data['_info'] = {'errno':'Error-Apply(idpw)', 'errmsg':'账号至少3个字符,密码至少5个字符'}
        return data
    if vtb['upass']!=vtb['upchk']:
        data['_info'] = {'errno':'Error-Apply(chkpw)', 'errmsg':'两次密码不一样'}
        return data

    fexist = umop.hasUname(vtb['uname'])
    if fexist:
        data['_info'] = {'errno':'Error-Apply(exist)', 'errmsg':'账号`'+vtb['uname']+'`已经存在，请更换'}
        return data

    pex = {'mname':mname, 'grade':grade} # , 'grade', 'mtel', 'memail'
    user = umop.Ubase(umod) 
    ares = user.reg(vtb['uname'], vtb['upass'], pex)
    uinfo = user.login(vtb['uname'], vtb['upass'])

    data = umop.fmtAuser(uinfo)
    #time.sleep(0.2)
    data['vtb'] = vtb
    return data

def loginAct(mkvs):
    data = {}
    ref = clib.chkref()
    pex = scut.gvget('run.pex')

    post = clib.upost() #;print('post', post)
    tab = 'uname,upass,vcode'
    fmtok = clib.tkval(post, 'fmtok', tab)
    vtb = fmtok['vtb']; vtb['vcode']=vtb['vcode'].upper()

    scode = session['vcode_idpwd'].upper() if 'vcode_idpwd' in session else ''
    if len(scode)<3 or scode!=vtb['vcode']: #(not 'vcode' in vtb) or 
        data['_info'] = {'errno':'Error-Vcode', 'errmsg':'认证码错误'}
        return data
    if len(vtb['uname'])<3 or len(vtb['upass'])<5: # not 'uname' in vtb or not 'upass' in vtb or 
        data['_info'] = {'errno':'Error-Login(1)', 'errmsg':'账号密码错误'}
        return data

    user = umop.Ubase()
    uinfo = user.login(vtb['uname'], vtb['upass'])
    if not uinfo['login']:
        data['_info'] = {'errno':'Error-Login(2)', 'errmsg':'账号密码错误'}
        return data

    data = umop.fmtAuser(uinfo)
    #time.sleep(0.2)
    data['vtb'] = vtb
    return data

def logoutAct(mkvs):
    data = umop.logoutAutok()

    print('logout:', data)
    return data

# ============================================ 

# 表单token
def fmtokAct(mkvs):
    data = {}
    ref = clib.chkref()
    pex = scut.gvget('run.pex')
    
    mod = pex['mod'] if 'mod' in pex else '(def)'
    djwt = {
        'mod': mod,
        'mkv': mkvs['mkv'],
        'ref': ref, # 来路
        #'uag': request.headers.get('User-Agent'), # 请求头
    }

    tab = pex['tab'] if 'tab' in pex else ''
    arr = tab.split(',')
    for tk in arr: #tk in arr:
        tv = tk+'_'+clib.rndKid(16)
        djwt[tk] = tv
        data[tk] = tv

    token = parse.jwt(djwt)
    data['token'] = token
    data['len'] = len(token)
    #time.sleep(2)
    return data;


# ================================

''' chkref, upost, tkval
    autok, fmtok
    static function req($key='autok'){
        $key = 'HTTP_' . strtoupper(str_replace(['-'], ['_'], $key)); 
        $jwt = empty($_SERVER[$key]) ? '' : $_SERVER[$key];
        return $jwt;
    }
    http://127.0.0.1:8083/root/uio
'''