
import json
from ..core import parse, dbop, dmop, umop, scut, xbug

# dbs                       

# 默认首页方法, 不返回数据或不改变模板可不要
def _homeAct(mkvs):

    data = {'testData':'homeData'}

    #data['_newtp'] = 'demo/home/dbs' # 设置新模板,默认htm
    #data['_return'] = 'json' # 直接返回json
    return data

# tum
def tumAct(mkvs):
    data = {}

    user = umop.Ubase('porson') ;# Member,Ubase
    data = user.reg()

    return data

# tmp1
def tmp1Act(mkvs):
    data = {}

    #raise TypeError('one of the hex, bytes, bytes_le, fields, or int arguments must be given')
    #raise Exception('x 不能大于 5。x 的值为: ')

    data['enc_x1'] = parse.md5('abc'); #dump($t1);
    data['enc_x2'] = parse.sha1('123'); #dump($t1);
    data['enc_x3'] = parse.hh128('test'); #dump($t1);

    data['ckey'] = umop.Perm().ckey()
    #xbug.log(data, 'test')

    data['pass1'] = umop.sysPass('name', 'pass', 'adminer')
    data['pass2'] = umop.sysPass('abcd', '1234', 'person')
    #die();

    import uuid

    name = 'test_name'
    #namespace = 'test_namespace'
    data['namespace'] = namespace = uuid.NAMESPACE_URL

    data['uuid1'] = uuid.uuid1()
    data['uuid3'] = uuid.uuid3(namespace,name)
    data['uuid4'] = uuid.uuid4()
    data['uuid5'] = uuid.uuid5(namespace,name)

    data['tmp-enc'] = tmp = parse.bs64('hi--world!1')
    data['tmp-dec'] = tmp = parse.bs64(tmp, 1)

    xbug.log(data, 'test')

    return data

# read
def readAct(mkvs):
    data = {}

    grades = scut.read('grades')
    groups = scut.read('groups')

    data['supper'] = grades['supper']
    data['news'] = groups['news']

    return data

# data
def dataAct(mkvs):
    data = {}

    news = dmop.Model('news') 
    
    data['row-1'] = news.get("did='2020-9q-80a9'", 1)
    data['list-2'] = news.get("vtype='ptxt'", 2) 
    data['list-page'] = news.get("vtype='ptxt'", '2,2') 
    data['list-order'] = news.get("", '2', "title DESC") 
    data['list-count'] = news.get("`show`='all'", 'count') 
    data['list-def'] = news.get()

    news2 = dmop.Model('news.join') 

    data['list-join'] = news2.get("vtype='ptxt'", "6", "title DESC")
    data['news-cfg'] = news2.cfg

    data['list-2b'] = dmop.Model('news').get('',2)
    data['list-2c'] = scut.db().get("SELECT * FROM {docs_news} LIMIT 1")
    data['list-2d'] = scut.mod('news').get('',2)

    return data

# mysql-表结构
def mytabAct(mkvs):

    db = dbop.Db('0') # dbop.dbm, scut.db, scut.data
    res = db.tbdic()
    resDetail = db.tbdic(1)

    data = {}
    data['tabNews'] = resDetail['docs_news_ys']
    data['tab'] = res

    return data

# sqlite-表结构
def sqtabAct(mkvs):

    db = dbop.Db('blog')
    res = db.tbdic()
    resDetail = db.tbdic(1)

    data = {}
    data['tabPost'] = resDetail['xtest_post']
    data['tab'] = res

    return data

# mysql方法
def mysqlAct(mkvs):

    data = {'testList - dbs ':'listData - dbs'}
    db = dbop.Db('0')
    
    # kid,model,pid,title
    sql = "SELECT kid,model,pid,title FROM {types_china} WHERE deep=%s AND TITLE LIKE %s AND pid IN('cngx','cnhn','cnzj')"
    data['lists'] = db.get(sql, ['2',"%州%"], 8)
    #sql = "SELECT kid,model,pid,title FROM {types_china} WHERE deep=%s AND TITLE LIKE %s AND pid IN(%s)"
    #data['lists'] = db.get(sql, ['2',"%州%",['cngx','cnhn','cnzj']], 8)

    data['_newtp'] = 'demo/test_dbs/mysql' # 设置新模板
    return data 

# sqlite方法
def sqliteAct(mkvs):

    data = {'testList - dbs ':'listData - dbs'}
    db = dbop.Db('blog')
    
    # id,user_id,created,title
    sql = "SELECT id,user_id,created,title FROM {post} WHERE title LIKE ?"
    data['lists'] = db.get(sql, ["%e%"], 8)

    data['_newtp'] = 'demo/test_dbs/sqlite' # 设置新模板
    return data 

# 默认列表方法, 不返回数据或不改变模板可不要
def _listAct(mkvs):

    data = {'testList - dbs ':'listData - dbs'}
    db = dbop.Db('0')
    
    sql = "SELECT kid,model,pid,title,`char` FROM {types_china} WHERE deep=?"
    data['lists'] = db.get(sql, ('1',), 5)

    #data['_newtp'] = 'home/dbs' # 设置新模板,默认htm
    return data 

