
import time, datetime, json, base64, random, math
from flask import Flask, g, current_app, request, session
from ..core import clib, parse, mkvop, umop, scut, xbug, urlpy

# -----------------------------

"""
  - locin (本地调试)       loc_test888
  - idpwd (账号密码)       peace_xie999
  - mobvc (手机短信)       13588882147
  - email (电子邮箱)       xp8888on@163.com
  - qq    (QQ)            80893510 / A7754037047CD87B94ABF2B988889F57
  - wxoacc (微信公众号)    wx4177c709888853c6 . jjcn2cFbxMLaMB8888YbCk
  - wxwork (企业微信)      wx4177c709886853c6 . jjcn2cFbxMLaMB8888YbCk
  - wxmpro (微信小程序)    wx4177c709882853c6 . jjcn2cFbxMLaMB8888YbCk
  - zfbmpro (支付宝小程序) 2021072866887148 . 2088022888225045
  - bdmpro (百度小程序)    24894429 . l214zFqNrEuIEnp6m78888w8yj

wxappid = wx4177c709166053c6
openid  = oyDK8v.jjcn2cFbxMLaMBhKEsYbCk
          o9LBd5.Q-eiYz4kOZbAdMDJQlqUmk
qqxxx   = DE4E30F9EDC99B0EBACD6CB446E921CA

bdappid = 24894429
bdxxx   = hr6jTV7Pcq5xNvqfrCCuN2Jn9I

"""

def homeAct(mkv=''):

    res = {}
    return res

def u1Act(mkvs):
    data = {}

    umode = 'idpwd' # idpwd,mobvc,email,wxmpro,zfbmpro,bdmpro
    if umode=='mobvc':
        appid = '-'
        apuid = '13588882147'
    elif umode=='email':
        appid = '-'
        apuid = 'xp8888on@163.com'
    elif umode[0:2]=='wx':
        appid = 'wx4177c709166053c6'
        apuid = 'oFapq1IgEvQNWO91qTnSDp-006'
    elif umode[0:3]=='zfb':
        appid = '2021072866887142'
        apuid = '2088022888225041'
    elif umode[0:2]=='bd':
        appid = '24894429'
        apuid = 'hr6jTV7Pcq5xNvqfrCCuN2Jn9I'
    else:
        appid = '-'
        apuid = 'peaxexie_232'

    user = umop.Ubase('person')
    #data['reg'] = user.reg('peace1', '123456');
    #data['get'] = user.get('peace1', '', 'idpwd');
    #return data

    data = get = user.get(apuid, appid, umode);
    if umode=='idpwd' and not get['uacc']: # and 
        #pass #data['get'] = get
        data['reg'] = user.reg(apuid, '123456'); #reg(self, uname='', upass='', mex={})
    #elif 
    elif not get['uppt']:
        data['bind'] = user.bind(apuid, appid, umode); 

    data = user.login(apuid, '123456')

    data['mod'] = user.mod
    #user.mod = 'testaaa'
    #data['mod2'] = user.mod
    #data['__smod'] = user.__smod

    return data

def jwt3rdAct(mkvs):

    data = {}
    import jwt

    dic = {
        'exp': int(time.time() + 86400), #datetime.datetime.now() + datetime.timedelta(days=1),  # 过期时间
        #'iat': datetime.datetime.now(),  #  开始时间
        #'iss': 'Wepy'
        'data': {'a':'b'}
    }
    secret = '123456'

    jdump = parse.json_dump(dic,0).replace("\n",'').replace(" ",'')
    data['jdump'] = jdump
    data['jlen'] = len(jdump)

    data['token'] = token = jwt.encode(dic, secret, algorithm='HS256')  # 加密生成字符串
    data['toklen'] = len(token)
    data['ujwt'] = jwt.decode(token, secret, algorithms=['HS256'])  # 解密，校验签名
    #print(type(ujwt))

    data['_b64a'] = parse.bs64('12345a')
    data['_b64b'] = parse.bs64('12345x2')
    data['_b64c'] = parse.bs64('12345e45')
    data['_b64d'] = parse.bs64('12345w789')
    data['_b64e'] = parse.bs64('12345e你45')
    data['_b64f'] = parse.bs64('12345w好789')

    return data

def jwtAct(mkvs):

    data = {}
    djwt = {  # 内容，一般存放该用户id和开始时间
        'uid': '2024-ab-ab68', # ''
        'uname': 'peacexie@163.com', # (Guest)
        'mname': '和平鸽', # (游客)
        'key': 'f7a0fd45-b9fd-11ec-bab0-d017c295a9cb',
        'adm' : 0,
        'login': 0,
    }

    jwtstr = parse.jwt(djwt)
    data['jwtstr'] = jwtstr
    data['jwtlen'] = len(jwtstr)

    data['jwtde1'] = parse.jwt(jwtstr)

    jwtstr = '1649847450.eyJuYW1lIjoicGVhY2V4aWXlpb0iLCJ1aWQiOiIyMDI0LWFiLWFiNjgiLCJrZXkiOiJmN2EwZmQ0NS1iOWZkLTExZWMtYmFiMC1kMDE3YzI5NWE5Y2IiLCJhZG0iOjAsImxvZ2luIjowfQ__.5977ce77f8c01b2ca887d1331d56edf6';
    data['jwtde2'] = parse.jwt(jwtstr, 3600)

    jwtstr = parse.jwt({'key':'val'})
    data['tokv0'] = jwtstr
    data['jwtde0'] = parse.jwt(jwtstr)

    data['yu1'] = 8 % 3
    #data['pi'] = math.pi
    #data['2root'] = math.sqrt(2)

    return data

# tmp3
def tmp3Act(mkvs):
    data = {}
    db = dbop.Db()

    data['idCard-1'] = clib.idCard('43282419180921437', 1);
    data['idCard-a'] = clib.idCard('432824191809214371');
    data['idCard-b'] = clib.idCard('432824191809214372');

    data['dbKid-1'] = dmop.dbKid('docs_news');
    data['dbKid-2'] = dmop.dbKid(xTime='2022-02-11 12:34:56');
    data['dbKid-3'] = dmop.dbKid(xTime='2022-02-11 12:34:57');
    data['dbKid-4'] = dmop.dbKid(xTime='2022-02-11 12:34:58');
    data['dbKid-5'] = dmop.dbKid(xTime='2022-02-11 12:34:59');

    data['getUid-a1'] = umop.getUid('1982-79-0709');
    data['getUid-a2'] = umop.getUid('1979-9d-0913');

    data['getUid-b1'] = umop.getUid();
    data['getUid-b2'] = umop.getUid();

    data['getUname-admin'] = umop.getUname('admin', 'person');
    data['getUname-(null1)'] = umop.getUname('', 'person');
    data['getUname-(null2)'] = umop.getUname('', 'person');
    data['getUname-peace'] = umop.getUname('peace', 'person');
    data['getUname-tel'] = umop.getUname('135-37432147', 'person');
    data['getUname-email'] = umop.getUname('abc@163.com', 'person');
    data['getUname-loing'] = umop.getUname('abc-4328241918092666_43282419180921888@163.com', 'person');

    for i in range(50):
        tmp = dmop.dbKid('docs_news'); # xTime='2022-02-11 12:34:57'
        sql1 = "INSERT INTO {docs_news} (did,dno) VALUES(?,?)"
        #rec = db.exe(sql1, (tmp[0],tmp[1]))

        sqlx = f"INSERT INTO docs_news (did,dno) VALUES(?,?)"

    return data

# tmp2
def tmp2Act(mkvs):
    data = {}

    lib = clib.Keyid() 

    data['fmtJinzhi(266,16)'] = clib.fmtJinzhi(255, 16)
    data['k1'] = clib.keyTab('k')
    data['k2'] = clib.keyTab('k', 1)
    data['r1'] = clib.rndKid(42)
    data['r2'] = clib.rndKid(42, '0')
    data['r3'] = clib.rndKid(42, 'f')

    data['tplKid-a0'] = clib.tplKid()
    data['tplKid-md2'] = clib.tplKid('md2', '2001-02-03 04:05:06')
    data['tplKid-md3'] = clib.tplKid('md3',  1412380039)
    data['tplKid-md2'] = clib.tplKid('md2', '1412380039')

    data['tplKid-b3'] = clib.tplKid('')

    data['tplKid-md3a'] = clib.tplKid('md3')
    data['tplKid-mdh'] = clib.tplKid('mdh')
    data['tplKid-5.5'] = clib.tplKid('y2md2h1.ms2s1r2') # y4[2:4] + f"{md2}{h1}-{ms2}" + r3[0:2]
    data['tplKid-def'] = clib.tplKid('def')
    data['tplKid-nul'] = clib.tplKid()

    data['clib.rndKid()'] = clib.rndKid()

    data['clib.aspKid(--)'] = clib.aspKid()
    data['clib.aspKid(12)'] = clib.aspKid(12)
    data['clib.aspKid(16)'] = clib.aspKid(16)
    data['clib.aspKid(18)'] = clib.aspKid(18)

    data['tplKid-x22'] = clib.tplKid('x22')
    data['tplKid-x35'] = clib.tplKid('x35')
    
    return data

# tmp1
def tmp1Act(mkvs):
    data = {}

    #raise TypeError('one of the hex, bytes, bytes_le, fields, or int arguments must be given')
    #raise Exception('x 不能大于 5。x 的值为: ')

    data['ckey'] = umop.Perm().ckey()
    #xbug.log(data, 'test')

    import uuid

    name = 'test_name'
    #namespace = 'test_namespace'
    data['namespace'] = namespace = uuid.NAMESPACE_URL

    data['uuid1'] = uuid.uuid1()
    data['uuid3'] = uuid.uuid3(namespace,name)
    data['uuid4'] = uuid.uuid4()
    data['uuid5'] = uuid.uuid5(namespace,name)

    data['tmp-enc'] = tmp = parse.bs64('hi--world!1')
    data['tmp-dec'] = tmp = parse.bs64(tmp, 1)

    xbug.log(data, 'test')

    return data

def setsAct(mkvs):
    session['myrnd'] = myrnd = clib.rndKid(6).upper()
    return myrnd

def getsAct(mkvs):
    myrnd = session['myrnd'] if 'myrnd' in session else ''
    return myrnd

def decAct(mkvs):

    data = {'a':{}, 'b':{}}

    data['t1'] = t1 = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    data['t2'] = t2 = '`-=[];,./~!@$%^&*()_+{}|:#' # \'#<>?"

    s1 = list(t1); random.shuffle(s1); 
    s2 = list(t2); random.shuffle(s2); 
    data['s1'] = ''.join(s1)
    data['s2'] = ''.join(s2)

    data['key'] = key = 'PeaceXie123'
    data['原文-'] = orgStr = 'ABCD XYZ 01289 和平鸽 ABCD XYZ 01289 `-=[];,./~!@$%^&*()_+{}|:# '

    data['密文0'] = encStr = parse.enc_main(orgStr, 0, 0)
    data['解密0'] = decStr = parse.enc_main(encStr, 1, 0)
    data['密文a'] = encStr = parse.enc_main(orgStr, 0, 2, key)
    data['解密a'] = decStr = parse.enc_main(encStr, 1, 2, key)

    data['密文1'] = encStr = parse.enc_main(orgStr, 0, 1)
    data['解密1'] = decStr = parse.enc_main(encStr, 1, 1)
    data['密文2'] = encStr = parse.enc_main(orgStr, 0, 2)
    data['解密2'] = decStr = parse.enc_main(encStr, 1, 2)

    data['t1'] = t1 = parse.jwt('1650094305.eyJjb2RlIjoiO11PXiJ9.e720fc94e41d86c6be8265ff04342533', 60)
    data['code'] = code = clib.rndKid(4).upper()

    data['enc1'] = enc1 = parse.enc_main(code, 0, 2, 'CODE')
    data['enc2'] = enc2 = parse.jwt({'code':enc1}, 600)
    #data['enc3'] = enc3 = parse.enc_main(enc2)

    #data['dec3'] = de3 = parse.enc_main(enc3, 1)
    data['dec2'] = de2 = parse.jwt(enc2)
    data['dec1'] = de1 = parse.enc_main(de2['djwt']['code'], 1, 2, 'CODE')


    return data

    """
    enc_key, dec_key = parse.suanfa(key)

    data['密文'] = encStr = parse.bianma(enc_key, orgStr)
    data['解密'] = decStr = parse.bianma(dec_key, encStr)
    #print('原文:',data); print('密文:',miwen); print('解密:',mingwen)
    
    data['enc_key'] = enc_key
    data['dec_key'] = dec_key

    #print "加密明文所得的密文：" + miwen
    #print "解密密文所得的明文：" + mingwen
    """

    return data

