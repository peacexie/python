
import json
from ..core import mkvop

# tpls                       

# xml返回
def rssAct(mkvs):
    data = {'title':'vdataValue'}  
    data['_newtp'] = 'test_tpl/rss.xml' # 设置新模板(xml)
    return data

# htm返回
def htmAct(mkvs):
    data = {'title':'vdataValue'}  
    data['_newtp'] = 'test_tpl/rss' # 设置新模板,默认htm
    return data

def statusAct(mkvs=''):
    #data = {'title':'vdataValue'}  
    #data['_return'] = 'json'
    #return data

    res = {}
    res['_return'] = 'json'
    res['_data'] = 'fail'
    #res = '.bad.' #
    return res

# -----------------------------

"""
# 默认首页方法, 不返回数据或不改变模板可不要
def _homeAct(request, mkvs):
    data = {'testData':'homeData'}
    return data

# 默认列表方法, 不返回数据或不改变模板可不要
def _listAct(request, mkvs):
    data = {'testList':'listData'}
    return data 
 
# 默认详情方法, 不返回数据或不改变模板可不要
def _viewAct(request, mkvs):
    data = {'flagDetail':'viewDetail'}
    data['kid'] = mkvs['key']
    return data
"""

# -----------------------------
