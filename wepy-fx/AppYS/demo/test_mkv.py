
import json
from flask import Flask, g, request, abort

# mkvs

# -----------------------------

# 默认首页方法, 不返回数据或不改变模板可不要
def _homeAct(mkvs):
    data = {'testData':'homeData'}
    return data

# 默认列表方法, 不返回数据或不改变模板可不要
def _listAct(mkvs):
    data = {'testList':'listData'}
    return data 
 
# 默认详情方法, 不返回数据或不改变模板可不要
def _viewAct(mkvs):
    data = {'flagDetail':'viewDetail'}
    data['kid'] = mkvs['key']
    return data

# -----------------------------

def vdataAct(mkvs):
    data = {'testData':'vdataValue'}  
    #abort(403)
    return data

def er500Act(mkvs):
    data = {}
    data['res'] = 8/0        
    return data

# -----------------------------

def utplAct(mkvs):
    data = {'tips':'resetTemplate'}
    data['_newtp'] = '~user-tpl' # 设置新模板
    return data # demo/test/~user-tpl

def rejsonAct(mkvs):
    data = {'jsonTest':'jsonDetail'}
    data['_return'] = 'json' # 直接返回json
    return data

# -----------------------------

# 提示信息: OK
def tipokAct(mkvs):
    data = {'tipok':'tipokDetail'}
    info = {'_retip':1, 'errmsg':'操作成功!', '_data':data}
    return info

# 提示信息: Bad
def errorAct(mkvs):
    data = {'tibad':'tip(bad)Detail'}
    info = {'_retip':0, 'errno':'test:BadMessage', 'errmsg':'It is a TEST BAD Message!', '_data':data}
    return info

# -----------------------------
