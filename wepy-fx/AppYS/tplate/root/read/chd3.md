


### Jinja2 模板


#### 站在巨人的肩上

微爬(Wepy)站在巨人的肩上，直接 [Jinja2](https://palletsprojects.com/p/jinja/) 模板。


#### 默认模板规则

在`MKV路由`的基础上，无需控制器指定模板；可根据mkv路由规则，直接按默认添加模板好就行。

在前一章中，我们在地址栏输入 mkv路由 地址，如果有对应的模板则自己显示，没有模板就出现类似提示："Template `/pc/news/_home.htm` NotFound."


**默认模板规则**

* 首页(url与模板对应)  
  [`/`] = [`/root/`] = [`/root/home`] ->(模板) `/root/home/_home.htm`  
  [`/part/`] = [`/part/home`] ->(模板) `/part/home/_home.htm`  
  [`/part/mod`] ->(模板) `/part/mod/_home.htm`  
* 列表页(url与模板对应)  
  ['/part/mod-k1'] ->(模板) `/part/mod/k1.htm` 或 默认 `/part/mod/_list.htm`  
  ['/part/mod-k1-v1'] ->(模板) `/part/mod/k1-v1.htm` 或 默认 `/part/mod/_list-v1.htm`  
* 详情页(url与模板对应)  
  ['/part/mod.k2'] ->(模板) `/part/mod/k2.htm` 或 默认 `/part/mod/_view.htm`  
  ['/part/mod.k2.v2'] ->(模板) `/part/mod/k2.v2.htm` 或 默认 `/part/mod/_view.v2.htm`  


#### 模板类型

默认模板类型为`.htm`，如果要返回`xml`、`json`等类型，请看后面的控制器与API。


#### 过滤器

微爬(Wepy) 在模板中统一设置了一个过滤器`scut`(ShortCut)，它指向了`AppYS/core/scut.py`模块，作为系统的过滤器；
用户另可扩展`AppYS/core/scut.py`模块，作为用于自定义过滤器。

**常用系统过滤器**

* 项目名称：{{ scut.ucfg('base.name') }}
* 静态目录：{{ scut.config.cins.stpre }}weys/weys.js
* 当前页MKV各值：{{ scut.ucfg('mkvs.mkv') }}、{{ scut.ucfg('mkvs.mod') }}、{{ scut.ucfg('mkvs.view') }}

**轻松解析Markdown**

* 解析md内容：{{ scut.mdinc('read/{key}.md')|safe }}；
* md标题：{{ scut.mdtitle('read/home.md') }}，提示：可用`mod`、`mod`、`view`占位；
* md文件内的变量替换语法: {&#61;ucfg.base.name}；
* md文件内的嵌套包含：{md&#58;"mds/test_md2.md"}。
