


### 部署发布

前面文档所有的运行效果，还仅仅是运行在你本地（机），算是孤芳自享。  
现在，简单提示一下，怎么部署并正式上线！


#### 相关提示

针对web环境搭建，我们对比一下PHP： 各种 PHP 环境套件一大把： phpStudy、宝塔面板、XAMPP、WampServer、PHPNow、EasyPHP、AppServ；
而 基于 Python 的web套件就有些尴尬！

* 宝塔面板  
  正式服务器推荐使用 推荐宝塔面板，可同时部署 php, java, python 等多种环境。

* phpStudy  
  在 phpStudy 基础上运行 flask，参考: [phpStudy + mod_wsgi 部署 Python-flask 项目](https://blog.csdn.net/qq_41129881/article/details/124900278)。  
  文件：`/data/demo-apache-vhost.conf`，这是 phpStudy-Apache 下配置的 可运行本项目的一个配置文件，做参考。

* 笨办法  
  开机运行本地服务: http://127.0.0.1:8083/ (根目录的`@web-run.bat`文件),  
  在现有（或重新安装）的 Apache/Nginx 服务器上，设置一个 `代理` 指向 上面的地址。  
  目的: 只让他运行，效率、安全上待考证！

Sorry! V3 (当前版本) 无线上演示(无服务器) (•◡•)


#### 部署相关文档

* Deployment Options  
  https://flask.palletsprojects.com/en/2.0.x/deploying/

* 宝塔面板 部署python环境  
  https://flask.palletsprojects.com/en/2.0.x/deploying/#hosted-options


#### 旧版 V2 提示

* 微爬(Wepy) - V2  
  演示: http://wepy.txjia.com/  
  环境: Win2008 + IIS + Python3 + Flask1
  

* V2 相关文档/下载  
  文档: http://wepy.txjia.com/tests/docs-dev?fp=doc-link.txt  
  下载: https://github.com/peacexie/python

