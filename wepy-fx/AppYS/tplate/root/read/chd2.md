


### MKV路由


#### MKV路由

MKV路由即由`part分组/model模型/kid/view视图`组成的非常简洁的一组url地址。

**新闻示例**

* 新闻首页：/pc/news
* 行业新闻：/pc/news-field
* 国际新闻：/pc/news-world
* 新闻详情：/pc/news.666

**楼盘示例**

* 楼盘首页：/pc/house
* 某区域楼盘列表：/pc/house-dg
* 楼盘详情：/pc/house.666
* 楼盘下的评论：/pc/house.666.remark
* 楼盘下的出租：/pc/house.666.rent
* 楼盘下的出售：/pc/house.666.sale
* 楼盘下的相册：/pc/house.666.pics


#### MKV意义

上述的 `/pc/` 即一层目录，就是一层分组（`part`）；
假设一个企业网站需要pc/wap两个版本，一个后台管理，一组微信API，一份说明文档，分组规划如下：
`/pc/`,  `/h5/`,  `/admin/`,  `/wxapi/`,  `/demo/`；

上述 `新闻/楼盘` 即是`model`；分类或详情ID `field/world/666` 即使 `kid`；
view视图，如果是一般的模型首页、列表页、详情页则省略，上述中 `remark/rent/sale/pics` 即是`view`视图模式。

每个分组(如果需要)设置一个模板或控制器目录，每个模型(如果需要)对应一个控制器文件；每个view对应一个方法。
无需专门的路由规则表；整个url简洁优雅。


#### MKV规范

* part, model, view 都是由字母、数字、下划线组成；
* kid如果是分类ID则由字母、数字、下划线组成；
* 如果是详情ID，除了可用字母、数字、下划线，还可以由中划线`-`组成；
* 模型+分类用中划线`-`分开，如：`news-field`, `news-world`；
* 模型+详情ID用点`.`分开，如：`news.888`, `news.2022-04-05`。
* 额外的搜索参数，用普通的 `?p1=v1&p2=v2` 形式。

**MKV路由仅仅是一个规范**，[Imcat(贴心猫)](http://txjia.com/imcat/)中在使用，微爬(Wepy)系统中在使用，其他系统中也可沿用！

