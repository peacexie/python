


### 下载安装


#### 基础环境配置

请自行配置如下环境...安装依赖。命令： `>pip install xxx`

**基本环境**

* Python v3.7+
* Flask v2+ (pip安装)

**基本依赖**

* PyMySQL==1.0.2+
* Markdown==3.3.6+
* requests==2.26.0+
* jwt==2.3+ (用户认证)
* pyquery==1.4.3+ (备用)
* click==8.0.3+ (备用)
* pytest==6.2.5+ (测试用)


#### 源码下载与说明

**下载地址**

* https://gitee.com/peacexie/python 微爬(Wepy) V3
* https://github.com/peacexie/python 微爬(Wepy) V2 (旧版参考)

**目录说明(gitee项目)**

* /we-mpro/ --- 微信小程序登录会员演示
* /wepy-dj/ --- 微爬(Wepy) D系列 (基于Django开发)
* /wepy-fx/ --- 微爬(Wepy) F系列 (基于Flask开发)
* /share_static/ --- 微爬(Wepy)共用 (D系列/F系列) 静态资源

**微爬(Wepy) F系列 目录说明**

* /AppYS/ --- app主体文件（含类库包，模板，控制器等）
* /data/ --- 扩展配置、缓存、日志、数据库 等
* /@cli-win.bat --- Windows下-命令行:运行入口批处理 
* /@web-run.bat --- Windows下-web服务:启动入口
* /cli-entry.py --- 命令行:py运行入口
* /mul-proc.py --- 命令行:多线程入口
* 测试、练习目录 --- `/exlog/`, `/test1/`, `/tests/` 等 (可删除)


#### 本地配置运行

**配置**

如需要，配置一下：`python/wepy-fx/AppYS/confg.py`，如数据库；其中`dbcfgs.0`可知己用贴心猫的数据库。

**运行**

* 直接点击如下文件运行：
* [A.] /wepy-fx/@cli-win.bat --- Windows下-命令行:运行入口批处理
* [B.] /wepy-fx/@web-run.bat --- Windows下-web服务:启动入口
* Linux下请自行写个s脚本。
* 打开 `http://127.0.0.1:8083/` 查看效果，  
  或查看本文档: `http://127.0.0.1:8083/root/read` 。

