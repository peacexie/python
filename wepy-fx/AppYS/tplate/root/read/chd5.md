


### Db/Model/User 操作

微爬(Wepy)内置丰富的数据库、模型、用户操作。
目前支持 sqlite, mysql。


#### Db 操作

mysql数据库结构 + 查询

* `db = dbop.Db('0')`  
    `res = db.tbdic()`  
    `tabStru = db.tbdic(1)`# mysql数据库结构  
    `sql = "SELECT * FROM {types_china} WHERE TITLE LIKE %s AND pid IN('cngx','cnhn')"`  
    `data['lists'] = db.get(sql, ["%郴州%",], 8)` # 查询 
* `db = dbop.Db('blog')` # sqlite数据库结构 - flask 演示数据库   
    `res = db.tbdic()`  
    `tabStru = db.tbdic(1)`
* `db = dbop.Db('mdlog')` # sqlite数据库结构 - MD博客数据库   
    `aid = '2022-51-8826' #clib.uparam('aid')`  
    `sql = "SELECT * FROM {post} p WHERE aid=?"`  
    `data = db.get(sql, (aid,), 1)`


提示：

* 数据库配置位置：/AppYS/config.py > dbcfgs.xxx；
* 使用`{表名}`会自动替换表前后缀。


#### Model 操作

模型操作，兼容[Imcat 贴心猫](http://txjia.com/imcat/)数据结构。

* `data = {}`  
    `news = dmop.Model('news')`  
    `data['row-1'] = news.get("did='2020-9q-80a9'", 1)`  
    `data['list-2'] = news.get("vtype='ptxt'", 2)`  
    `data['list-page'] = news.get("vtype='ptxt'", '2,2')`   
    `data['list-order'] = news.get("", '2', "title DESC")`  
    `data['list-count'] = news.get("`show`='all'", 'count')`  
    `data['list-def'] = news.get()`  
    `news2 = dmop.Model('news.join')`  
    `data['list-join'] = news2.get("vtype='ptxt'", "6", "title DESC")`  
    `data['news-cfg'] = news2.cfg`  
    `data['list-2b'] = dmop.Model('news').get('',2)`  


#### User 操作

用户操作，兼容[Imcat 贴心猫](http://txjia.com/imcat/)系统的用户加密方式；  
注意配置：/config.py > safe 部分的加密key与贴心猫一致。

* 支持 cookie / jwt 认证：
* web演示url: `/root/uio` (注册，登录，登出)
* 小程序-api演示: 微信小程序开发工具打开目录 `/we-mpro/`(项目) 测试。

```
    user = umop.Ubase('person') # 用户实例
    xreg = user.reg(post['uname'], post['upass'], {'mname':'Wepy测试'}) # 注册
    xuinfo = user.login(post['uname'], post['upass']) # 登录
    xuinfo = user.get(post['uname'], '', 'idpwd') # 由账号获取用户信息
    xbind = user.bind(openid, appid, 'wxmpro') # 绑定微信openid
    xuinfo = user.get(openid, appid, 'wxmpro') # 由微信openid获取用户信息
```

