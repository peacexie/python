


### 进阶应用
 
微爬(Wepy)：命令行/多线程，可用于API操作，多线程爬虫等场景。


#### 命令行(CommandCli)

Windows下，直接单击`/wepy-fx/@cli-win.bat`文件执行。

![微爬(Wepy):命令行(CommandCli){@class=ims1}](/share_static/demo/wepy-cmd.png)

Linux下，可自行编写 .sh脚本 文件，欢迎网友提供！


#### 多线程(Multithreading)

运行入口: 
`/wepy-fx/python mul-proc.py`

![微爬(Wepy):多线程(Multithreading)](/share_static/demo/wepy-mul.png)

提示：请根据需要，自行配置需要多线程运行的`mkv路由表`；  
可以设置一个计划任务，指向此运行入口。

