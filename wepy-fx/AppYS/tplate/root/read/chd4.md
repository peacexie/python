


### 控制器


#### 控制器作用

简单的页面：如介绍页、md文档，**无需控制器**；官方内置文档就是很好的例子：一个首页模板，一个列表(章节)页模板，其他就是N个md文档。

处理交互逻辑如登录认证，或处理复杂数据：用控制器为最佳选择。

另外，使用控制器还可指定特定模板（不受默认模板限制），控制输出类型 如`xml`、`json`。


#### 控制器方法

每个模型可对应一个控制器文件（python模块）；每个view对应一个方法（函数）：

* 控制器`/AppYS/pc/news.py`样例
* `/pc/news` -> `_homeAct`(默认首页方法)
* `/pc/news-world` -> `worldAct`(world分类专有方法[有限])、`_listAct`(默认列表页方法)
* `/pc/news.666` -> `_viewAct` (默认详情页方法)


#### 控制器返回

所有数据，直接返回dict字典，无需类似 `return render_template(tplname, data=data, scut=scut)` 长长的语法；
特殊处理，直接在dict字典添加特殊健值：

* 返回普通数据  
    `data = {'testData':'vdataValue'}`  
    `return data`
* 提示信息: OK  
    `data = {'tipok':'tipokDetail'}`  
    `info = {'_retip':1, 'errmsg':'操作成功!', '_data':data}`  
    `return info`  
* 设置模板  
    `data = {'title':'vdataValue'}`  
    `data['_newtp'] = 'test_tpl/rss.xml' # 设置新模板(xml)`  
    `return data`  
* 返回json  
    `data = {'title':'vdataValue'}`  
    `data['_return'] = 'json'`  
    `return res`  
* 返回字符串(状态)  
    `res = {}`  
    `res['_return'] = 'json'`  
    `res['_data'] = 'fail'`  
    `return res`  


#### API-JSON返回

临时少量json返回，如上操作。
如果是专门的api操作，整个路由分组下的控制器-方法都返回JSON，那可以设置一个参数，无需每次指定`data['_return'] = 'json'`：

* 参数位置：`/AppYS/config.py` > vmode.vapi；
* 参数值：`'vapi': ['wxapi', 'baiduapi'],`，即 wxapi, baiduapi 路由分组按api方式默认返回JSON数据。

