


### 博客/小程序 综合演示

图片认证码 / jwt：用于表单认证。
综合演示： Markdown 博客。
小程序小程序 用户授权、MD博客 操作。


#### 图片认证码

代码: `<img src="/vcode?mod=test1">`，效果：<img src="/vcode?mod=test1" style="width:120px;height:36px"> (此图片需要运行web服务方可正常显示)；  
认证参考: `post['my_code'] = session['vcode_test1'].upper()`。


#### JSON Web Token

微爬(Wepy)：实现了更精简的 jwt 认证功能。

```

    data1 = {  # 内容，一般存放该用户id和开始时间
        'uid': '2024-ab-6688', # ''
        'uname': 'peacexie@163.com', # (Guest)
        'mname': '和平鸽', # (游客)
        'sid': 'f7a0fd45-b9fd-11ec-bab0-d017c295a9cb',
    }
    jwtstr = parse.jwt(data1) # 编码
    jwtlen = len(jwtstr)
    data2 = parse.jwt(jwtstr) # 解码
```


#### MD 博客

详见 [MD 博客](/umc/mdlog)

* 博客正文可解析 Markdown 文件，游客可浏览但不能管理。
* 主要演示数据库的增删改查 与 分页；演示用户登录管理，权限判断。
* 用户部分使用Mysql数据库，博客部分使用Sqlite数据库，用于演示多库调用。



#### 微信小程序

用户授权、MD博客 操作

**功能**

* 用户授权登录，jwt认证
* MD博客 增删改查

**代码**

* /we-mpro/
* 请使用 微信开发工具 打开此目录调试


