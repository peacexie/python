

#### test_md1 ： 语法测试

* Markdown 动态解析
* 默认模板: `_list.htm`
* 其他文字


#### test_md2 ： 语法测试2

A list with items and sub-items:

* First item containing a sub-list and an additional paragraph:

    - First sub-item
    - Second sub-item
    
    Additional paragraph in first item

* Second item

    * 1.2 sub-item
    * 2.2 sub-item


#### md xxx


![微爬(Wepy):命令行(CommandCli){@class=ims1}](/share_static/demo/wepy-cmd.png)
![微爬(Wepy):多线程(Multithreading)](/share_static/demo/wepy-mul.png)

---


<!--
<p class='mimg'>
    <img alt="微爬(Wepy) - Command" src="/share_static/demo/wepy-cmd.png" />
    <br>Command for 微爬(Wepy)
</p>
-->


aaa


bbb

```php
    $a = func1();
    $b = 'Peace'
```

ccc{@class=tc}

aa, myhomepage is: http://www.xyx.com/ , Wxx

ddd

Some *emphasized{@id=bar}* text.
![Alt text{@id=baz}](path/to/image.jpg)

_AAA_

!!! type "optional explicit title within double quotes"
    Any number of other indented markdown elements.

    This is the second paragraph.

