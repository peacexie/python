
import math, random
from ..core import clib, fdir, scut #, mkvop
from PIL import Image, ImageDraw, ImageFont, ImageFilter

""" image
    图像相关
    1/5 :  51=x33  ## 33, 66, 99, CC : 6^3=216
    1/4 :  64=x40  ## 40, 80, C0     : 5^3=125
    1/3 :  85=x55  ## 55, AA         : 4^3= 64
"""

# TAB # ''.join(tb3), tuple(tb3)

ctabK = '6' # 4,5,6
ctabN = {}; ctabA = {}; ctabB = {}

ctabN['4'] = {'0':0, '5':85, 'A':170, 'F':255} # gap:5 : 0-5-A-F
ctabN['5'] = {'00':0, '40':64, '80':128, 'C0':192, 'F0':255} # gap:8 : 00:80:C0:F0 
ctabN['6'] = {'0':0, '3':51, '6':102, '9':153, 'C':204, 'F':255} # gap:6
ctabA['6'] = {
    '0':['6','9','C','F'],
    '3':[    '9','C','F'],
    '6':['0',    'C','F'],
    '9':['0','3',    'F'],
    'C':['0','3','6'    ],
    'F':['0','3','6','9'],
}
ctabB['6'] = {
    '0':[    '3'],
    '3':['0','6'],
    '6':['3','9'],
    '9':['6','C'],
    'C':['9','F'],
    'F':['C'    ],
}

{'0':51, '3':102, '6':153, '9':102, 'C':255, 'F':204}

def tabrc(n=6, rc3=0, rx=0): 
    tbxn = ctabN['6']; tba6 = list(tbxn.keys())
    random.shuffle(tba6); tb3 = tba6[0:3]; 
    # one
    it = ''.join(tb3) if rc3 else (tbxn[tb3[0]],tbxn[tb3[1]],tbxn[tb3[2]])
    if not n:
        return it;
    # RGB
    ts1 = []; ts3 = []; 
    xs1 = []; xs3 = []; 
    for i in range(3):
        ts1.append(ctabA['6'][tb3[i]])
        xs1.append(ctabB['6'][tb3[i]])
    # return n + 1
    ts3.append(it)
    for i in range(n): 
        c1 = random.choice(ts1[0]); c2 = random.choice(ts1[1]); c3 = random.choice(ts1[2]); 
        it = (c1+c2+c3) if rc3 else (tbxn[c1],tbxn[c2],tbxn[c3]); ts3.append(it); 
        c1 = random.choice(xs1[0]); c2 = random.choice(xs1[1]); c3 = random.choice(xs1[2]); 
        it = (c1+c2+c3) if rc3 else (tbxn[c1],tbxn[c2],tbxn[c3]); xs3.append(it); 
    #print("\n\n",tb3,":","\n",ts1,"\n",ts3,"\n")
    return ts3 if rx else [ts3,xs3]

# 绘制干扰线条
def vc_lines(draw, size):
    line_num = random.randint(*(2, 3)) # 干扰线条数
    for i in range(line_num):
        # 起始点
        begin = (random.randint(0, size[0]), random.randint(0, size[1]))
        # 结束点
        end = (random.randint(0, size[0]), random.randint(0, size[1]))
        draw.line([begin, end], fill=(0, 0, 0))

# 图形扭曲
def vc_trans(image, size):
    params = [ # 参数
        1 - float(random.randint(1, 2)) / 100,
        0, 0, 0,
        1 - float(random.randint(1, 10)) / 100,
        float(random.randint(1, 2)) / 500,
        0.001,
        float(random.randint(1, 2)) / 500
    ]
    # 创建扭曲
    image = image.transform(size, Image.PERSPECTIVE, params)
    #原文链接：https://blog.csdn.net/kongsuhongbaby/article/details/119083806


def vcode(vcode=''):

    # 输出文字:
    if vcode=='':
        vrnd =  random.randint(3, 5)
        vcode = clib.rndKid(vrnd).upper()
    else:
        vrnd = len(vcode)
    gap = (5-vrnd) * 11.5
    size = (120, 36) # width = 120; height = 40; 
    image = Image.new('RGB', size, (255, 255, 255)) # 创建Font对象:
    
    # 创建Draw对象, 填充
    draw = ImageDraw.Draw(image)
    ctb2 = tabrc(vrnd, 0, 1); #print("\n\n", vcode, ctb2);
    ctbA = ctb2; ctbB = ctb2
    for x in range(size[0]):
        for y in range(size[1]):
            draw.point((x, y), fill=ctbA[0])

    for t in range(len(vcode)):
        fpre = fdir.fpbase('app') + '/../../share_static/ui3nd/fonts/wch-' 
        frec = random.choice([['avant',0], ['bvsans',3], ['miriam',3]]); #frec = ['bvsans',0]
        font = ImageFont.truetype(fpre+frec[0]+'.ttf', 30)
        draw.text((24*t+3+gap+1, frec[1]),   vcode[t:t+1], font=font, fill=ctbB[t+1]) # x+1
        draw.text((24*t+3+gap,   frec[1]+1), vcode[t:t+1], font=font, fill=ctbB[t+1]) # y+1
        draw.text((24*t+3+gap,   frec[1]),   vcode[t:t+1], font=font, fill=ctbA[t+1])

    #vc_lines(draw, size)
    #vc_trans(image, size)

    return image, vcode


"""

"""
