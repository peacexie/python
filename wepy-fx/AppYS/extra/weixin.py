
import time, json
#from flask import Flask, g, current_app, request
from ..core import parse, mkvop, dbop, dmop, umop, scut, xbug, urlpy

""" weixin
    mpid, mpro, qiye
"""

# 基础接口类
class base:

    def __init__(self, appid='', secret=''):
        self.host = 'https://api.weixin.qq.com'
        self.init(appid, secret)

    def init(self, appid='', secret=''):
        self.appid = appid
        if secret:
            self.secret = secret
        else:
            wxcfg = scut.read('weixin', 'ext')
            if appid in wxcfg:
                self.secret = wxcfg[appid]['secret']
            else:
                self.secret = ''

    # 获取 access_token, 公众号,小程序
    def tkget_api(self):
        api = f"{self.host}/cgi-bin/token"
        params = {
            'appid': self.appid,
            'secret': self.secret,
            'grant_type': 'client_credential',
        }
        res = urlpy.get(api, params, refmt='json')
        return res

    # 获取 access_token, xxx
    def tkget_xx(self):
        return {}

    # 获取 access_token, 有缓存
    def tkget(self, mode='', fix=''):
        sql = "SELECT * FROM {token_store} WHERE kid=%s AND exp>%s"
        exp = int(time.time())
        db = dbop.Db();
        row = db.get(sql, (self.appid+fix,exp), 1)
        if row:
            return row['token']
        else:
            if mode=='xx': # 扩展
                res = self.tkget_xx()
            else:
                res = self.tkget_api()
            if 'errcode' in res:
                scut.vdie(f"tkget Error:[{res['errcode']}]{res['errmsg']}")
            upd = (self.appid+fix, res['access_token'], exp+5600, exp)
            sql = "REPLACE INTO {token_store} (kid,token,exp,etime) VALUES (%s,%s,%s,%s)"
            db.exe(sql, upd)
            return res['access_token']
    
    # code 换取 openid
    def code2openid(self, code, mode=''):
        api = f"{self.host}/sns/jscode2session"
        params = {
            'appid': self.appid,
            'secret': self.secret,
            'js_code': code,
            'grant_type': 'authorization_code',
        }
        res = urlpy.get(api, params)
        if 'errcode' in res:
            scut.vdie(f"code2openid Error:[{res['errcode']}]{res['errmsg']}")
        
        return res

# 公众号接口类
class mpid(base):

    def xx1():
        pass


# 小程序接口类
class mpro(base):

    def xx2():
        pass


# 企业微信接口类
class qiye:

    def __init__(self, appid='', secret=''):
        pass

    def xx2():
        pass


"""



"""

