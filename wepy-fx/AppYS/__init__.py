
from .core import appys
#from . import config

""" app-web(web): 
    run.mcli:0
"""

def create_app(appcfg=None):

    app = appys.init(appcfg)

    from .blog import auth # --- demo:blog ---
    app.register_blueprint(auth.bp)

    from .blog import blog
    app.register_blueprint(blog.bp)

    return app

"""
    @app.route('/demo-hello') # --- 自定义代码 --- 
    def demo_hello():
        return 'Hello Demo'
"""
