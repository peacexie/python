
import os, io, random #, sys, time
from flask import current_app, make_response, redirect, session #, render_template, request
from .. core import mkvop, scut, clib, parse #, fdir
from .. extra import image
from .. import config

""" root(Biuld in Actions / 内置视图): 
    -
"""

noclis = ['vcode'] # 不支持cli运行

# 默认首页方法 `vdef` 固定方法, 首页专用
def vdef(mkv=''): 
    #return redirect('/root/read') # 自定义跳转
    return mkvop.tpl('root', 'home') # 默认首页

# 验证码前背景色对比
def vcodeAct(mkvs={}):
    data = {}
    for i in range(3):
        data[i] = image.tabrc(6, 1)
    return data

# 测试 `xxxAct` 用 `/root/home-xxx` 访问 (mkv通用)
def hiAct(mkv=''):
    return 'Hi, run in hiAct.'

# 测试 `xxxDo` 用 `/xxx` 访问 (home.py专用)
def hiDo(mkv=''):
    return 'Hi, run in hiDo.'

# 验证码
def vcodeDo(mkvs={}):
    pex = scut.gvget('run.pex')
    mod = pex['mod'] if 'mod' in pex else '_def'
    vrnd =  random.randint(3, 5)
    session['vcode_'+mod] = vcode = clib.rndKid(vrnd).upper()
    # jwt-vcode
    imgem, code = image.vcode(vcode) # code是验证码
    imgio = io.BytesIO()
    imgem.save(imgio, 'jpeg') # png,jpeg,gif
    iodata = imgio.getvalue()
    return mkvop.mkresp(iodata, 'jpeg')

# 运行内置视图
def run(mkv=''):
    obj = __import__('AppYS.root.home', fromlist='..')
    func = getattr(obj, mkv+'Do', '')

    if '/'+mkv in config.stfps:  # 根目录静态文件 
        return current_app.send_static_file(config.stfps['/'+mkv])
    elif func:                   # 内置`Action`视图
        return func(mkv)
    else:                        # Message
        return f"Action `{mkv}Do` NotFound."

"""
    * TODO:
    1. noclis = ['vcode'] # 不支持cli运行
    2. /root/home-vcode   # Error
    3. /vcode >优化 /root/home-vcode
    * jwt-vcode
    enc1 = parse.enc_main(vcode, 0, 2, 'CODE')
    enc2 = parse.jwt({'vcode':enc1}, 600)
    dec2 = parse.jwt(enc2)
    dec1 = parse.enc_main(dec2['djwt']['vcode'], 1, 2, 'CODE')
    * response
    #response = make_response(iodata)
    #response.content_type = 'image/jpeg'
    #return response
"""
