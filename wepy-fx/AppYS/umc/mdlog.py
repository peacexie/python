
import os, time, datetime, json #, jwt
from flask import redirect, request, session
from ..core import parse, mkvop, dbop, dmop, umop, scut, xbug, urlpy, clib
from .. import config

""" mdlog(MarkDown-Blog/ MD博客): 
    
"""

# =======================================================

# 默认首页方法
def _homeAct(mkvs):
    utok = chkLogin(mkvs)

    data = listAct(mkvs)
    del data['_return']

    data['utok'] = utok
    return data

# 详情
def _viewAct(mkvs):
    utok = chkLogin(mkvs)

    data = rec1(mkvs, 0)
    return data

# 管理列表
def adminAct(mkvs):
    utok = chkLogin(mkvs, 1)

    data = listAct(mkvs)
    del data['_return']

    data['utok'] = utok
    return data

# =======================================================

# 一页列表
def listAct(mkvs):
    #os.environ['TZ'] = 'Asia/Shanghai'
    db = dbop.Db('mdlog')
    page  = clib.uparam('page','1')
    limit = clib.uparam('limit','5')
    offset = (int(page)-1) * int(limit)
    
    keywd = clib.uparam('keywd','')
    field = "SELECT p.aid, title, body, atime, auser"
    sfrom = "FROM {post} p " 
    where = "WHERE title LIKE '%"+keywd+"%'" if keywd else ''
    filters = "&keywd="+keywd if keywd else ''
    slimit = 'LIMIT '+str(offset)+','+str(limit)

    pager = db.pager(f"{sfrom} {where}", page, limit)
    pager['keywd'] = keywd #; print(ctemp,pager)
    sql = f"{field} {sfrom} {where} ORDER BY atime DESC {slimit}"
    posts = db.get(sql, ())

    data = {'posts':posts, 'pcnt':len(posts), 'pager':pager, 'filters':filters}
    data['_return'] = 'json' # 直接返回json
    return data

# 一笔数据
def rec1(mkvs, mode='json'):

    aid = mkvs['key'] if mkvs['type']=='view' else clib.uparam('aid')
    sql = "SELECT * FROM {post} p WHERE aid=?"

    db = dbop.Db('mdlog')
    data = db.get(sql, (aid,), 1)
    if not data:
        scut.vdie('该页无法找到!', 'json')
    return data

# 登录检查
def chkLogin(mkvs, die=0, vret=''):
    data = {}
    utok = umop.heartBeat(ckeep=1) 

    if utok and 'djwt' in utok and 'uname' in utok['djwt']:
        user = umop.Ubase() 
        uinfo = user.get(utok['djwt']['uname'])
        del uinfo['uacc']['upass']
        data = {**uinfo, **utok}
    else:
        if die:
            scut.vdie('No-Login, 请登录使用!', vret)
        #redirect('/root/uio') # 自定义跳转

    return utok

# =======================================================

def addAct(mkvs):
    utok = chkLogin(mkvs, 1)
    uname = utok['djwt']['mname']
    data = {'title':'', 'body':'', 'auser':uname, 'atime':time}
    data['id'] = ''
    data['_newtp'] = 'mdlog/edit.htm' # 设置新模板,默认htm
    return data

def aedoAct(mkvs):
    data = {}
    utok = chkLogin(mkvs, 1, 'api')
    #data = rec1(mkvs, 0)
    post = clib.upost()
    data['post'] = post

    tab = ['title','auser','body','atime']
    for tk in tab:
        if not tk in post or not post[tk]:
            scut.vdie('请填写完整!', 'api')

    db = dbop.Db('mdlog')
    aid = clib.uparam('aid') #request.args.get('aid','') 
    if aid:
        sql = "UPDATE {post} SET title=?, body=?, auser=?, atime=? WHERE aid = ?"
        data['rdb'] = db.exe(sql, (post['title'], post['body'], post['auser'], post['atime'], aid))
    else:
        aid = clib.tplKid()
        sql = "INSERT INTO {post} (aid, title, body, auser) VALUES (?, ?, ?, ?)"
        data['rdb'] = db.exe(sql, (aid, post['title'], post['body'], post['auser']))

    data['utok'] = utok
    data['_return'] = 'json' # 直接返回json
    return data

def editAct(mkvs):
    #os.environ['TZ'] = 'Asia/Shanghai'
    utok = chkLogin(mkvs, 1)
    data = rec1(mkvs, 0)

    rejson = clib.uparam('rejson')
    if rejson:
        data['mdstr'] = parse.mdinc(data['body'],0)
        data['_return'] = 'json' # 直接返回json
    return data

def delAct(mkvs):
    utok = chkLogin(mkvs, 1, 'api')
    data = {}
    aid = clib.uparam('aid')

    sql = "DELETE FROM {post} WHERE aid=?"
    db = dbop.Db('mdlog')
    data['res'] = db.exe(sql, (aid,))

    data['_return'] = 'json' # 直接返回json
    return data

"""
    
"""
