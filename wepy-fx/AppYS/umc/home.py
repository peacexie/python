
import time, datetime, json #, jwt
from flask import redirect, request, session
from ..core import parse, mkvop, dbop, dmop, umop, scut, xbug, urlpy, clib
from .. import config

""" umc(User-Member Center/ 用户中心): 
    -
"""

# 默认首页方法
def _homeAct(mkvs):
    data = {}
    utok = umop.heartBeat()

    if utok and 'djwt' in utok and 'uname' in utok['djwt']:
        user = umop.Ubase() 
        uinfo = user.get(utok['djwt']['uname'])
        del uinfo['uacc']['upass']
        data = {**uinfo, **utok}
        data['vname'] = uinfo['umod']['mname'] if len(uinfo['umod']['mname']) else utok['djwt']['uname']
    else:
        return redirect('/root/uio-login') # 跳转到登录

    #umop.clearAutok(data['djwt']) # Test
    return data

"""
    
"""
