
#import os
from flask import Flask
from . import mkvop, scut, vfmt
from .. root import home
from .. import config

""" app-ys: 
    app = appys.init(): 
"""

def init(appcfg=None):

    # app实例 
    app = Flask(
        __name__, 
        template_folder = '../' + config.cins['tpl_dir'],
        static_folder = '../' + config.cins['static_dir'], # static_folder
        #instance_relative_config = True,
    )

    # app配置
    app.config.from_mapping(config.capp)
    if appcfg:
        app.config.from_mapping(appcfg)
    #os.environ['TZ'] = 'Asia/Shanghai'

    # --- errors & filters ---------------------

    @app.errorhandler(Exception)
    def error_500(ex):
        return mkvop.vexc()

    # filters # 尽量使用 scut.xxx
    reg = 0
    for fkey in vfmt.__dir__():
        if fkey=='_demo_filter':
            reg = 1
        if reg:
            app.jinja_env.filters[fkey] = getattr(vfmt, fkey)

    # --- hooks ---------------------

    @app.before_request
    def before():
        scut.gvinit()
        mkvop.chk_path()

    @app.teardown_appcontext
    def teardown(exc):
        pass # close_db()

    # --- demo:mkv|api ---------------------

    @app.route('/') # 首页路由
    def vf_home():
        return home.vdef()

    @app.route('/<string:mkv>') # 内置视图
    def vf_biact(mkv=''):
        return home.run(mkv)

    @app.route('/<path:path>', methods=['GET','POST']) # 默认路由`/part/mkv`
    #@mkvop.cross # for api
    def vf_mkdef(path=''):
        return mkvop.vdef(path)

    return app
