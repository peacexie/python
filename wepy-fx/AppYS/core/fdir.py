
import sys, os, time, re, hashlib, markdown
from . import scut
from .. import config

""" 文件目录操作(fdir=file-directory): 
    `file`为python系统函数, 使用`fdir`为包名
"""

# 基础目录(app,tpl,core)
def fpbase(sec='app'):
    dir_core = os.path.dirname(__file__)
    dir_app = os.path.dirname(dir_core)
    if sec=='tpl':
        udir = dir_app +'/'+ config.cins['tpl_dir']
    elif sec=='ins':
        udir = os.path.dirname(dir_app) +'/'+ 'data'
    else: # app
        udir = dir_app
    return udir

# 为文件自动添加相对目录: read -> part/mod/read.md
def fpfix(file, mkvs={}, ext=''):
    if not 'mkv' in mkvs:
        mkvs = scut.gvget('mkvs') 
    if not '/' in file:
        file = mkvs['part'] +'/'+ mkvs['mod'] +'/'+ file
    elif file.count('/')==1: # not (mkvs['part']+'/') in file:
        file = mkvs['part'] +'/'+ file
    if ext and not ext in file:
        file += ext
    return file

# 文件完整路径 root/info.htm -> {proj}/tphtml/root/info.htm
def fpfull(fp, key='base'):
    path = fpbase('app') #settings.BASE_DIR;
    if key=='tpl':
        path = fpbase('tpl') #settings.TEMPLATES[0]['DIRS'][0]
    return str(path) + '/' + fp;

def get(fp, cset='utf-8', mode='rb'):
    flag = os.path.exists(fp)
    if not flag:
        return ''
    data = ''
    fh = open(fp, mode)
    data = fh.read()
    fh.close()
    if cset:
        data = data.decode(cset)
    return data

def put(fp, data): 
    with open(fp, 'a', encoding='utf-8') as f:
        f.write(data)
    f.close()


# 自动文件名
def fpauto(url, full=1):
    if len(url)==0:
        url = 'index.htm'
    file = url if (full or not os.path.basename(url)) else os.path.basename(url)
    file = re.sub("http(s)?://",'',file)
    file = file.replace('/','!').replace('?','---').replace('&',';')
    file = re.sub('[*:|]','-',file)
    file = re.sub('[\'"<>%#]','',file)
    if len(file)>96: # 160
        file = re.sub('[\u0080-\uffff]','',file)
        m5 = hashlib.md5(file.encode("latin1")).hexdigest()
        file = file[:60] +'---'+ m5 +'---'+ file[-30:]
    reg = r'\.(jpg|jpeg|gif|png|htm|html)$'
    if not re.findall(reg, file):
        file += '.htm';
    return file
