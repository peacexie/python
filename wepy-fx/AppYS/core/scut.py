
import os, sys, time, re, math, json
from flask import g, has_app_context
from . import fdir, parse, dbop, dmop, xbug
from .. import config

""" 助手函数(helpers:(捷径)scut=shortcut): 
    `help`为python系统函数, 改用`scut`为包名
"""

# =============================

# raise Exception
def vdie(message, vret=''):
    if vret:
        gvset('run.vret',vret)
    raise Exception(message)

# 获取get参数
def param(key, area='get'):
    params = gvget('run.pex')
    return params[key] if key in params else ''
    # cli下, request.args.get('key')提示`Working outside of request context`

# user-config, runVars
def ucfg(keys, gv=0):
    if gv or keys[0:4] in 'run.mkvs.':
        cfgs = gvget(0)
    else:
        cfgs = config.ucfg
    return parse.dkget(cfgs, keys)

# make-url
def mkurl(mkv=''):
    if mkv.find(':')>0:
        path = '/' + mkv.replace(':','/')
    else:
        path = './' + mkv
    # path += '.htm' # 伪静态
    return path

# markdown-include
def mdinc(key, rf=1):
    return parse.mdinc(key, rf)

# markdown-mdtitle
def mdtitle(key, rf=1):
    return parse.mdtitle(key, rf)

def trace(flag=''):
    return xbug.trace(flag)

# debug-info (运行信息) // Used:0.0(ms); root/error.htm; Time:2021-12-12 18:35:55
def irun(tp=''):
    return xbug.irun(tp)

# 替换路径
def redir(path):
    return xbug.redir(path)

# 分页信息
def pager(rcnt, page=1, limit=10):
    return dbop.pager(rcnt, page, limit)

# =============================

# 获取db对象 : scut.db().get("SELECT * FROM {docs_news} LIMIT 1")
def db(dbkey='0'):
    return dbop.Db(dbkey)

# 获取模型对象 : scut.mod('news').get('',2)
def mod(mod, dbkey='0'):
    return dmop.Model(mod, dbkey)

# 获取用户对象 : 
def user(mod, dbkey='0'):
    return dmop.user(mod, dbkey)
    pass

# 读取缓存 : scut.read('grades', 'dic|ext')
def read(fp, path='dic'):
    base = fdir.fpbase('ins') 
    jsonStr = fdir.get(f'{base}/{path}/{fp}.json')
    data = json.loads(jsonStr)
    return data

# === Global-Vars OP ==========================

# global 不能跨文件？(https://www.cnblogs.com/rnckty/p/7722603.html)
def gvinit():
    global _runVars
    run = { 
        'stamp':time.time(),
        'qtime':0, 'qcnt':0, 'memory':0, 'tpname':'',
        'mcli':0, # mode-cli(0-web:网页,1-cli:模式,2-cmp:多线程)
        'vret':'tpl', # view-return(tpl-模板,api-接口)
        'm3rd':0, # mode-3rd(第三方接口,如支付回调)
        'pex':{}, # 额外参数
        'cookie':(), # cookie
        'user':{'uid':'-', 'uname':'-', 'mname':'(Guest)'} # user
    }
    _runVars = {'run':run, 'mkvs':{}}

    #if has_app_context():
    #    g.run = run
    #    g.mkvs = {}

def gvset(gk, gv=''):
    global _runVars #;print(gk)
    _runVars = parse.dkset(_runVars, gk, gv)

    #if has_app_context():
    #    g = _runVars

def gvget(gk):
    global _runVars
    if not gk:
        return _runVars
    return parse.dkget(_runVars, gk)

# =============================

"""
    for k0 in ['mod', 'tab','odf','fpk']:
        cfg[k0] = vars()[k0]
"""
