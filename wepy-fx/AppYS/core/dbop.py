
import os, sys, math
from . import scut, dbop
from .. import config
#import pymysql, sqlite3, pymssql ... # 动态加载: 


# === func ===========================

# 分页信息(也可用于文本分页)
def pager(rcnt, page=1, limit=10):
    rcnt = int(rcnt); limit = int(limit)
    page = int(page); pcnt = math.ceil(rcnt/limit)
    data = {'rcnt':rcnt, 'page':page, 'pcnt':pcnt}
    data['first'] = 1
    data['prev'] = max(1, page-1)
    data['next'] = min(pcnt, page+1)
    data['last'] = pcnt
    return data

# 组装sql, 添加a/e附加字段, fmt值
def dicsql(xdic, xmode='a/e/0', xfmt={}):
    sqtu = []; sql1 = sql2 = '' # (uid, uno, uname, upass, umods) VALUES(?, ?, ?, ?, ?)"
    for key in xdic:
        sql1 += (', ' if sql1 else '') + key
        sql2 += (', ' if sql2 else '') + '?'
        val = xdic[key] # TODO...
        sqtu.append(val)

    sql = f"({sql1}) VALUES ({sql2})"
    return {'sql':sql, 'sqtup':tuple(sqtu)}

# [使用sqlite3将查询结果(result)]从元祖转换成字典（dict）
def dict_row(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

def tkset():
    pass

def tkget():
    pass

# === Db类 ===========================

""" 数据库操作(database-opration)

    * db = dbop.Db('0')
      - sql = "SELECT * FROM {types_china} WHERE deep=%s AND TITLE LIKE %s AND pid IN('cngx','cnhn','cnzj')"
      - data['lists'] = db.get(sql, ['2',"%州%"], 8)
      - res = db.tbdic()
      - resDetail = db.tbdic(1)
      - data['tabNews'] = resDetail['docs_news_ys']
      - data['tab'] = res

    * db = dbop.Db('blog')
      - sql = "SELECT * FROM {post} WHERE title LIKE ?"
      - data['lists'] = db.get(sql, ["%e%"], 8)
      - res = db.tbdic()
      - resDetail = db.tbdic(1)
      - data['tabPost'] = resDetail['xtest_post']
      - data['tab'] = res

    * 占位符 ? %s 可以互换，做了兼容

"""

class Db:

    def __init__(self, key=None):
        cfg = config.dbdef # 默认db配置
        dbcfgs = config.dbcfgs
        if key and key in dbcfgs:
            cfg = dbcfgs[key]

        self.cfg = cfg
        self.dbcon()

    def close(self):
        self.cur.close()
        self.con.close()

    # -----------------------

    def dbcon(self):
        cfg = self.cfg
        func = 'dbcon_' + cfg['type']
        if func in dir(self):
            getattr(self, func)(cfg)
        else: # 'Error connect @ AppYS.core.dbop' 
            scut.vdie(f"DB-type `{cfg['type']}` Not Support(@dbcon).") 

    def dbcon_mysql(self, cfg):
        import pymysql
        self.con = pymysql.connect(
            host = cfg['host'], port=cfg['port'], 
            user = cfg['user'], password=cfg['pass'], 
            database = cfg['name'], charset="utf8"
        )
        self.cur = self.con.cursor(cursor=pymysql.cursors.DictCursor)

    def dbcon_sqlite(self, cfg):
        import sqlite3;
        self.con = sqlite3.connect(
            cfg['path'],
            detect_types = sqlite3.PARSE_DECLTYPES
        )
        self.con.row_factory = dict_row # sqlite3.Row
        self.cur = self.con.cursor()

    # -----------------------

    def table(self, tbsql, sql=1):
        pre = self.cfg['tbpre']
        fix = self.cfg['tbfix']
        if sql: # {base_catalog}
            if self.cfg['type']=='sqlite': # sqlite 占位符
                tbsql = tbsql.replace('%s', '?')
            else: # mysql|oracle|postgre 占位符
                tbsql = tbsql.replace('?', '%s')
            return tbsql.replace('{', pre).replace('}', fix)
        else:
            return pre + tbsql + fix

    def get(self, sql, parms=(), re=None): # re = 1,n,None
        cur = self.cur #;print(self.table(sql), parms)
        cur.execute(self.table(sql), parms)
        if not re:
            res = cur.fetchall()
        elif re==1:
            res = cur.fetchone()
        else: 
            res = cur.fetchmany(re)
        return res

    def exe(self, sql, parms=(), mod=None):
        cur = self.cur
        if type(parms) == list :
            res = cur.executemany(sql, parms) 
        else:
            res = cur.execute(self.table(sql), parms)
        self.con.commit()
        return res

    # -----------------------

    def tbdic(self, detail=0):
        cfg = self.cfg
        func = 'tbdic_' + cfg['type']
        if func in dir(self):
            return getattr(self, func)(detail)
        else:
            scut.vdie(f"DB-type `{cfg['type']}` Not Support(@tbdic).") 

    def tbdic_mysql(self, detail=0):
        res = {}; tabs = []
        lists = self.get("SHOW tables")
        for tr in lists:
            for key,value in tr.items():
                tabs.append(value)

        for tab in tabs:
            fields = []
            ctmp = self.get("SHOW full fields FROM `"+tab+"`")
            for tr in ctmp:
                fields.append(tr if detail else tr['Field'])
            res[tab] = fields

        return res

    def tbdic_sqlite(self, detail=0):
        res = {}; tabs = []
        lists = self.get("SELECT name FROM sqlite_master WHERE type='table'")
        tabs = [row[0] for row in lists]

        for tab in tabs:
            fields = []
            ctmp = self.get("pragma table_info('"+tab+"')")
            for fx in ctmp:
                tr = {
                    '_No': fx[0], 'Field': fx[1], 'Type': fx[2],
                    'Null': fx[3], 'Default': fx[4], 'Key': fx[5],
                }
                fields.append(tr if detail else tr['Field'])
            res[tab] = fields # [fx[1] for fx in ctmp]

        return res

    # -----------------------

    def pager(self, sfwhr, page=1, limit=10):
        ctemp = self.get(f"SELECT COUNT(*) AS rcnt {sfwhr}",(),1)
        return pager(ctemp['rcnt'], page, limit)

# === end ===========================

"""

"""
