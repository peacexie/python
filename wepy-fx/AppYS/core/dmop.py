
from . import scut, dbop, clib
#from .. import config


# === func ===========================

def dbKid(tab='xtest_keyid', tpl='max', n=0, xTime=''):
    db = dbop.Db()
    tabf = db.cfg['tbpre']+tab+db.cfg['tbfix']
    tfix = tab[0:5]
    tkey = 'kid'; tno = 'kno';
    if tfix in f",docs_,coms_,advs_,users,":
        tkey = tfix[0:1]+'id'
        tno = tfix[0:1]+'no'
    ktmp = clib.tplKid(tpl, xTime); # 2013-mdh-4689
    if n==0:
        kno = 1; kid = ktmp
    else:
        pdb = ktmp[0:ktmp.rfind('-')]
        sql1 = f"SELECT max({tno}) as {tno} FROM {tabf} WHERE {tkey} LIKE ?"
        mdb = db.get(sql1, (f"{pdb}%"), re=1)
        kno = int(mdb[tno])+1 if mdb[tno] else 1
        kfix = ''+str(kno) if kno<10 else str(kno)
        kid = ktmp[0:len(ktmp)-2] + kfix
    if n:
        kid = kid[0:len(kid)-1] + clib.rndKid(2,'k')
    sql2 = f"SELECT {tkey} FROM {tabf} WHERE {tkey}=?"
    rec = db.get(sql2, (kid,), 1) #;print('a', rec, kid)
    if rec:
        return dbKid(tab, tpl, n+1, xTime)
    else:
        return [kid, kno]
    pass

# model-config
def modCfg(mod):
    cfg = {}
    groups = scut.read('groups')
    if not mod in groups:
        return {}

    cfg['pid'] = pid = groups[mod]['pid'];
    cfg['extab'] = ''
    if pid=='docs':
        tab = 'docs_' + mod; cfg['extab'] = 'dext_' + mod
        fpk = 'did'; odf = 'did DESC'
    elif pid=='users':
        tab = 'users_' + mod;
        fpk = 'uid'; odf = 'atime DESC'
    elif pid=='advs':
        tab = 'advs_' + mod;
        fpk = 'aid'; odf = 'atime DESC'
    elif pid=='coms':
        tab = 'coms_' + mod;
        fpk = 'cid'; odf = 'ctime DESC'
    elif pid=='types':
        tab = 'types_common' if not groups[mod]['etab'] else 'types_'+mod;
        fpk = 'kid'; odf = 'deep, top';

    for k0 in ['mod', 'tab','odf','fpk']:
        cfg[k0] = vars()[k0]
    return cfg

# model-field
def modField():
    pass

# model-mtype
def modType():
    pass

# === Model类 ===========================

""" 数据模型操作(data-model-opration)

    * news = dmop.Model('news') 
      - data['row-1'] = news.get("did='2020-9q-80a9'", 1)
      - data['list-2'] = news.get("vtype='ptxt'", 2) 
      - data['list-page'] = news.get("vtype='ptxt'", '2,2') 
      - data['list-order'] = news.get("", '2', "title DESC") 
      - data['list-count'] = news.get("`show`='all'", 'count') 
      - data['list-def'] = news.get()

    * news2 = dmop.Model('news.join') 
      - data['list-join'] = news2.get("vtype='ptxt'", "6", "title DESC")
      - data['news-cfg'] = news2.cfg

"""

class Model:

    def __init__(self, mod, dbkey='0'):
        self.cfg = {'dbkey':dbkey}
        self._mod(mod)
        self.db = dbop.Db(dbkey)

    # mod = news, news.join  ->  mod, join, pid, tab, odf, fpk
    def _mod(self, mod):
        cfg1 = {}
        if '.' in mod:
            mtmp = mod.split('.')
            #self.cfg['mod'] = mod = mtmp[0]
            cfg1['join'] = 1;
            mod = mtmp[0]
        else:
            #self.cfg['mod'] = mod
            cfg1['join'] = 0;
        
        cfg2 = modCfg(mod)
        if not 'pid' in cfg2:
            scut.vdie(f"Model `{mod}` Not Found.") 
        else:
            self.cfg = {**cfg2, **cfg1} 
        return self.cfg
    
    # $whr: did='2020-9q-80a9', `show`='all', `show`='0', 
    # $lmt: 1, 10, 10,10, count
    # $ord: atime DESC, title ASC
    def get(self, whr='', lmt='10', odf=''):
        self._where(whr)
        self._limit(lmt)
        self._order(odf)

        sfrom = "FROM {" +self.cfg['tab']+ '}'
        where = "WHERE "+self.cfg['whr'] if self.cfg['whr'] else ''

        # data
        if self.cfg['type']=='count':
            res = self.db.get(f"SELECT COUNT(*) AS rcnt {sfrom} {where}",(),1)
            return res['rcnt']
        else:
            sql = f"SELECT * {sfrom} {where} ORDER BY {self.cfg['odf']} LIMIT {self.cfg['lmt']}"
            #print('get:', f"sql=`{sql}`", f"mod=`{mode}`")
            res = self.db.get(sql, (), 0)

        if self.cfg['lmt']=='1':
            return res[0] if res else {}

        # join
        if res and self.cfg['join']:
            res = self._exjoin(res)

        return res

    # whr: `show`='all'
    def _where(self, whr):
        whr = whr if whr else ''
        pid = self.cfg['pid']
        if pid in ['docs','users','coms','advs']:
            if "`show`='all'" in whr:
                whr = whr.replace(" AND `show`='all'",'').replace("`show`='all' AND ",'').replace("`show`='all'",'')
            elif '`show`=' in whr:
                pass
            else:
                whr += " AND (`show`='1')";

        if whr[0:5]==' AND ':
            whr = whr[5:]

        self.cfg['whr'] = whr
        return whr

    # ord: atime-1, atime
    def _order(self, odf=''):
        if odf:
            self.cfg['odf'] = odf

    # lmt: 1, 10, 10,10, count
    def _limit(self, lmt='10'):
        lmt = str(lmt)
        if lmt=='count':
            self.cfg['lmt'] = ''
            self.cfg['type'] = 'count'
        elif ',' in lmt:
            self.cfg['lmt'] = lmt
            self.cfg['type'] = 'page'
        else: # 1, 10
            self.cfg['lmt'] = lmt if lmt.isdigit() else '10'
            self.cfg['type'] = 'list' if lmt!='1' else '1'

    def _exjoin(self, res):
        ids = ''; kid = self.cfg['fpk']
        for iv in res:
            ids += (',' if ids else '') + "'" + iv[kid] + "'";

        sfrom = "FROM {" +self.cfg['extab']+ '}'
        sql = f"SELECT * {sfrom} WHERE {kid} IN({ids})"
        extab = self.db.get(sql, ()); exts = {} # exts
        for itm in extab:
            key = itm[kid]
            exts[key] = itm

        res2 = []
        for itm in res:
            key = itm[kid]
            if key in exts:
                itm2 = {**itm, **exts[key]} 
            else:
                itm2 = itm
            res2.append(itm2)
        return res2

# === end ===========================

"""

"""
