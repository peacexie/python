
import time, json, uuid, hashlib
from . import scut, dbop, dmop, clib, parse
from .. import config

""" 用户会员操作(user-member-opration)

* umode
  - locin (本地调试)       loc_test888
  - idpwd (账号密码)       peace_xie999
  - mobvc (手机短信)       13588882147
  - email (电子邮箱)       xp8888on@163.com
  - qq    (QQ)            80893510 / A7754037047CD87B94ABF2B988889F57
  - wxoacc (微信公众号)    wx4177c709888853c6 . jjcn2cFbxMLaMB8888YbCk
  - wxwork (企业微信)      wx4177c709886853c6 . jjcn2cFbxMLaMB8888YbCk
  - wxmpro (微信小程序)    wx4177c709882853c6 . jjcn2cFbxMLaMB8888YbCk
  - zfbmpro (支付宝小程序) 2021072866887148 . 2088022888225045
  - bdmpro (百度小程序)    24894429 . l214zFqNrEuIEnp6m78888w8yj

wxappid = wx4177c709166053c6
openid  = oyDK8v.jjcn2cFbxMLaMBhKEsYbCk
          o9LBd5.Q-eiYz4kOZbAdMDJQlqUmk
qqxxx   = DE4E30F9EDC99B0EBACD6CB446E921CA

bdappid = 24894429
bdxxx   = hr6jTV7Pcq5xNvqfrCCuN2Jn9I

"""


# === token ===========================

# 心跳函数(维持连接)
def heartBeat(supd=300, ckeep=0): # 5-10min spad
    autok = clib.uparam('autok')
    if not autok or not '.' in autok:
        return {}

    utok = parse.jwt(autok) 
    if 'uid' in utok['djwt'] and utok['stat']['chk']:
        if utok['stat']['sec']<0: # 过期
            res['cdb'] = delAutok(utok['djwt']['sid'])
        elif int(time.time())-utok['djwt']['stime']>supd:
            utok = updAutok(utok, supd) # 认证更新
            if ckeep and 'afresh' in utok: 
                ck1 = {'key':'autok','val':utok['token']}
                scut.gvset('run.cookie', [ck1])
    return utok

# 组装user-token
def fmtAuser(uinfo, exact='ck,db'):
    autok = issAutok(uinfo)
    data = {**uinfo, **autok} # uppt, uacc, umod, [login], djwt, token
    # cookie
    if 'ck' in exact and 'token' in autok: 
        ck1 = {'key':'autok','val':autok['token']}
        #scut.gvset('run.cookie', [ck1])
    # save-db
    if 'db' in exact and 'djwt' in autok:
        redb = insAudb(autok['djwt'])

    return data

# 登出token()
def logoutAutok(utok={}):
    res = {}
    res['auorg'] = autok = clib.uparam('autok')
    if autok and '.' in autok:
        data = parse.jwt(autok)
        djwt = data['djwt'] if 'djwt' in data else {}
        if 'uname' in djwt:
            pass #uinfo = user.logoutBase(djwt['uname']) # delBase
        if 'sid' in djwt: 
            res['cdb'] = delAutok(djwt['sid'])

    ck1 = {'key':'autok','val':''}
    #scut.gvset('run.cookie', [ck1])

    return res

# clear-token
def clearAutok(djwt, supd=300):
    rtime = int(time.time()) - int(supd*2.5) # TODO : , supd
    #sql = "DELETE FROM {active_online} WHERE uname=? AND stime<?"
    #rdb = dbop.Db().exe(sql, (djwt['uname'],rtime))
    rdb = delAutok(djwt['uname'], 'uname', "AND stime<"+str(rtime)+"")
    return rdb

# 更新token(先认证)
def updAutok(utok={}, supd=300):
    sql = "SELECT sid FROM {active_online} WHERE sid=%s"
    rec = dbop.Db().get(sql, [utok['djwt']['sid']], 1)
    if not rec: #db验证?
        rdb = delAutok(utok['djwt']['sid'])
        return {'rdb':rdb}

    stime = int(time.time())
    utok['afresh'] = stime-utok['djwt']['stime']
    utok['djwt']['stime'] = int(time.time())
    utok['token'] = parse.jwt(utok['djwt'])
    utok['fupd'] = insAudb(utok['djwt'])

    rdb = clearAutok(utok['djwt'], supd)
    return utok

# 颁发token
def issAutok(uinfo={}):
    res = {}; djwt = {
        'sid': uuid.uuid1(),
        'stime': int(time.time()),
    }
    if 'umod' in uinfo: # 'login' in uinfo and uinfo['login'] and 
        umod = uinfo['umod']
        arr = 'uid,uname,grade,mname,show'.split(',') # grade=[a]stop???,show
        for tk in arr:
            if tk in umod:
                djwt[tk] = umod[tk]

        res['djwt'] = djwt
        res['token'] = parse.jwt(djwt)

    return res

# delAutok
def delAutok(fval, fk='sid', exstr=''):
    sql = "DELETE FROM {active_online} WHERE "+fk+"=%s" + exstr
    rdb = dbop.Db().exe(sql, (fval,))
    return rdb

# db保存token
def insAudb(djwt):
    cfgu = {'uid':djwt['uid'],'mname':djwt['mname']}
    cfgs = json.dumps(cfgu) # , ensure_ascii=False, cls=JsonEncoder, indent=indent
    row = {'sid':djwt['sid'],'uname':djwt['uname'],'grade':djwt['grade'],'`show`':djwt['show'],
        'cfgs':cfgs,'errno':0,'stime':djwt['stime'],'sip':clib.uip()}
    sql = dbop.dicsql(row) 
    res = dbop.Db().exe("REPLACE INTO {active_online}"+sql['sql'], sql['sqtup'])
    return res


# === func ===========================

# 密码加密-兼容php
def sysPass(uid='', upw='', mod=''):
    if not uid or not upw:
        return ''
    pfix = config.safe['pass']+mod if mod=='adminer' else 'pass'
    return sysEncode(f"{upw}@{uid}", pfix, 24)

def sysEncode(xstr, xkey='other', xlen=16): 
    safe = config.safe
    xstr = parse.md5(xstr)
    xstr = parse.sha1(xstr)
    xstr = parse.sha1(xstr)
    xkey = safe[xkey] if xkey in safe else xkey
    skey = safe['site'] #;return xstr
    ostr = f"{skey}@{xstr}@{xlen}@{xkey}"
    xstr = parse.sha1(ostr) + parse.hh128(ostr) # 72位
    olen = int((len(xstr)-xlen)/2)
    return xstr[olen:olen+xlen]

def defUname(uname, umode=''):
    if uname=='' or umode[0:2]=='wx':
        uname = clib.rndKid(7, '30')
    if umode=='mobvc':
        uname = '_tel_' + uname
    elif umode=='email':
        uname = '_em_' + uname.replace('@','_').replace('-','_').replace('.','_')
    elif umode[0:2]=='wx':
        uname = '_wx_' + uname.replace('-','_')
    elif umode[0:3]=='zfb':
        uname = '_zfb_' + uname
    elif umode[0:2]=='bd':
        uname = '_bd_' + uname
    else:
        pass #uname = '' + umode
    return uname

def getUname(uname='', mod='', no=0):
    if not uname:
        uname = mod[0:1] + clib.tplKid('0').replace('-','') 
    else:
        uname = uname.replace('@','_').replace('.','_').replace('-','_')
    if len(uname)>20:
        uname = uname[0:12] +'_'+ uname[-4:] # a-17
    elif no and len(uname)>15:
        uname = uname[0:15] + clib.rndKid(4,'24') # b-19
    elif no:
        uname += clib.rndKid(4,'24')
    
    sql = "SELECT uname FROM {users_uacc} WHERE uname=?"
    rec = hasUname(uname) #dbop.Db().get(sql, (uname,), 1) #;print('a:',uname, rec)
    if rec:
        if no>10:
            scut.vdie(f"UserName `{uname}` Has Exists.") 
        return getUname(uname, mod, no+1)
    return uname

def getUid(uid='', no=0):
    if not uid:
        tmp = dmop.dbKid('users_uacc')
        uid = tmp[0]; uno = tmp[1]
    else:
        uid = uid[0:9] + clib.rndKid(3); uno = 1

    sql = "SELECT uid FROM {users_uacc} WHERE uid=?"
    rec = dbop.Db().get(sql, (uid,), 1) #;print('a:',uid, rec)
    if rec:
        if no>10:
            scut.vdie(f"UserName `{uname}` Has Exists.") 
        return getUid(uid, no+1)
    return {'uid':uid, 'uno':uno}

def hasUname(uname):
    sql = "SELECT uname FROM {users_uacc} WHERE uname=?"
    rec = dbop.Db().get(sql, (uname,), 1)
    return 1 if rec else 0

# === Ubase ===========================

class Ubase: # (dmop.mod)

    def __init__(self, mod='', dbkey='0'):
        self.mod = mod
        self.db = dbop.Db(dbkey)

    # 注册(增加)一个用户
    def reg(self, uname='', upass='', mex={}):
        mod = self.mod; rex = {}
        row = getUid()
        row['uname'] = getUname(uname, mod);
        row['upass'] = sysPass(uname,upass,mod) if len(upass)>5 else '-'
        row['umods'] = mod

        rex['uid'] = row['uid']
        rex['uname'] = row['uname']
        tab = ['mname', 'grade', 'mtel', 'memail']
        for key in tab:
            if key in mex:
                rex[key] = mex[key]
            else:
                rex[key] = '-'

        r2x = {'row':row, 'rex':rex}; 
        sql = dbop.dicsql(row) 
        reacc = dbop.Db().exe("INSERT INTO {users_uacc}"+sql['sql'], sql['sqtup'])
        sql = dbop.dicsql(rex) 
        remod = dbop.Db().exe("INSERT IGNORE INTO {users_"+mod+"}"+sql['sql'], sql['sqtup'])
        # (xdic, xmode='a/e/0', xfilt={}) 
        
        res = {**{'reacc':reacc, 'remod':remod, 'sql':sql}, **r2x}
        return res

    # 绑定(自动增加)一个用户, 请先判断是否uname存在
    def bind(self, apuid='', appid='', umode='wxwork'):
        uname = defUname(apuid, umode);
        reg = self.reg(uname, '', {});
        row = {'uname':reg['row']['uname'], 'pptmod':umode, 'pptapp':appid, 'pptuid':apuid}
        sql = dbop.dicsql(row)
        uppt = dbop.Db().exe("INSERT INTO {users_uppt}"+sql['sql'], sql['sqtup'])
        return {**{'uppt':uppt}, **reg}

    # 查询一个用户
    def get(self, uname='', appid='', umode='idpwd', uex='acc,mod'):
        res = {} 
        uval = uname; ukey = 'uname'
        if umode!='idpwd':
            sql = "SELECT * FROM {users_uppt} WHERE pptapp=%s AND pptuid=%s"
            res['uppt'] = uppt = dbop.Db().get(sql, [appid,uname], 1)
            uval = uppt['uname'] if uppt else '~'
        else:
            res['uppt'] = uppt = 1
        if uppt and 'acc' in uex:
            res['uacc'] = self.getUacc(uval, ukey)
        if uppt and 'mod' in uex:
            res['umod'] = self.getUmod(uval, ukey)
        return res

    def getUacc(self, uval='', ukey='uname'):
        sql = "SELECT * FROM {users_uacc} WHERE "+ukey+"=%s"
        row = dbop.Db().get(sql, [uval], 1)
        if row and self.mod!= row['umods']:
            self.mod = row['umods']
        return row

    def getUmod(self, uval='', ukey='uname'):
        mod = self.mod
        if not mod:
            return {}
        sql = "SELECT * FROM {users_"+mod+"} WHERE "+ukey+"=%s"
        return dbop.Db().get(sql, [uval], 1)

    def login(self, uname='', upass='', uex='acc,mod'):
        uinfo = self.get(uname, uex=uex)
        mod = self.mod
        encpw = sysPass(uname, upass, mod)
        if uinfo and uinfo['uacc']:
            uinfo['login'] = True if encpw==uinfo['uacc']['upass'] else False
            del uinfo['uacc']['upass']
        else:
            uinfo['login'] = False
        return uinfo

    # logout
    def logoutBase(self, uname=''):
        return {}
        pass # cookie, active_online, token

    # del
    def delBase(self):
        pass # active_online, uacc, umod, users_uppt

# === Admin/Member ===========================

class Admin(Ubase):

    def _abc1(self):
        pass

class Member(Ubase):

    def _abc2(self):
        pass

# === Perm ===========================

# 权限
class Perm:

    # issup
    def issup(self):
        pass

    # check
    def check(self, need=''):
        pass

    # ckey # bc960a2c.33e1af41739711ec8816d017c295a9cb
    def ckey(self, sk='namespace'): 
        ckey = str(uuid.uuid1())
        stamp = str(time.time())
        skey = f"{sk}@{ckey}@{stamp}"
        senc = hashlib.md5(skey.encode()).hexdigest()
        return {'ckey':ckey, 'stamp':stamp, 'skey':skey, 'senc':senc}

"""

imcat : 81ffd708.2021-79-mjrn-g8qv46cuc-64067a679
uuid1 : 8f98480f-7393-11ec-9ae0-d017c295a9cb
python  bc960a2c.33e1af41739711ec8816d017c295a9cb

f1c2be1a-8e12-11ec-9876-d017c295a9cb~1644897255.8269978~31b0f193111d02c079ca45eb50153394
123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-

// mod,uname,upass; mname,mtel,memail; company,uid,grade,check
static function addUser($mod,$uname,$upass,$mname='',$mtel='',$memail='',$excfg=array()){ 
    $arr = array('uname','mname','mtel','memail'); foreach($arr as $k){ $$k = basStr::filTitle($$k); }
    
    return $re;
}

"""
