
import os, sys, time, re, math, json
from . import fdir, parse, dbop, dmop, scut
from .. import config

""" 调试相关(x(no)-debug)
    -
"""

def log(data, pre=''):
    rv = scut.gvget(0)
    path = fdir.fpbase('ins')
    file = time.strftime("%Y-%m%d", time.localtime()) # .%H%M%S
    res = {
        # get, post
        '_time': time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()), 
        'run': rv['run'], 'mkvs': rv['mkvs'],
        'data': data
    }
    #jsonStr = "\n\n" + str(res) + "\n"
    jsonStr = "\n\n" + parse.json_dump(res) + "\n" # 
    fpre = pre+'-' if pre else ''
    fdir.put(f"{path}/log/{fpre}{file}.log", jsonStr)

def trace(flag=''):
    exinfo = sys.exc_info()
    html = ''
    if exinfo[0]:
        for item in exinfo:
            html += "<li>"+str(item).replace('<','&lt;')+"</li>"
    html = html.replace(str(fdir.fpbase('app')),'{proj}/')
    return html 

# debug-info (运行信息) // Used:0.0(ms); root/error.htm; Time:2021-12-12 18:35:55
def irun(tp=''):
    run = scut.gvget('run')
    if not tp:
        tp = 'Used:{run}; {tpl}.htm; Time:{now}'
    if 'stamp' not in run:
        return tp
    used = time.time() - run['stamp']
    if used>1:
        used = str(used) + '(s)'
    else:
        used = str(round(used*1000,3)) + '(ms)'
    now = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    tpl = run['tpname'] 
    html = tp.replace('{run}',used).replace('{tpl}',tpl).replace('{now}',str(now))
    return html 

# 替换路径 
def redir(path):
    tab = {
        'app': fdir.fpbase('app'),
        'python': os.__file__
    }
    for (xkey,xdir) in tab.items():
        path = path.replace(os.path.dirname(xdir),'{'+xkey+'}')
    return path.replace('\\','/')
