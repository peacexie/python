
import os, sys, time
from . import fdir, scut

""" 过滤器(filter:(格式化显示)vfmt=view-format): 
    `filter`为python系统函数, 改用`vfmt`为包名
"""

# === demo ==========================

def _demo_filter(v1=1, v2=2):
    return v1 * v2

# === funcs ==========================

# debug-info (测试方法) 
def irun(tp=''):
    return scut.irun(tp)

# === end ==========================
