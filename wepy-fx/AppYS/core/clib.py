
import os, sys, time, json, random, re, urllib.parse
#from urllib import request as ureq
from flask import request, session
from ..core import parse, scut #, mkvop, dbop, dmop, umop, xbug, urlpy, clib
from .. import config


""" common-lib(常用类库): 
    Request, String, Keyid, ...
"""

# === func Request ============

def uip(mode='-'):
    ip = request.headers.get('X-Forwarded-For')
    if not ip:
        try:
            ip = request.remote_addr
        except Exception as e:
            ip = '-'
    return ip

def filstr(data, mode='-'): # id, title, html
    if not data:
        return data
    if type(data)==str:
        if mode=='id': # [^\\u4e00-\\u9fa5^a-z^A-Z^0-9]
            reg = re.compile(r'[^\w\.\-\@]')
        elif mode=='html':
            reg = re.compile(r'<\s*script[^>]*>[^<]*<\s*/\s*script\s*>',re.I) #去掉SCRIPT
        else: # title,else
            reg = re.compile(r'[\<\>\'\"\\]')
        data = reg.sub('', data)
    elif type(data)==dict:
        for tk in data:
            data[tk] = filstr(data[tk], mode)
    else:
        pass
    return data

# 获取post值
def upost(key='', vdef='', fmode=''):
    if request.method == "POST":
        if request.content_type.startswith('multipart/form-data'):
            post = request.form
        elif request.content_type.startswith('application/json'):            
            post = request.json
        else:
            post = request.values
    else:
        post = {}
    post = filstr(post, fmode)
    
    if not key:
        return post
    elif key in post:
        return post[key]
    else:
        return vdef

# header, cookie, session, get
def uparam(key='', vdef='', fmode=''):
    val = request.headers.get(key, '')
    if not val:
        val = request.cookies.get(config.ucfg['ckpre']+key, '')
        val = urllib.parse.unquote(val)
    if not val:
        pex = scut.gvget('run.pex')
        val = pex[key] if key in pex else vdef

    val = val if val else ''
    val = filstr(val, fmode)
    return val


# 检查来源地址(域名)
def chkref():
    ref = request.referrer # http://127.0.0.1:8083/root/uio
    root = request.url_root # http://127.0.0.1:8083/
    if (not ref) or (not root in str(ref)):
        scut.vdie('Error-Referrer `'+str(ref)+'` .')
    return ref

# 获取token加密表单域的值
def tkval(post, tkid, tab):
    res = {}; vtb = {}
    res['post'] = post

    if not tkid in post:
        return scut.vdie('Error `Null-Token` .')
    toks = parse.jwt(post[tkid])
    stat = toks['stat'] if 'stat' in toks else {}
    if not 'chk' in stat or stat['sec']<0 or not stat['chk']:
        return scut.vdie('Error-Token `'+post[tkid]+'` .')
    
    res['djwt'] = djwt = toks['djwt']
    arr = tab.split(',')
    for tk in arr: 
        if tk in djwt and djwt[tk] in post:
            vtb[tk] = post[djwt[tk]].strip()
        else:
            vtb[tk] = ''

    res['vtb'] = vtb
    return res

# === func String ============

# 原文链接：https://blog.csdn.net/xfjpeter/article/details/85092576
def idCard(idno, rch=0):
    if not rch and not len(idno)==18:
        return 0
    w = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
    y = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2']; 
    sm = 0;
    for i in range(0,17): # (1,a+1,1):
        sm += int(idno[i:i+1]) * w[i]
    md = sm % 11 # 取余
    if rch:
        return y[md]
    else:
        return idno[17:18] == y[md]

def cutStr(mstr, p1=1024, p2=1024, omit='...'):
    if len(mstr)>p1+p2+len(omit):
        mstr = mstr[0:p1] + omit + mstr[-p2:]
    return mstr

# === func Keyid ============ 

# ctab(常量字符表)
def keyTab(key='k', rnd=0):
    t10 = '0123456789'
    t22 = 'abcdefghjkmnpqrstuvwxy' # -iloz(易混淆字符)
    t26 = 'abcdefghijklmnopqrstuvwxyz'
    # 场景
    if key=='10' or key=='0': # 数字
        res = t10
    elif key=='22':
        res = t22
    elif key=='26' or key=='a':
        res = t26
    elif key=='16' or key=='h':
        res = t10 + 'abcdef'
    elif key=='36' or key=='f':
        res = t10 + t26
    elif key=='32' or key=='k':
        res = t10 + t22
    elif key=='30': # (t22) - 0e(0字形,e读音易混淆)
        res = t10.replace('0','') + t22.replace('e','')
    elif key=='24': # (t22) - 012emnr(去除字形读音易混淆者)
        res = t10.replace('012','') + t22.replace('mn','').replace('r','')
    else:
        res = t10 + t22
    # 打乱
    if rnd:
        slist = list(res) # 字符串转换成列表
        random.shuffle(slist) # 调用random模块的shuffle函数打乱列表
        res = ''.join(slist) # 将列表转字符串
    return res

# 任意进制 / fmtBase32  
def fmtJinzhi(num, b=32): # 
    tab = keyTab('k')
    return ((num == 0) and "0") or (fmtJinzhi(num // b, b).lstrip("0") + tab[num % b])

# 摸版ID / kidTemp, tpl=`y4-md2-h1ms2|`
def tplKid(tpl='md2', xtime=''):
    if str(xtime).isdigit():
        ndtm = time.localtime(int(xtime))
    elif xtime:
        ndtm = time.strptime(xtime, "%Y-%m-%d %H:%M:%S")
    else:
        ndtm = time.localtime()
    # parts 
    ny = ndtm[0]; nm = ndtm[1]; nd = ndtm[2]; ktab = keyTab('k') 
    nh = ndtm[3]; ni = ndtm[4]; ns = ndtm[5]; nsec = 60*ni+ns; r3 = rndKid(3)
    nsd4 = int(nsec/4); nsi4 = nsec%4; nsj4 = random.randint(nsi4*8, (nsi4+1)*8-1)
    y4 = str(ny); m1 = ktab[nm:nm+1]; d1 = ktab[nd:nd+1]; d2 = ('0' if nd<=0 else '') + str(nd)
    # tab
    tab = {
        'y4':y4, 'y2':y4[2:4], 'x3':fmtJinzhi(ny), 'x2':fmtJinzhi(ny-1970),
        'm1':m1, 'd1':d1, 'md2':m1+d1, 'md3':m1+d2, 'h1':ktab[nh:nh+1],
        'ms2':fmtJinzhi(nsd4), 's1':fmtJinzhi(nsj4), 
        'r3':r3, 'r2':r3[0:2], 'r1':r3[0:1], 'd2':d2, 
    }
    tps = {
        'md2': 'y4-md2-h1ms2s1',
        'md3': 'y4-md3-h1ms2s1',
        'mdh': 'y4-md2h1-ms2s1r1',
        '5.5': 'y2md2h1-ms2s1r2',
        'x35': 'x3md2-h1ms2r2',
        'x22': 'x2md2h1-ms2',
    }
    # return
    if tpl and tpl in tps:
        tpl = tps[tpl]
    elif tpl:
        tpl = tps['md2']
    if '.' in tpl or '-' in tpl:
        for k in tab.keys() :
            tpl = tpl.replace(k, tab[k])

    return tab if not tpl else tpl

# 随机子串 / kidRand
def rndKid(sn=16, tb='k'):
    res = ''
    while len(res)<sn:
        res += keyTab(tb, rnd=1)
    return res[0:sn]

# kidAuto / 0bb38d5cbadd(之前asp系统使用) -> 20222ahwvf8h
def aspKid(sn=24, xtime=''):
    res = tplKid('mdh').replace('-','')
    if len(res)<sn:
        res += rndKid(16)
    return res[0:sn]

# === class ============ 

class String: # ()

    def __init__(self, xxx=''):
        pass
        self.xxx = xxx

class Keyid: # ()

    def __init__(self, xxx=''):
        pass
        self.xxx = xxx

# === end xxx ============ 

"""


"""
