
import os, sys, time
from multiprocessing import Pool #,Process
from AppYS.core import mkvop, scut, parse
from AppYS import create_app # config, 

class Pools:

    def __init__(self, tab, pre=''):
        self.tab = tab
        self.pre = pre
        self.dores = {} 
        self.cbres = {}
        self.info = {'start':time.time(), 'cnt':len(self.tab)}

    # 按进程运行
    def iapp(self):
        scut.gvinit()
        scut.gvset('run.mcli', 2)
        return create_app({})

    # 
    def __del__(self):
        pass

    # 执行子进程
    def domkv(self, no, mks=''):
        start = time.time() # todo-start
        rec = {'no':no, 'pid':os.getpid()}
        stab = [mks] if isinstance(mks,str) else mks
        app = self.iapp() 
        # mkv-run
        skey = []; resx = []
        for i in range(len(stab)):
            skid = stab[i]
            if not '/' in skid:
                skid = self.pre + skid
            with app.app_context():
                re1 = mkvop.vdef(skid) 
                if len(stab)==1:
                    resx = re1
                else:
                    resx.append(re1)
                skey.append(skid)
        # return
        rec['run'] = f"{(time.time()-start):.3f}(s)"
        rec['ktab'] = skey
        return (rec, resx)

    # 执行回调
    def cbdo(self, resx):
        (res1,res2) = resx
        no = res1['no']
        self.cbres[no] = res1
        print(no, res1)
        self.dores[no] = res2

    # 错误回调
    def cbex(self, msg):
        rec = {'pid':os.getpid(), 'msg':msg}
        print('   !!!   err_cb:', rec)

    # 主运行入口
    def run(self, cnt=4):
        pool = Pool(cnt) #创建一个`cnt`个进程的进程池
        for i in range(len(self.tab)):
            mks = self.tab[i] #;print(kid)
            pool.apply_async(func=self.domkv, args=(i,mks), callback=self.cbdo, error_callback=self.cbex)
        # end
        pool.close()
        pool.join()
        return self

    # 显示结果
    def vres(self, svlog=0):
        self.info['run'] = time.time() - self.info['start']
        self.info['time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        del self.info['start']
        print("\n", self.info, "\n")
        dores = parse.json_dump(self.dores, 4)
        dores = dores.replace("\\n","\n")
        print(dores)

"""

"""