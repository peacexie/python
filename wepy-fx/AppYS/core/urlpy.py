
import os, sys, time, json, re, requests, urllib.parse
from urllib import request
from urllib.parse import urljoin
from flask import request
from functools import wraps
from . import scut, fdir, xbug, parse
from .. import config

""" url抓取(py=爬)相关函数: 
    -
"""

# 获取远程数据
def get(url, data={}, mode='get', refmt=''):
    # fetch 
    head = py_head()
    redef = '' # 默认返回
    if mode=='json':
        head = {'Content-Type':'application/json'}
        res = requests.post(url, json=data, headers=head)
        redef = 'json'
    elif mode=='form':
        head = {'Content-Type':'application/x-www-form-urlencoded'}
        res = requests.post(url, data=data, headers=head)
    elif mode=='files':
        res = requests.post(url, files=data, headers=head)
    else:
        res = requests.get(url, params=data, headers=head)
    # return
    refmt = refmt if refmt else redef
    if refmt=='org':
        return res
    elif refmt=='json':
        return res.json()
    else:
        return tx_fmt(res)

# 返回文本格式化
def tx_fmt(res):
    if res.encoding == 'ISO-8859-1':
        encodes = requests.utils.get_encodings_from_content(res.text)
        if encodes:
            encode = encodes[0]
        else:
            encode = res.apparent_encoding
        html = res.content.decode(encode, 'replace') #如果设置为replace，则会用?取代非法字符；
    else:
        html = res.text
    
    if html and html[0:2]=='{"' and html[-1:]=='}':
        return json.loads(html)
    else:
        return html

# 默认header
def py_head(head={}):
    agent = {"User-Agent": "Mozilla/5.0 (Window 10) Chrome/98.0"}
    return dict(agent,**head) if head else agent

# 从url保存一个文件
def svurl(url, sdir, file='', path='../_cache'):
    if url.find('://')<0:
        return ''
    data = request.urlopen(url).read()
    if len(data)==0:
        return ''
    file = file if file else files.autnm(url)
    fp = path + '/' + sdir + '/' + file
    with open(fp, "wb") as fo:
        fo.write(data) #写文件用bytes而不是str
    return file

def fxurl(url, base=''):
    if not '://' in url:
        url = urljoin(base, url)
    return url

# --- 以下函数,尽量使用PyQuery代替,这里出现只是练习的意义 --- 

def block(html, tag, end=''):
    p1 = html.find(tag)
    if p1<0:
        return ''
    slen = len(html)
    html = html[p1:slen]
    p1 = html.find(end)
    if p1<0 or end=='':
        return html
    p1 += len(end)
    html = html[0:p1]
    return html
