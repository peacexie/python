
import os, sys, time, json, re, urllib.parse
from flask import current_app, render_template, request, make_response
from functools import wraps
from . import scut, fdir, xbug, parse, clib
from .. import config

""" mkv-opration(mkv操作): 
    -
"""

# ====================================== 

# 检查 pathinfo
def chk_path(mcli=None):
    if not mcli:
        fpath = urllib.parse.unquote(request.full_path)
        if fpath[-1:]=='?':
            fpath = fpath[:-1]
        mode = 'web' # not-cli
    else:
        scut.gvset('run.mcli', mcli)
        argv = sys.argv
        fpath = argv[1] if len(argv)>1 else ''
        if fpath[0:1]=='/':
            fpath = fpath[1:]
        mode = argv[2] if len(argv)>2 else 'api'

    if fpath and re.search(r'[\<\>\'\"\\]', fpath):
        scut.vdie('Error-Url(1), Url `'+fpath+'` Error.')

    tmp1 = fpath.replace('?','&').split('&')
    path = ''; pex = {} # path
    for tk in tmp1:
        if not path:
            path = tk
        elif tk:
            tmp2 = tk.split("=")
            pex[tmp2[0]] = '' if len(tmp2)==1 else tmp2[1]

    scut.gvset('run.pex', pex)
    res = {'path':path, 'pex':pex, 'cli':mode}
    return res

# 默认视图 path, mkv
def vdef(path): # 
    res0 = re.findall(r'^([\w]{1,}[\w\-\.]*)(/([\w]{1,}[\w\-\.]*)?)?$', path)
    if len(res0)==0:
        scut.vdie('Error-Url(2), Path `'+path+'` Error.')

    res = res0[0] #;print(res)
    if res[1]:
        mkv = res[2]
        part = res[0]
    else:
        scut.vdie('Error-Url(2B), Path `'+path+'` Error.')
    
    if part in config.vmode['vapi']:
        scut.gvset('run.vret', 'api')
        return api(part, mkv)
    else:
        return tpl(part, mkv)

# 模板视图
def tpl(part='root', mkv='home'):
    info = {}
    mkvs = mkv_info(part, mkv)
    acts = mkv_act(mkvs)
    if type(acts).__name__=='Response':
        return acts
    
    info['tpname'] = now_tpl(mkvs)
    if acts['act']:
        info = dict(info, **acts['info'])
        data = acts['data']
        if '_return' in info and info['_return']=='json': # 直接返回json
            return vjson(info, data)
        if '_retip' in info: # 提示信息: 1-OK/0-Bad
            info['status'] = 200 if info['_retip'] else 404
            info['tpname'] = 'root/' + ('info' if info['_retip'] else 'error')
        if '_newtp' in info: # 设置新模板
            info['tpname'] = fdir.fpfix(info['_newtp'], mkvs)
    else:
        data = {}
    
    # 检查模板是否存在
    fp = fdir.fpbase('tpl') +'/'+ info['tpname']
    fpext = fp if '.' in info['tpname'] else fp+'.htm'
    print(fpext)
    if not os.path.exists(fpext): 
        tpname = info['tpname'] + ('' if '.' in fp else '.htm')
        info = tip_info('Error-Template', 'Template `/'+tpname+'` NotFound.')
        return vjson(info, data)
    else:
        return vhtml(info, data)

# api 视图
def api(part, mkv=''):
    info = {}
    mkvs = mkv_info(part, mkv)
    acts = mkv_act(mkvs) 
    if type(acts).__name__=='Response':
        return acts

    if acts['act']:
        info = dict(info, **acts['info'])
        data = acts['data']
        if '_newtp' in info:
            info['tpname'] = info['_newtp'] 
            return vhtml(info, data)
    else:
        data = {}
        info['errno'] = 'Error-API'
        info['errmsg'] = f"API `{acts['acfunc']}` NotFound."
    
    # 默认无错误信息
    if not 'errno' in info:
        info['errno'] = 0
    if not 'errmsg' in info:
        info['errmsg'] = 'ok'
    return vjson(info, data)

# ====================================== 

# exception 信息
def vexc():
    extype, exmsg, track = sys.exc_info()
    stbak = str(exmsg)[0:3]
    if stbak>='400' and stbak<='650': # [400-600] 1xx,2xx,3xx 
        status = stbak
        extip = str(extype).replace("<class '",'').replace("'>",'')
    else:
        status = '500' 
        extip = 'Unknow Error ('+status+')'

    info = tip_info('Http-'+status, str(exmsg), int(status))
    info['title'] = extip.replace("werkzeug.exceptions.",f'Http {status} ')
    
    data = {'extype':str(extype), 'exmsg':str(exmsg)}
    import traceback
    tracks = reversed(traceback.extract_tb(track)); no = 0
    for item in tracks:
        path = xbug.redir(item[0])
        no += 1
        data['track'+str(no)] = path +' @ '+ str(item[1]) +':'+ item[2] + '()'

    if scut.gvget('run.vret')=='api':
        return vjson(info, data)
    else:
        return vhtml(info, data)

# ====================================== 

# @跨域装饰器:allow_cross_domain
def cross(fun):
    @wraps(fun)
    def wrap_fun(*args, **kwargs):
        return reshd(res, 'json')
    return wrap_fun

# resp-header 跨域头 
def mkresp(data, vtype='json', status=0):
    if scut.gvget('run.mcli'):
        return vcli(data, vtype, status)

    resp = make_response(data) 
    if status:
        resp.status = status # 设置状态码
    # vtype
    utab = {
        'json': 'application/json',
        'xml': 'application/xml',
        'jpeg': 'image/jpeg', # vcode
    }
    tpstr = utab[vtype] if vtype in utab else 'text/html'
    resp.headers['Content-Type'] = tpstr
    # Access
    if vtype=='json':
        resp.headers['Access-Control-Allow-Origin'] = '*'
        resp.headers['Access-Control-Allow-Methods'] = 'PUT,GET,POST,DELETE'
        resp.headers['Access-Control-Allow-Headers'] = "Referer,Accept,Origin,User-Agent"
    # cookies 
    cks = scut.gvget('run.cookie')
    if cks:
        for ck1 in cks:
            exp = ck1['exp'] if 'exp' in ck1 else '3600'
            resp.set_cookie(config.ucfg['ckpre']+ck1['key'], ck1['val'], max_age=3600) 

    return resp

# (Cli)text输出
def vcli(data, vtype, status):
    mkvs = scut.ucfg('mkvs')
    ifor = f"`/{mkvs['part']}/{mkvs['mkv']}` [{vtype}, {status}]"
    omit = "\n\n              …… …… …… …… ……               \n\n"
    data = clib.cutStr(data, 480, 480, omit=omit)
    if scut.gvget('run.mcli')==2: # 2:
        return data #{'ifor':ifor, 'data':data, 'irun':irun}
    else:
        print('[result] for', ifor, ":\n")
        print(data)
        if config.cmd['DEBUG']:
            irun = xbug.irun('{run} '+f"{len(data)/1024:,.3f}"+'(KB) {now}')
            print("\n[debug]", irun)

# (API)json输出
def vjson(info={}, data={}):
    status = info['status'] if 'status' in info else 200
    if type(data)==str: # 直接返回状态码,如`success`
        sret = data
    else:
        if scut.gvget('run.mcli')==2:
            return data
        resp = {'info':info, 'data':data, 'status':status}
        sret = parse.json_dump(resp, 4) 
    return mkresp(sret, 'json', status)

# (Template)html(xml)输出
def vhtml(info={}, data={}):
    status = info['status'] if 'status' in info else 200
    tpname = info['tpname'] if 'tpname' in info else 'root/info'
    scut.gvset('run.tpname', tpname);
    fpext = tpname if '.' in tpname else tpname+'.htm'
    sret = render_template(fpext, info=info, data=data, scut=scut)
    return mkresp(sret, 'xml' if '.xml' in fpext else 'html', status)

# ====================================== 

# 组提示(错误)信息
def tip_info(no, msg, status=404):
    info = {}
    info['errno'] = no
    info['errmsg'] = msg
    info['tpname'] = 'root/' + ('info' if status==200 else 'error')
    info['status'] = status
    return info

# 获取当前模板
def now_tpl(mkvs):
    fp1 = fdir.fpbase('tpl') +'/'+ mkvs['tpone']
    #fpd = fdir.fpbase('tpl') +'/'+ mkvs['tpdef']
    fpext = fp1 if '.' in fp1 else fp1+'.htm'
    if mkvs['tpone'] and os.path.exists(fpext):
        tpname = mkvs['tpone']
    else: #elif os.path.exists(fpd+'.htm'):
        tpname = mkvs['tpdef']
    return tpname

# 获取 mkv-act
def mkv_act(mkvs={}):
    file = mkvs['part']+'/'+mkvs['mod']+'.py'
    pyp = 'AppYS.'+file.replace('.py','').replace('/','.')
    fp = fdir.fpbase('app') +'/'+ file
    func = (mkvs['key'] if mkvs['key'] else '_home') + 'Act'

    if not os.path.exists(fp):
        return {'act':'', 'data':{}, 'info':{}, 'acfunc':pyp+'.'+func}

    obj = __import__(pyp, fromlist='..')
    act = ''
    arr = [mkvs['key'], '_'+mkvs['type']] #, '_list'
    for itm in arr:
        act = getattr(obj, itm+'Act', '')
        if act:
            break

    res = act(mkvs) if act else {}
    if not res:
        return {'act':act, 'data':{}, 'info':{}, 'acfunc':pyp+'.'+func}
    if type(res).__name__=='Response':
        return res
    
    info = data = {} # deel `_xxx`
    if '_data' in res:
        info = res
        data = res['_data'] 
        del info['_data']
    elif '_info' in res:
        data = res
        info = res['_info'] 
        del data['_info']
    elif act:
        data = res 
        tab = ['_return', '_retip', '_newtp', 'errno', 'errmsg']
        for key in tab:
            if key in data:
                info[key] = data[key]
                del data[key];
                
    return {'act':act, 'data':data, 'info':info, 'acfunc':pyp+'.'+func}

# 分析 mkv 信息
def mkv_info(part, mkv='home'):
    # default
    if len(part)==0:      # </root>/info
        part = 'root'
    mkvOrg = mkv
    # mode
    ufix = config.vmode['urlfix'] # .htm, .html 伪静态
    if ufix and mkv[-len(ufix):]==ufix:
        mkv = mkv[0:-len(ufix):]
    mode = 'home'
    if (not mkv) or len(mkv)==0:  # /front/
        mkv = 'home'
    elif mkv.find('.')>0: # /front/news.nid
        mode = 'view'
    elif mkv.find('-')>0: # /front/news-cid
        mode = 'list'
    else:                 # /front/news
        pass # mkv = mkv
    # spilt-mkv
    if mode=='home':
        mod = mkv; key = ''; view = '';
    else:
        tmp = mkv.split('.') if mkv.find('.')>0 else mkv.split('-')
        mod = tmp[0]; key = tmp[1]; view = tmp[2] if len(tmp)>=3 else ''
    # tpname
    tpdir = part +'/'+ mod +'/'; tpone = '';
    if mode=='home':
        tpfile = '_'+ mode
    elif mode=='list':
        tpfile =   '_'+ mode + ('-'+view if len(view)>0 else '')
        tpone = tpdir + key + ('-'+view if len(view)>0 else '')
    else: # view
        tpfile = '_'+ mode + ('.'+view if len(view)>0 else '')
    # return-mkvs
    mkvs = {
        'mkv':mkv, 'mod':mod, 'key':key, 'view':view, 'mkvOrg':mkvOrg,
        'part':part, 'type':mode, 'tpdef':tpdir+tpfile, 'tpone':tpone,
    }
    # check
    scut.gvset('mkvs', mkvs);
    reg = r'(^[\.\-])|([\.\-]$)' # 不允许`.-`开头结尾
    if re.search(reg, key) or re.search(reg, view):
        scut.vdie(f'Error-Url(3), Path `{mkv}` Error.')
    if mkvs['key'] in ['_home','_list','_view']:
        scut.vdie('Error-Url(4): Key `'+mkvs['key']+'` Error.')

    return mkvs

# ====================================== 

