
import os, sys, string, time, random, re, json, hashlib, base64, markdown
from . import fdir, scut
from .. import config

""" 解析转化(parse): 
    -


#reload(sys) 
#sys.setdefaultencoding('utf8') 
#import string
  

"""

# === basic ==========================

def md5(s0):
    hobj = hashlib.md5()
    hobj.update(bytes(s0, encoding='utf-8'))
    shash = hobj.hexdigest()
    return shash

def sha1(s0):
    hobj = hashlib.sha1()
    hobj.update(bytes(s0, encoding='utf-8'))
    shash = hobj.hexdigest()
    return shash

def hh128(s0):
    from ..extra import encs
    digest = encs.ripemd128(bytes(s0, encoding='utf-8')) 
    import binascii
    return binascii.b2a_hex(digest).decode('utf-8')

# 从`udic`中取出`k1.k2`对应的值
def dkget(udic, uk):
    vdic = udic
    if '.' in uk:
        arr = uk.split(".")
        for i in range(len(arr)):
            key = arr[i]
            if key in vdic:
                vdic = vdic[key]
            else:
                return ''
        return vdic
    else:
        return vdic[uk] if uk in vdic else ''

# 设置 `udic` 下 > `k1.k2` 的值
def dkset(udic, uk, uv):
    vdic = udic
    if '.' in uk:
        arr = uk.split(".") # TODO:多层次？
        if not arr[0] in vdic:
            vdic[arr[0]] = {}
        vdic[arr[0]][arr[1]] = uv
    else:
        vdic[uk] = uv
    
    return vdic

# base64 : dec:1-decode,0-encode # `+/=` -> `-_~`
def bs64(data, dec=0, cset='utf8'):
    if dec: # 解码先还原
        data = data.replace('-','+').replace('_','/').replace('~','=') #.replace('%7E','=')
    data = data.encode(cset)
    try:
        data = base64.decodebytes(data) if dec else base64.encodebytes(data) # b'xxx' 二进制
        data = data.decode(cset)
    except Exception as exmsg:
        data = ''
    if not dec: # 编码后变形
        data = data.replace('+','-').replace('/','_').replace('=','~').replace("\n",'') # ?
    return data.strip()

# 得到命令行参数, no=1,2,3
# /part/mod-act?p1=v1&p2=v2
def cmdp(no, d=''):
    args = sys.argv
    val = args[no] if len(args)>no else d
    return val

# === jwt ==========================

def jwt(data, utime=-1):
    secret = config.safe['api']
    stamp = int(time.time())
    if isinstance(data,str): # === 解码
        utime = utime if utime>0 else 5.5*3600 # 默认更新时间(少于utime更新)
        tab = data.split('.')
        if len(tab)!=3 or not re.search(r'^\d+$', tab[0]):
            scut.vdie('Error-Data:`'+data+'`.')
        jstr = bs64(tab[1],1)
        try:
            djwt = json.loads(jstr)
            ermsg = ''
        except Exception as exmsg:
            ermsg = exmsg
            djwt = {}
        isec = int(tab[0])-stamp # 过期剩余秒数
        stat = {
            'len': len(data), 'sec': isec, # 过期剩余秒数
            'upd': isec<utime, # 是否需要更新(备用)
            'chk': md5(f"{tab[0]}.{tab[1]}.{secret}") == tab[2]
        } 
        res = {'djwt':djwt, 'dorg':tab, 'stat':stat, 'dmsg':ermsg}
    else:                    # === 加密
        utime = int(utime*2) if utime>0 else 6*3600 # 默认过期时间(stamp+utime)
        jstr = json_dump(data,0).replace("\n",'').replace(" ",'')
        jstr = str(stamp+utime) +'.'+ bs64(jstr)
        res = jstr +'.'+ md5(jstr+'.'+secret)
    return res
""" # 外部jwt库   # now  exp/tab[0]
    data['token'] = jwt.encode(dic={}, secret, algorithm='HS256')  # 加密生成字符串
    data['toklen'] = len(token)
    data['ujwt'] = jwt.decode(token='', secret, algorithms=['HS256'])  # 解密，校验签名
"""

# === markdown ==========================

# markdown
def mdinc(key, rf=1, restr=0):
    # 获取文件
    if rf:
        mkvs = scut.gvget('mkvs') # mkv动态
        key = fdir.fpfix(key, mkvs, '.md')
        key = key.replace('{mod}',mkvs['mod']).replace('{key}',mkvs['key']).replace('{view}',mkvs['view'])
        fp = fdir.fpfull(key, 'tpl')
        if os.path.exists(fp):
            mdstr = fdir.get(fp)
        else:
            mdstr = '{md:`'+key+'`}'
    else:
        mdstr = key
    # 递归嵌套
    mdkeys = re.findall(r"\{md\:\"([\w\/\.\-]+)\"\}", mdstr)
    if mdkeys:
        for k1 in mdkeys:
            kstr = mdinc(k1)
            mdstr = mdstr.replace('{md:"'+k1+'"}', kstr)
    # path替换 {=ucfg.base.name}
    paths = re.findall(r"\{\=ucfg\.([\w\.]+)\}", mdstr)
    if paths:
        for k2 in paths:
            kpath = scut.ucfg(k2)
            mdstr = mdstr.replace('{=ucfg.'+k2+'}', kpath)
    # return
    if restr:
        return mdstr
    # links
    mdstr = links(mdstr)
    exts = ['extra','codehilite','legacy_attrs','legacy_em','sane_lists']
    md = markdown.Markdown(extensions=exts) # extensions=admonition,meta,nl2br,smarty,toc,wikilinks
    html = md.convert(mdstr)
    html = html.replace("<p><img", "<p class='tc'><img").replace('<a ', '<a target="_blank" ')
    return html

# mdtitle
def mdtitle(key, rf=1):
    title = ''
    mdstr = mdinc(key, rf, 1) if rf else key
    reg = r'([#]+) ([^\n\r]+)' 
    tobj = re.search(reg, mdstr, re.M|re.I) #;print(tobj)
    if tobj:
        title = tobj.group(2)
    return title

# 替换链接
def links(data):
    reg = r'(\s{1})(https?\:\/\/[^\ \'\"\n\r]+)(\s{1})'
    data = re.sub(reg, r'\1<a href="\2">\2</a>\3', data) #  target="_blank"
    return data
    #res = re.findall(reg, data, re.S|re.M)

# === json ==========================

def json_dump(data, indent=4): 
    return json.dumps(data, ensure_ascii=False, cls=JsonEncoder, indent=indent)

class JsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, bytes):
            return str(obj, encoding='utf-8')
        elif isinstance(obj, int):
            return int(obj)
        elif isinstance(obj, float):
            return float(obj)
        else:
            return obj.__str__()

# === enc ==========================

def enc_tab(dec=0, ext=0, salt=''): 
    stab = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    srev = 'cwUJCs3fu1xZLNjBQO0rGRFKIbykAt4egzdXWViHpE7PShTnqmMDa8Y5v692lo' # key
    if salt:
        li = list(salt); li = set(li)
        for ch in li:
            srev = srev.replace(ch, '')
        srev = ''.join(li) + srev
    tab2 = '`-=[];,./~!@$%^*()_+{}|:&#' # \'<>?"
    rev2 = '+]@|[:;}(,_`$)~{!%#=&/^-*.'

    if ext==1:
        stab += tab2
        srev += rev2
    elif ext==2:
        stab = stab + tab2
        srev = rev2 + srev

    if dec:
        return {'b':stab, 'a':srev}
    else:
        return {'a':stab, 'b':srev}

def enc_main(ustr, dec=0, ext=0, salt=''):
    tab = enc_tab(dec, ext, salt)
    list1 = list(ustr)
    data1 = []
    for ch in list1:
        if ch in tab['a']:
            pos = tab['a'].find(ch) 
            c2 = tab['b'][pos:pos+1]
            data1.append(c2)
        else:
            data1.append(ch)
    return ''.join(data1)

# === xxx ==========================

# _xxx
def _xxx(tp=''):
    return xxxx.irun(tp)

# === end ==========================
