
from AppYS.core import mkvop, scut #, root
from AppYS import create_app # config, 
from AppYS.root import home

""" app-cmd(命令行): ??? 无法post?
    run.mcli:1
"""

if __name__ == '__main__':

    scut.gvinit()
    res = mkvop.chk_path(1)
    mode = res['cli']

    if mode and mode=='api': # 仅api显示模式-快, 不能解析模板
        mkvop.vdef(res['path'])
    else:                    # 默认tpl解析模式-慢, 可解析模板
        app = create_app({})
        with app.app_context():
            if res['path']=='':
                home.vdef()
            elif not '/' in res['path']:
                res = home.run(res['path'])
                print(type(res), res)
            else:
                mkvop.vdef(res['path'])

"""
    @app.errorhandler(Exception)
    def error_500(ex):
        return mkvop.vexc()
"""
