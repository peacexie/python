
import json
from django.http import HttpResponse
from django.shortcuts import render

# mkvs

# -----------------------------

# 默认首页方法, 不返回数据或不改变模板可不要
def _homeAct(request, mkvs):
    data = {'testData':'homeData'}
    return data

# 默认列表方法, 不返回数据或不改变模板可不要
def _listAct(request, mkvs):
    data = {'testList':'listData'}
    return data 
 
# 默认详情方法, 不返回数据或不改变模板可不要
def _viewAct(request, mkvs):
    data = {'flagDetail':'viewDetail'}
    data['kid'] = mkvs['key']
    return data

# -----------------------------

def vdataAct(request, mkvs):
    data = {'testData':'vdataValue'}  
    return data

def er500Act(request, mkvs):
    data = {}
    data['res'] = 8/0        
    return data

# -----------------------------

def utplAct(request, mkvs):
    data = {'tips':'resetTemplate'}
    data['_newtp'] = '~user-tpl' # 设置新模板
    return data # read/test/~user-tpl

def rejsonAct(request, mkvs):
    data = {'jsonTest':'jsonDetail'}
    data['_return'] = 'json' # 直接返回json
    return data

# -----------------------------

# 提示信息: OK
def tipokAct(request, mkvs):
    data = {'tipok':'tipokDetail'}
    info = {'_retip':1, 'errmsg':'操作成功!', 'data':data}
    icon = request.GET.get('icon', default='')
    if icon:
        info['icon'] = icon
    return info

# 提示信息: Bad
def errorAct(request, mkvs):
    data = {'tibad':'tip(bad)Detail'}
    info = {'_retip':0, 'errno':'test:BadMessage', 'errmsg':'It is a TEST BAD Message!', 'data':data}
    return info

# -----------------------------
