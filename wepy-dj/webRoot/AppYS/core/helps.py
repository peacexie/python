
import os, time, re, markdown
from django.conf import settings

from . import files

# user-config
def ucfg(keys):
    cfgs = settings.UCFG 
    arr = keys.split('.')
    for i in range(len(arr)):
        key = arr[i]
        if key in cfgs:
            cfgs = cfgs[key]
        else:
            return ''
    return cfgs

# make-url
def mkurl(mkv=''):
    if mkv.find(':')>0:
        path = '/' + mkv.replace(':','/')
    else:
        path = './' + mkv
    return path

# markdown-include
def mdinc(key):
    # mkv动态
    mkvs = settings.UCFG['mkvs']
    key = files.fpfix(key, mkvs, '.md')
    key = key.replace('{mod}',mkvs['mod']).replace('{key}',mkvs['key']).replace('{view}',mkvs['view'])
    # 获取文件
    fp = files.fpfull(key, 'tpl')
    if os.path.exists(fp):
        mdstr = files.get(fp)
    else:
        mdstr = key
    # 递归嵌套
    mdkeys = re.findall(r"\{md\:\"([\w\/\.\-]+)\"\}", mdstr)
    if mdkeys:
        for k1 in mdkeys:
            kstr = mdinc(k1)
            mdstr = mdstr.replace('{md:"'+k1+'"}', kstr)
    # path替换 {=ucfg.base.name}
    paths = re.findall(r"\{\=ucfg\.([\w\.]+)\}", mdstr)
    if paths:
        for k2 in paths:
            kpath = ucfg(k2)
            mdstr = mdstr.replace('{=ucfg.'+k2+'}', kpath)
    # return
    md = markdown.Markdown() # extensions=['extra']
    html = md.convert(mdstr)
    return html

# =============================

"""
str = "a123b"
print re.findall(r"a(.+?)b",str)
"""
