#coding=UTF-8
# mkvop - mkv-view-operate

import sys, os, time, json

from django.http import HttpResponse
from django.shortcuts import render
from django.conf import settings

from . import helps, files

# ====================================== 

# 模板视图
def tpl(request, part='root', mkv='home'):
    info = {}
    mkvs = mkv_info(part, mkv)
    if mkvs['key'] in ['_home','_list','_view']:
        info = tip_info('Error-Url', 'Error `'+mkvs['key']+'` in the Url')
        return view(request, info, {})
    acts = mkv_act(mkvs)
    info['tpname'] = now_tpl(mkvs)
    if acts['act']:
        res = data = acts['act'](request, mkvs)
        if '_return' in res and res['_return']=='json': # 直接返回json
            return resp_json(info, res, mkvs)
        if '_retip' in res: # 提示信息: 1-OK/0-Bad
            info = res; data=res['data']; del info['data'];
            if info['_retip'] and (not 'icon' in info):
                info['icon'] = 'check-square-o'
            info['status'] = 200 if info['_retip'] else 404
            info['tpname'] = 'root/' + ('info' if info['_retip'] else 'error')
        if '_newtp' in res: # 设置新模板
            info['tpname'] = files.fpfix(res['_newtp'], mkvs)
    else:
        data = {}
    # 检查模板是否存在
    fp = str(settings.TEMPLATES[0]['DIRS'][0]) +'/'+ info['tpname']
    if not os.path.exists(fp+'.htm'):
        info = tip_info('Not-Found:Template', 'Template: `{tpl}/'+info['tpname']+'.htm` No Found')
    return view(request, info, data)

# api 视图
def api(request, part, mkv=''):
    info = {}
    mkvs = mkv_info(part, mkv)
    acts = mkv_act(mkvs)
    if acts['act']:
        data = acts['act'](request, mkvs)
    else:
        info['errno'] = 'Not-Found:View-Act'
        info['errmsg'] = 'Not-Found View-Act for this Url'
        data = {}
    # 默认无错误信息
    if not 'errno' in info:
        info['errno'] = 0
    if not 'errmsg' in info:
        info['errmsg'] = 'ok'
    resp_json(info, data, mkvs)

def resp_json(info, data, mkvs={}):
    info['mkvs'] = mkvs
    info['data'] = data
    jsonStr = json.dumps(info)
    return HttpResponse(jsonStr, content_type="application/json")

# 兼容视图
def run(request, part, mkv=''):
    pass

# ====================================== 

def view(request, info={}, data={}):
    status = info['status'] if 'status' in info else 200
    tpname = info['tpname'] if 'tpname' in info else 'root/info'
    info['mkvs'] = settings.UCFG['mkvs'] if 'mkvs' in settings.UCFG else {}
    info['data'] = data
    settings.UCFG['run']['tpname'] = tpname
    return render(request, tpname+'.htm', info, status=status)

def i400(request, exception):
    return exc_info(request, '400', 'exc')
def i403(request, exception):
    return exc_info(request, '403', 'exc')
def i404(request, exception):
    return exc_info(request, '404', 'exc')
def i500(request):
    return exc_info(request, '500', 'exc')

# 提取 http-exception 信息
def exc_info(request, status, data=''):
    tabs = {
        '400': 'Bad Request (400)',
        '403': '403 Forbidden',
        '404': '404 Not Found',
        '500': 'Server Error (500)',
    }
    if data=='exc':
        data = {'excmsg':'Exception-'+status}
    info = tip_info('Http-'+status, tabs[status], int(status))
    return view(request, info, data)

# ====================================== 

# 组提示(错误)信息
def tip_info(no, msg, status=404):
    info = {}
    info['errno'] = no
    info['errmsg'] = msg
    info['tpname'] = 'root/' + ('info' if status==200 else 'error')
    info['status'] = status
    return info

# 获取当前模板
def now_tpl(mkvs):
    fp1 = str(settings.TEMPLATES[0]['DIRS'][0]) +'/'+ mkvs['tpone']
    fpd = str(settings.TEMPLATES[0]['DIRS'][0]) +'/'+ mkvs['tpdef']
    if mkvs['tpone'] and os.path.exists(fp1+'.htm'):
        tpname = mkvs['tpone']
    else: #elif os.path.exists(fpd+'.htm'):
        tpname = mkvs['tpdef']
    return tpname

# 获取 mkv-act
def mkv_act(mkvs={}):
    path = settings.BASE_DIR #TEMPLATES[0]['DIRS'][0]
    file = 'AppYS/'+mkvs['part']+'/'+mkvs['mod']+'.py'
    fp = str(path) +'/'+ file #;print(fp)
    if not os.path.exists(fp):
        return {'obj':'', 'act':''}
    cp = file.replace('.py','').replace('/','.')
    obj = __import__(cp, fromlist=(os.path.basename(file))) 
    act = ''
    arr = [mkvs['key'], '_'+mkvs['type']] #, '_list'
    for itm in arr:
        act = getattr(obj, itm+'Act', '')
        if act:
            break
    return {'obj':obj, 'act':act}

# 分析 mkv 信息
def mkv_info(part, mkv='home'):
    # default
    mkvOrg = mkv
    if len(part)==0:      # </root>/info
        part = 'root'
    mode = 'home'
    if (not mkv) or len(mkv)==0:  # /front/
        mkv = 'home'
    elif mkv.find('.')>0: # /front/news.nid
        mode = 'view'
    elif mkv.find('-')>0: # /front/news-cid
        mode = 'list'
    else:                 # /front/news
        pass # mkv = mkv
    # spilt-mkv
    if mode=='home':
        mod = mkv; key = ''; view = '';
    else:
        tmp = mkv.split('.') if mkv.find('.')>0 else mkv.split('-')
        mod = tmp[0]; key = tmp[1]; view = tmp[2] if len(tmp)>=3 else ''
    # tpname
    tpdir = part +'/'+ mod +'/'; tpone = '';
    if mode=='home':
        tpfile = '_'+ mode
    elif mode=='list':
        tpfile =   '_'+ mode + ('-'+view if len(view)>0 else '')
        tpone = tpdir + key + ('-'+view if len(view)>0 else '')
    else: # view
        tpfile = '_'+ mode + ('.'+view if len(view)>0 else '')
    # return-mkvs
    mkvs = {
        'mkv':mkv, 'mod':mod, 'key':key, 'view':view, 'mkvOrg':mkvOrg,
        'part':part, 'type':mode, 'tpdef':tpdir+tpfile, 'tpone':tpone,
    }
    settings.UCFG['mkvs'] = mkvs
    return mkvs

# ====================================== 


"""

root: root, error, info
type: home, list, view

root/home
part/mod.view
part/mod-list

$res['errno'] = 'Error-Sign';
$res['errmsg'] = '签名错误或超时';
errno, errmsg, title

"""
