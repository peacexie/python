# files:文件相关

import sys, os, time, hashlib, re, markdown
from urllib import parse
from django.conf import settings

# 为文件自动添加相对目录: read -> part/mod/read.md
def fpfix(file, mkvs={}, ext=''):
    if not 'mkv' in mkvs:
        mkvs = settings.UCFG['mkvs']
    if not '/' in file:
        file = mkvs['part'] +'/'+ mkvs['mod'] +'/'+ file
    elif file.count('/')==1: # not (mkvs['part']+'/') in file:
        file = mkvs['part'] +'/'+ file
    if ext and not ext in file:
        file += ext
    return file

# 文件完整路径 root/info.htm -> {proj}/tphtml/root/info.htm
def fpfull(fp, key='base'):
    path = settings.BASE_DIR;
    if key=='tpl':
        path = settings.TEMPLATES[0]['DIRS'][0]
    return str(path) + '/' + fp;

def get(fp, cset='utf-8', mode='rb'):
    flag = os.path.exists(fp)
    if not flag:
        return ''
    data = ''
    fh = open(fp, mode)
    data = fh.read()
    fh.close()
    if cset:
        data = data.decode(cset)
    return data

def put(fp, data): 
    with open(fp, 'w', encoding='utf-8') as f:
        f.write(data)
    f.close()


# 自动文件名
def fpauto(url, full=1):
    if len(url)==0:
        url = 'index.htm'
    file = url if (full or not os.path.basename(url)) else os.path.basename(url)
    file = re.sub("http(s)?://",'',file)
    file = file.replace('/','!').replace('?','---').replace('&',';')
    file = re.sub('[*:|]','-',file)
    file = re.sub('[\'"<>%#]','',file)
    if len(file)>96: # 160
        file = re.sub('[\u0080-\uffff]','',file)
        m5 = hashlib.md5(file.encode("latin1")).hexdigest()
        file = file[:60] +'---'+ m5 +'---'+ file[-30:]
    reg = r'\.(jpg|jpeg|gif|png|htm|html)$'
    if not re.findall(reg, file):
        file += '.htm';
    return file
