
import os, sys, time, markdown

from django import template
from django.utils.safestring import mark_safe 
from django.conf import settings

from . import files, helps

register = template.Library()  # register的名字是固定的,不可改变

# user-config
@register.simple_tag
def ucfg(keys):
    return helps.ucfg(keys)

# make-url
@register.simple_tag
def mkurl(mkv=''):
    path = helps.mkurl(mkv)
    return mark_safe(path)

# data
@register.simple_tag
def cdata(pa='', pb=''):
    data = {'a':'va', 'b':'vb'}
    return data

# markdown-include
@register.simple_tag
def mdinc(key):
    html = helps.mdinc(key)
    return mark_safe(html)

@register.simple_tag
def excmsg():
    exinfo = sys.exc_info()
    html = ''
    if exinfo[0]:
        for item in exinfo:
            html += "<li>"+str(item).replace('<','&lt;')+"</li>"
    html = '-' #html.replace(str(settings.BASE_DIR),'{proj}/')
    return mark_safe(html)

# Used:0.0(ms); root/error.htm; Time:2021-12-12 18:35:55
# debug-info (动态信息) 
@register.simple_tag
def info(tp=''):
    if not tp:
        tp = 'Used:{run}; {tpl}.htm; Time:{now}'
    used = time.time() - settings.UCFG['run']['stamp']
    if used>1:
        used = str(used) + '(s)'
    else:
        used = str(round(used*1000,3)) + '(ms)'
    now = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    tpl = settings.UCFG['run']['tpname']
    html = tp.replace('{run}',used).replace('{tpl}',tpl).replace('{now}',str(now))
    return mark_safe(html)

# =============================

# 利用装饰器 @register.filter 自定义过滤器。
# 注意：装饰器的参数最多只能有 2 个。

@register.filter
def my_filter(v1, v2):
    return v1 * v2

# 利用装饰器 @register.simple_tag 自定义标签。

@register.simple_tag
def my_tag1(v1, v2, v3):
    return v1 * v2 * v3

@register.simple_tag
def my_html(v1, v2):
    temp_html = "<input type='text' id='%s' class='%s' />" %(v1, v2)
    return mark_safe(temp_html)

