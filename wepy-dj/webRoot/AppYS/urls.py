
#https://docs.djangoproject.com/en/3.2/topics/http/urls/

from django.contrib import admin
from django.conf.urls import handler400, handler403, handler404, handler500 #, url, defaults
from django.urls import path, re_path, include

#from AppYS.demo import views
from .core import mkvop 

urlpatterns = [

    path('sysadmin/', admin.site.urls),

    re_path(r'^(read|comm)/([\w\-\.]+)?$', mkvop.tpl),

    path("demo/", include(("AppYS.demo.url_demo","demo"))),
    path("demo/url1/", include(("AppYS.demo.url_route","url1"))),

    path('', mkvop.tpl, name='homePage'),
]

# Http-handlers
handler400 = mkvop.i400
handler403 = mkvop.i403
handler404 = mkvop.i404
handler500 = mkvop.i500 # defaults.server_error #
