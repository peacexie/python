
import os
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent
SECRET_KEY = 'Xdjango-insecure-wm)7iud2-2(s@r@6+f!boz40xaqfafe-g+v$#ih^yel&g-ec^v'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False # False,True
ALLOWED_HOSTS = ['*'] # ['*'], False
#DEBUG_PROPAGATE_EXCEPTIONS = True

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'TestModel',  # 添加此项
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    'AppYS.core.midware.basic',
]

ROOT_URLCONF = 'AppYS.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        #'DIRS': [os.path.join(BASE_DIR, 'tplate')], # ok
        #'DIRS': ["tplate"], # ok
        'DIRS': [BASE_DIR / "tplate", ], # ok
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            "libraries":{
                'ctags':'AppYS.core.ctags' # templatetags.my_tags, tpltags.ctags
            } 
        },
    },
]

WSGI_APPLICATION = 'AppYS.wsgi.application'

# Database
DATABASES = {
    'default': 
    {
        'ENGINE': 'django.db.backends.mysql',    # 数据库引擎
        'NAME': 'txext_main', # 数据库名称
        'HOST': '127.0.0.1', # 数据库地址，本机 ip 地址 127.0.0.1 
        'PORT': 3306, # 端口 
        'USER': 'root',  # 数据库用户名
        'PASSWORD': '123456', # 数据库密码
    },
    'sqlite3': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'), #BASE_DIR / 'db.sqlite3',
    }
}

# Password validation
AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',},
]

# Internationalization
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images) 
STATIC_URL = '/static/'
STATICFILES_DIRS = [ 
    os.path.join(BASE_DIR, "../../share_static"), 
]

# Default primary key field type
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

# 自定义参数

# 该变量的变量名需要全部大写，否则会引用不到
UCFG = {
    'base':{
        'name': 'Python测试' # AppYS,永顺App
    },
    'path':{
        'imcat': 'http://txjia.com/imcat',
        'python': 'https://www.python.org',
        'xdemo1': ''
    },
    'fix'
    'static': '/static/',
    'mkv': {}, 'run': {} 
}
