
import pymysql
pymysql.install_as_MySQLdb()

# MySQLdb 只适用于python2.x，发现pip装不上；它在py3的替代品是： import pymysql。
# 而Django默认的还是使用MySQLdb:执行会报：ImportError: No module named ‘MySQLdb’
