
from django.shortcuts import render, HttpResponse
from django.views import View

"""
def Cbview(request):
    return HttpResponse("func:Cbview")
"""

#"""
class Cbview(View):

    def get(self, request):
        return HttpResponse("Django 视图 - FBV 与 CBV - GET 方法")

    def post(self, request):
        user = request.POST.get("user")
        pwd = request.POST.get("pwd")
        if user == "runoob" and pwd == "123456":
            return HttpResponse("Django 视图 - FBV 与 CBV - POST 方法:OK")
        else:
            return HttpResponse("Django 视图 - FBV 与 CBV - POST 方法:Back")
#"""
