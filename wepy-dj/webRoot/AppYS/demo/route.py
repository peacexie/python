
import os
from django.contrib import auth
from django.contrib.auth.models import User
from django.http import HttpResponse,QueryDict
from django.shortcuts import render, reverse, redirect
from TestModel import models 

# route

def article(request, kid):
    data         = {}
    # 
    data['kid'] = kid;
    data['dict'] = {"name":"peace", "age":18};
    return render(request, 'demo/article.htm', data)

def vroute(request):
    data         = {}
    # 
    data['rev_login'] = reverse("v1_login")
    data['dict'] = {"name":"peace", "age":18};
    return render(request, 'demo/article.htm', data)

def loginv1(request):
    data         = {}
    # 
    #data['kid'] = kid;
    data['dict'] = {"name":"peace", "age":18};
    return render(request, 'demo/article.htm', data)

# ------------------

def cs_login(request):
    if request.method == "GET":
        return render(request, "demo/cs_login.htm")
    username = request.POST.get("username")
    password = request.POST.get("pwd")

    user_obj = models.UserInfo.objects.filter(username=username, password=password).first()
    print(user_obj.username)

    if not user_obj:
        return redirect("/demo/cs_login/")
    else:
        rep = redirect("/demo/cs_index/")
        rep.set_cookie("is_login", True)
        return rep
       
def cs_index(request):
    print(request.COOKIES.get('is_login'))
    status = request.COOKIES.get('is_login') # 收到浏览器的再次请求,判断浏览器携带的cookie是不是登录成功的时候响应的 cookie
    if not status:
        return redirect('/cs_login/')
    return render(request, "demo/cs_index.htm")

def cs_logout(request):
    rep = redirect('/demo/cs_login/')
    rep.delete_cookie("is_login")
    return rep # 点击注销后执行,删除cookie,不再保存用户状态，并弹到登录页面

def cs_order(request):
    print(request.COOKIES.get('is_login'))
    status = request.COOKIES.get('is_login')
    if not status:
        return redirect('/demo/cs_login/')
    return render(request, "demo/cs_order.htm")

# ------------------

def cs_sigin(request):
    if request.method == "GET":
        return render(request, "demo/cs_login.htm")
    username = request.POST.get("username")
    password = request.POST.get("pwd")

    user_obj = models.UserInfo.objects.filter(username=username, password=password).first()
    print(user_obj.username)

    if not user_obj:
        return redirect("/demo/cs_sigin/")
    else:
        request.session['is_login'] = True
        request.session['user1'] = username
        return redirect("/demo/cs_home/")

def cs_home(request):
    status = request.session.get('is_login')
    if not status:
        return redirect('/cs_sigin/')
    return render(request, "demo/cs_index.htm")


def cs_sigout(request):
   # del request.session["is_login"] # 删除session_data里的一组键值对
    request.session.flush() # 删除一条记录包括(session_key session_data expire_date)三个字段
    return redirect('/cs_sigin/')

# ------------------

def au_login(request):
    if request.method == "GET":
        return render(request, "demo/au_login.htm")
    username = request.POST.get("username")
    password = request.POST.get("pwd")
    valid_num = request.POST.get("valid_num")
    keep_str = request.session.get("keep_str")
    if keep_str.upper() == valid_num.upper():
        user_obj = auth.authenticate(username=username, password=password)
        print(user_obj.username)
        if not user_obj:
            return redirect("/au_login/")
        else:

            auth.login(request, user_obj)
            path = request.GET.get("next") or "/au_index/"
            print(path)
            return redirect(path)
    else:
        return redirect("/au_login/")

def au_logout(request):
    ppp = auth.logout(request)
    print(ppp) # None
    return redirect("/au_login/")

def au_index(request):
    ppp = auth.logout(request)
    print(ppp) # None
    return redirect("/au_login/")