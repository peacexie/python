

from django.contrib import admin
from django.urls import path, re_path, include

from django.conf.urls import url
from . import views,testdb,search,route
from . import views,testdb,search,route,cbview,book

urlpatterns = [

    url('hello/', views.hello),
    url('udir/', views.udir),
    url('home/', views.home),

    path('hi-template/', views.hi_template),
    path('super-template/', views.super_template),
    #
    path('testdb/hidb', testdb.hidb),
    path('testdb/fetch', testdb.fetch),
    path('testdb/edit', testdb.edit),
    path('testdb/remove', testdb.remove),
    #
    url(r'^so-form/$', search.so_form),
    url(r'^search/$', search.search),
    url(r'^so-post/$', search.so_post),
    #
    url('myResp/', views.myResp),
    url('gpDemo/', views.gpDemo),
    url('vbody/', views.vbody),
    # 
    re_path(r'^articles/([0-9]{4})\.htm$', route.article), # 正则路径
    re_path(r'^articles.([0-9]{4})$', route.article), # 正则路径
    path("vroute/", route.vroute),
    path("login1/", route.loginv1, name="v1_login"),
    #path("app01/", include(("app01.urls","app01"))),
    #path("app02/", include(("app02.urls","app02"))),
    
    #
    path("cbview/", cbview.Cbview.as_view()), # 
    #
    path('book/add1/', book.add1),
    path('book/add2/', book.add2),
    path('book/test1/', book.test1),
    #
    path('cs_login/', route.cs_login),
    path('cs_index/', route.cs_index),
    path('cs_logout/', route.cs_logout),
    path('cs_order/', route.cs_order),
    path('cs_sigin/', route.cs_sigin),
    path('cs_home/', route.cs_home),
    path('cs_sigout/', route.cs_sigout),
    # 
    path('au_login/', route.au_login),
    path('au_logout/', route.au_logout),
    path('au_index/', route.au_index),
    # 
    path('books/mtable/', book.mtable),
    path('books/juhe/', book.juhe),
    path('add_emp/', views.add_emp),
    #


]

"""
r'^$'
hello/
"""
