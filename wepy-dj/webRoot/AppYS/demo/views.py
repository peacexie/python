
import os
from django.http import HttpResponse, QueryDict
from django.shortcuts import render, redirect
from django.urls import path, re_path

from .My_forms import EmpForm
from TestModel import models
from django.core.exceptions import ValidationError

# View


def vbody(request):
    body = request.body; print(body)
    path = request.path #print(path)
    method = request.method #print(method)
    res = "body="+body.decode()+", <br>path="+path+", <br>method="+method
    return HttpResponse(res)

def gpDemo(request):
    getName = request.GET.get("getName")
    getName = 'get:姓名：{}'.format(getName)
    postName = request.POST.get("postName")
    postName = 'post:姓名：{}'.format(postName)
    return HttpResponse(getName +"<br>"+ postName)


def add_emp(request):
    if request.method == "GET":
        form = EmpForm()  # 初始化form对象
        return render(request, "demo/add_emp.htm", {"form":form})
    else:
        form = EmpForm(request.POST)  # 将数据传给form对象
        if form.is_valid():  # 进行校验
            data = form.cleaned_data
            data.pop("r_salary")
            models.Emp.objects.create(**data)
            return redirect("/index/")
        else:  # 校验失败
            clear_errors = form.errors.get("__all__")  # 获取全局钩子错误信息
            return render(request, "demo/add_emp.htm", {"form": form, "clear_errors": clear_errors})
    """
    if request.method == "GET":
        form = EmpForm()
        return render(request, "demo/add_emp.htm", {"form": form})
    else:
        form = EmpForm(request.POST)
        if form.is_valid():  # 进行数据校验
            # 校验成功
            data = form.cleaned_data  # 校验成功的值，会放在cleaned_data里。
            data.pop('salary')
            print(data)

            models.Emp.objects.create(**data)
            return HttpResponse(
                'ok'
            )
            # return render(request, "demo/add_emp.html", {"form": form})
        else:
            print(form.errors)    # 打印错误信息
            clean_errors = form.errors.get("__all__")
            print(222, clean_errors)
        return render(request, "demo/add_emp.htm", {"form": form, "clean_errors": clean_errors})
    """

# Response


def myResp(request):
    import json
    dic = {}
    dic['hi'] = 'Hi, HttpResponse!'
    dic['path'] = request.path
    dic['_get'] = request.GET
    dic['_post'] = request.POST
    #dic['_req'] = request.REQUEST
    dic['_cookie'] = request.COOKIES
    #dic['_meta'] = request.META
    #dic['_raw'] = request.raw_post_data
    #
    #dic['f_has_key'] = request.has_key('GET')
    dic['f_get_full_path'] = request.get_full_path()
    dic['f_is_secure'] = request.is_secure()
    #
    q1 = QueryDict('QueryDict_a=QueryDict_1')
    q1 = q1.copy() # to make it mutable
    q1.update({'QueryDict_a': 'QueryDict_2'})
    dic['o_q1'] = q1
    #
    q2 = QueryDict('a=1&a=2&a=3')
    q2.items()
    dic['o_q2'] = json.dumps(q2)
    #
    q3 = QueryDict('a=1&a=2&a=3')
    q3.lists()
    dic['o_q3'] = json.dumps(q3)
    # output
    
    jsonStr = json.dumps(dic, indent=2)
    #print(request.path)
    return HttpResponse("<pre>"+jsonStr+"</pre>")


# hello

def home(request):
    data         = {}
    # 
    data['dict'] = {"name":"peace", "age":18};
    return render(request, 'demo/home.htm', data)

def udir(request):
    return redirect('/demo/home/')

def hello(request):
    import time
    nowtm = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
    return HttpResponse("Hello world ! ["+nowtm+"]")

def hi_template(request):
    import datetime
    data         = {}
    # base
    data['name'] = 'Peace'
    data['list'] = ["人生","苦短","我","再爬"]
    data['name1'] = 'abc-name1'
    data['words'] = 'I am peace, my favorite is sports!'
    data['time'] = datetime.datetime.now()
    data['link'] = "<a href='http://txjia.com/'>点击跳转:txjia</a>"
    # flow
    data['num'] = 88;
    data['dict'] = {"name":"peace", "age":18};
    return render(request, 'demo/hi-template.htm', data)

def super_template(request):
    data         = {}
    # 
    data['num'] = 88;
    data['dict'] = {"name":"peace", "age":18};
    return render(request, 'demo/super-template.htm', data)
