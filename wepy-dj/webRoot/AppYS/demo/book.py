
import os
from django.http import HttpResponse,QueryDict
from django.shortcuts import render, redirect
from django.urls import path, re_path
from django.db.models import Avg,Max,Min,Count,Sum, F, Q
#from TestModel.models import Test
from TestModel import models 

# View

def juhe(request):
    
    res = models.Books.objects.aggregate(Avg("price"))
    print(res, type(res))

    res = models.Books.objects.aggregate(c=Count("id"),max=Max("price"),min=Min("price"))
    print(res,type(res))

    res = models.Publish.objects.values("name").annotate(in_price = Min("books__price"))
    print(res)

    res = models.Books.objects.annotate(c = Count("authors__name")).values("title","c")
    print(res)

    res = models.Books.objects.filter(title__startswith="菜").annotate(c = Count("authors__name")).values("title","c")
    print(res)

    res = models.Books.objects.annotate(c = Count("authors__name")).filter(c__gt=1).values("title","c")
    print(res)

    print(' --- xxx1 --- ')
    res = models.Books.objects.annotate(c = Count("authors__name")).order_by("-c").values("title","c")
    for i in res:
        print(i)

    res = models.Author.objects.annotate(all = Sum("books__price")).values("name","all")
    for i in res:
        print(i)

    #lists = models.Emp.objects.filter(salary__gt=F("age")).values("name","age") # 查询工资大于年龄的人：
    #res = models.Books.objects.update(price=F("price")-17)
    #print(res)

    res = models.Books.objects.filter(Q(price__gt=350)|Q(title__startswith="菜")).values("title","price")
    print(res)

    res = models.Books.objects.filter(Q(title__endswith="菜") | ~Q(Q(pub_date__year=2010) & Q(pub_date__month=10)))
    res = models.Books.objects.filter(Q(pub_date__year=2004) | Q(pub_date__year=1999), title__contains="菜")
    print(res)  

    return HttpResponse("ok")


# =========================

def add1(request):
    book = models.Book(title="菜鸟教程",price=300,publish="菜鸟出版社",pub_date="2008-8-8") 
    book.save()
    return HttpResponse("<p>数据添加成功1！</p>")

def add2(request):
    books = models.Book.objects.create(title="如来神掌",price=200,publish="功夫出版社",pub_date="2010-10-10") 
    print(books, type(books)) # Book object (18) 
    return HttpResponse("<p>数据添加成功2！</p>")

def test1(request):
    
    books = models.Book.objects.filter(publish='菜鸟出版社', price=300)
    books = models.Book.objects.exclude(publish='菜鸟出版社', price=300)
    book1 = models.Book.objects.get(pk=5)
    books = models.Book.objects.order_by("-price") 
    books = models.Book.objects.order_by("-price").reverse()
    count = models.Book.objects.filter(price=200).count(); print('count = '+ str(count)) 
    # .first(), .last(), 
    #books = models.Book.objects.get(pk=18)  # 报错，没有符合条件的对象
    #books = models.Book.objects.get(price=200)  # 报错，符合条件的对象超过一个
    #print(books,type(books)) # QuerySet类型，类似于list，访问 url 时数据显示在命令行窗口中。publish  pub_date

    booka = models.Book.objects.exists()
    #bookb = models.Book.objects.count().exists() # 报错，判断的数据类型只能为QuerySet类型数据，不能为整型
    #bookc = models.Book.objects.first().exists() # 报错，判断的数据类型只能为QuerySet类型数据，不能为模型类对象

    bookp = models.Book.objects.values("pk","price"); print('bookp = '+ str(bookp)) 
    print(bookp[0]["price"],type(bookp)) # 得到的是第一条记录的price字段的数据

    bookvl = models.Book.objects.values_list("price","publish"); print(bookvl)
    bookp2 = models.Book.objects.values_list("publish").distinct(); print(bookp2) # 查询一共有多少个出版社

    books = models.Book.objects.filter(price__in=[200,300])
    books = models.Book.objects.filter(price__gt=200)
    books = models.Book.objects.filter(price__gte=200)
    books = models.Book.objects.filter(price__lt=300)
    books = models.Book.objects.filter(price__lte=300)
    books = models.Book.objects.filter(price__range=[200,300])
    books = models.Book.objects.filter(title__contains="菜")
    books = models.Book.objects.filter(title__icontains="python") # 不区分大小写
    books = models.Book.objects.filter(title__startswith="菜")
    books = models.Book.objects.filter(title__endswith="教程")
    books = models.Book.objects.filter(pub_date__year=2008) 
    books = models.Book.objects.filter(pub_date__month=10) 
    books = models.Book.objects.filter(pub_date__day='01')

    #books=models.Book.objects.filter(pk=8).first().delete()
    #books=models.Book.objects.filter(pk__in=[1,2]).delete()
    #books=models.Book.objects.delete()　 # 报错
    #books=models.Book.objects.all().delete()　　 # 删除成功

    books = models.Book.objects.filter(pk=7).first() 
    books.price = 357 
    books.save()

    books = models.Book.objects.filter(pk__in=[8,9]).update(price=789)
    return HttpResponse(books)

    booka = models.Book.objects.all() 
    for i in booka:
        print(str(i.id) +':'+ i.title +':'+ str(i.price) +':'+ i.publish +':') 
    return HttpResponse("<p>查找成功！</p>")

# =========================

# 多表实例
def mtable(request):

    """
    #  获取出版社对象
    pub_obj = models.Publish.objects.filter(pk=1).first()
    #  给书籍的出版社属性publish传出版社对象
    book = models.Books.objects.create(title="菜鸟教程", price=200, pub_date="2010-10-10", publish=pub_obj)
    print(book, type(book))
    return HttpResponse(book)
    """

    """
    #  获取出版社对象
    pub_obj = models.Publish.objects.filter(pk=1).first()
    #  获取出版社对象的id
    pk = pub_obj.pk
    #  给书籍的关联出版社字段 publish_id 传出版社对象的id
    book = models.Books.objects.create(title="冲灵剑法", price=100, pub_date="2004-04-04", publish_id=pk)
    print(book, type(book))
    return HttpResponse(book)
    """

    """
    #  获取作者对象
    chong = models.Author.objects.filter(name="令狐冲").first()
    ying = models.Author.objects.filter(name="任盈盈").first()
    #  获取书籍对象
    book = models.Books.objects.filter(title="菜鸟教程").first()
    #  给书籍对象的 authors 属性用 add 方法传作者对象
    book.authors.add(chong, ying)
    return HttpResponse(book)
    """

    """
    #  获取作者对象
    chong = models.Author.objects.filter(name="令狐冲").first()
    #  获取作者对象的id
    pk = chong.pk
    #  获取书籍对象
    book = models.Books.objects.filter(title="冲灵剑法").first()
    #  给书籍对象的 authors 属性用 add 方法传作者对象的id
    book.authors.add(pk)
    return HttpResponse(book)
    """

    """
    book_obj = models.Books.objects.get(id=3)
    author_list = models.Author.objects.filter(id__gt=2)
    book_obj.authors.add(*author_list)  # 将 id 大于2的作者对象添加到这本书的作者集合中
    # 方式二：传对象 id
    book_obj.authors.add(*[1,3]) # 将 id=1 和 id=3 的作者对象添加到这本书的作者集合中
    return HttpResponse("ok")
    """

    """
    pub = models.Publish.objects.filter(name="明教出版社").first()
    wo = models.Author.objects.filter(name="任我行").first()
    book = wo.books_set.create(title="吸星大法", price=300, pub_date="1999-9-19", publish=pub)
    print(book, type(book))
    return HttpResponse("ok")
    """

    """
    author_obj =models.Author.objects.get(id=1)
    book_obj = models.Books.objects.get(id=6)
    author_obj.books_set.remove(book_obj)
    return HttpResponse("ok")
    """

    """
    #  清空 独孤九剑 关联的所有作者
    book = models.Books.objects.filter(title="独孤九剑").first()
    book.authors.clear()
    """

    book = models.Books.objects.filter(pk=3).first()
    res = book.publish.city
    print(res, type(res))
    #return HttpResponse("ok")

    pub = models.Publish.objects.filter(name="明教出版社").first()
    res = pub.books_set.all()
    for i in res:
        print(i.title)
    #return HttpResponse("ok")

    author = models.Author.objects.filter(name="令狐冲").first()
    res = author.au_detail.tel
    print(res, type(res))
    #return HttpResponse("ok")

    addr = models.AuthorDetail.objects.filter(addr="黑木崖").first()
    res = addr.author.name
    print(res, type(res))
    #return HttpResponse("ok")

    book = models.Books.objects.filter(title="菜鸟教程").first()
    res = book.authors.all()
    for i in res:
        print(i.name, i.au_detail.tel)
    #return HttpResponse("ok")

    author = models.Author.objects.filter(name="任我行").first()
    res = author.books_set.all()
    for i in res:
        print(i.title)
    #return HttpResponse("ok")

    res = models.Books.objects.filter(publish__name="明教出版社").values_list("title", "price")
    res = models.Publish.objects.filter(name="明教出版社").values_list("books__title","books__price")

    res = models.Books.objects.filter(authors__name="任我行").values_list("title")
    res = models.Author.objects.filter(name="任我行").values_list("books__title")

    res = models.Author.objects.filter(name="任我行").values_list("au_detail__tel")
    res = models.AuthorDetail.objects.filter(author__name="任我行").values_list("tel")

    return HttpResponse("ok")


